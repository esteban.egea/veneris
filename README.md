# Veneris simulator: a 3D traffic simulator


Veneris is a  set of Unity components and related assets that provide a realistic microscopic road network simulation in an interactive 3D environment. 
Builder components are used to generated the scenario elements: roads, intersections, traffic lights or buildings. 
Vehicle components include a model of the dynamics of the vehicle and components which model the behavior of the vehicles on roads, intersections and the interaction with other vehicles. 
Communication components implement the communication with simulation modules. Managers handle different aspects of the simulation globally. 

It uses the [Unity](https://unity3d.com/) game engine.

With Veneris you can create a simulation scenario from real-world  map data from 
OpenStreet and run a traffic simulation on it. The goal of Veneris is to support 
the simulation of realistic Vehicular Networks together with other tools: [OMNET++](https://inet.omnetpp.org/), 
which provide the network simulation and [Opal](https://gitlab.com/esteban.egea/opal) (uses Opal 0.1 at the moment), a 
multipath channel propagation simulator on GPUs.

For more information visit the [Veneris website](http://pcacribia.upct.es/veneris/).



## Installation
In this repository you can find only the  code used by Veneris, as a version control. To run Veneris you also need a number of additional assets, such as prefabs, 3D models, materials and so on, which are bundled together with the code in a Unity package that you can download (see below). In addition, you need to configure the environement. 
### Requirements
Unity 3D game engine. Veneris has been **tested only on Unity 2017.3**.  You can find all releases [here](https://unity.com/releases/editor/archive). We plan to update it to the latest versions of Unity in next releases. 

### Install Veneris
1. Install Unity and create a new project.
2. Download the Veneris Unity package from [our web](http://pcacribia.upct.es/veneris/downloads/releases/veneris-0.2.1.unitypackage), where you can find the code and all the additional assest.
3. Click on Assets>Import Package>Custom Package ... and select our package.
4. Once the package has been imported correctly (you may need to close and reopen your project), you need to set the Tags and Layers properly. Make sure you create the tags and layers you can see in the picture below 

![layers](layers.png)

5. Finally, you need to set the physics matrix, Edit>Project Settings>Physics as in the image below

![physics](physics.png)

### Use with Opal
If you want to use [Opal, our ray-tracing based propagation simulator](https://gitlab.com/esteban.egea/opal), note that the unitypackage in our last release already comes with 
the Opal plugin included and ready to use. In  [releases](https://gitlab.com/esteban.egea/veneris/-/releases), you can find the corresponding version included. **The included version is the Linux one**. You can use a different 
version (if it is compatible), a different platform (Windows) or add your changes to Opal. In that case, you have to recompile Opal (follow instructions in its repository) and follow the following steps (additional  info in the Opal repository):

1. In the Plugins folder create an `Opal` subdirectory and drop the generated .dll. If the target platform is Linux, do the same but with the .so. In this case, the .so is in 
the `SDK/lib` folder. In that folder you can also find a `ptx` folder, that you have to copy in the `Opal` plugin folder too.
2. You have to copy the `Common.h` also in the `Opal` folder. 
3. Finally, copy the `optix` tree from the source code to the `Opal` folder too.  


### Usage
Once you have the package imported you can open a validation scene and run it in the Unity editor or build an executable. 

You usually start by creating a scenario. For this, you need to use the [SUMO](https://sumo.dlr.de/wiki/Simulation_of_Urban_MObility_-_Wiki) network, routes and polygons file. The easiest way is to use the [SUMO osmWebWizard](http://sumo.dlr.de/wiki/Tutorials/OSMWebWizard) to download and generate the files from OpenStreetMap. You can also download the files and test Veneris with our [scenario builder](http://pcacribia.upct.es/veneris/scenario).

You can run the validation scenes. The size of the traffic demand is large in some of them, for instance in the `Pasubio` scenario, so the framerate will drop, especially in the editor. For better performance, build an executable and then execute it without graphics with
```bash
./executableName -nographics -batchmode
```
