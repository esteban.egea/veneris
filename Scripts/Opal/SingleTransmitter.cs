/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opal;
namespace Opal {

	public class SingleTransmitter : MonoBehaviour
	{
		public Antenna t;
		void Awake() {
			if (t == null) {
				t = GetComponent<Antenna> ();
			}
			if (!OpalManager.isInitialized) {
				OpalManager.Instance.RegisterOpalInitializedListener (OnOpalManagerInitialized);
			}

		}
		public void OnOpalManagerInitialized() {
			if (OpalManager.isInitialized) {
				if (t.transmitter) {
					t.Transmit ();
				}
			}
		}
	}
}
