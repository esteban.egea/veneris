/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opal;
namespace Opal {

	public class DeferredTransmitter : MonoBehaviour
	{
		public Antenna t;
		public float delay=1;
		protected bool transmitted=false;
		void Awake() {
			if (t == null) {
				t = GetComponent<Antenna> ();
			}

		}
		void Update() {
			if (Time.time > delay && !transmitted) {
				if (t.transmitter) {
					t.Transmit();
					transmitted=true;
				}
			}
		}
	}
}

