/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Opal
{
	[System.Serializable]
	public struct OpalEdgeInfo {
		public Vector3 v;
		public Vector3 a;
		public Vector3 b;
		public Vector3 o;
		public float n;
		public uint face_a; 
		public uint face_b; 
		public Vector3 n_a;//Must be normalize
//Must be normalized
		public Vector3 n_b;
		public	uint id;

	}
	public class OpalEdge : MonoBehaviour
	{
		public OpalEdgeInfo info;	
		public OpalMeshProperties opalMeshProperties;
		// Use this for initialization
		void Start () {
			Debug.DrawRay(info.o,info.v,Color.blue);
		}
		void Awake ()
		{
			FillMembers();
		}
		public OpalMeshProperties GetOpalMeshProperties() {
			return opalMeshProperties;
		}
		public void FillMembers() {
			OpalMeshProperties op = transform.parent.GetComponent<OpalMeshProperties> ();
			if (op != null) {
				opalMeshProperties = op;
			}
		}
		public void SetInfo(OpalEdgeInfo i) {
			info=i;
		}
		public void SetInfoValues(Vector3 origin, Vector3 v, uint faid, uint fbid, Vector3 a, Vector3 b, Vector3 na, Vector3 nb, uint eid) {
			info.o=origin;
			info.a=a;
			info.b=b;
			info.n_a=na.normalized;
			info.n_b=nb.normalized;
			info.face_a=faid;
			info.face_b=fbid;
			info.v=v;
			info.id=eid;
			//To get the signed angle (internal) between a and b
			//We do not assume that the edge is perpendicular to the plane of the faces, so compute here the normal
			Vector3 ua=info.a.normalized;
			Vector3 ub=info.b.normalized;
			Vector3 nf=Vector3.Cross(info.n_a,ua); // This defines the side of the angle measured to be the opposite of the one the normal points at
			float angle=Mathf.Atan2(Vector3.Dot(Vector3.Cross(ua,ub),nf), Vector3.Dot(ua,ub));
			//Debug.Log("ua="+ua+"normal_a="+info.n_a+"nf="+nf+"angle="+(angle/Mathf.Deg2Rad));
			if (angle<0) {
				angle = 2*Mathf.PI-angle;
			}
			info.n=2.0f-(angle/Mathf.PI);
		}	
	}
}

