/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opal;
using Veneris;
namespace Opal {

	public class FollowPath : MonoBehaviour
	{
		public List<Path> paths;
		public int currentPath=0;
		public int currentIndex=0;
		void Awake() {
			if (paths.Count >0) {
				//Init all path structures, in case they are not
				foreach (Path p in paths) {
					if (!p.initialized) {
						p.InitPathStructures();
					}
				}
				//Assuming the object is somewhere along the first path, find the closest interpolated point
				if (currentPath<paths.Count) {
					int currentIndex=paths[currentPath].FindClosestPointInInterpolatedPath(transform.position);
					//Set position to that point
					transform.position=paths[currentPath].interpolatedPath[currentIndex].position;
				}
			}

		}
		void Update() {
			//Advanced along the path
				transform.position=paths[currentPath].interpolatedPath[currentIndex].position;
				//Advance indexes
				if (currentIndex==paths[currentPath].interpolatedPath.Length-1) {
					if (currentPath<paths.Count-1) {
						currentPath++;
						currentIndex=0;
					}
				} else {
					currentIndex++;
				}
		}
	}
}

