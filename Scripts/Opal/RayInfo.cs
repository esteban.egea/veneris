﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Opal
{
	public class RayInfo : MonoBehaviour
	{
		public float unfoldedPathLength;
		public int reflections;
	}
}
