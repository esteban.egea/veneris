/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace Opal
{
	public class OpalLogTraceManager : MonoBehaviour {
		public enum Mode
		{
			All,
			Ray,
			Range}

		;
		public enum Type 
		{
			Reflection,
			Diffraction,
			External
		}

		;
		public Color c=Color.red;

		public Mode mode;
		public Type type;
		public bool deleteTraceAtEnd = false;
		public bool showWithoutExecuting = false;
		protected string pFile="ref-trace.txt";
		public string eFile="/home/eegea/optix6.5/SDK/bin/dif-trace.txt";
		public string init = "";
		public string end = "";
		public GameObject rayPrefab;
		public string ray;
		//public Vector3 transmitterPosition;
		public GameObject transmitter;

		void Start() {
			if ( type==Type.External && showWithoutExecuting) {
				ShowAll(1);
			}
		}
		public void ShowRay (string line, GameObject root)
		{
			List<Vector3> positions = new List<Vector3> ();
			string[] rayDir = line.Split (':');
			char[] separator = new char[]{ '\t' };
			System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo ("en-US");
			//GameObject go=new GameObject("ray");
			GameObject go = Instantiate (rayPrefab);
			LineRenderer lr = go.GetComponent<LineRenderer> ();
			lr.startColor = c;
			lr.endColor = c;


			float distance = 0f;
			string[] hitpoints = rayDir [1].Split ('|');
			positions.Add (transmitter.transform.position);
			for (int j = 0; j < hitpoints.Length; j++) {
				string[] tokens = hitpoints [j].Split (separator);
				//Debug.Log ("tokens=" + tokens.Length);
				//Debug.Log (tokens [0] + "," + tokens [1] + "," + tokens [2]);

				Vector3 p = new Vector3 (float.Parse (tokens [0], ci), float.Parse (tokens [1], ci), float.Parse (tokens [2], ci));
				//Debug.Log ("p=" + p);
				distance += Vector3.Distance (p, positions [j]);
				positions.Add (p);
			}


			if (positions.Count > 0) { 

				lr.positionCount = positions.Count;
				lr.SetPositions (positions.ToArray ());
				lr.enabled = true;
				go.name = "ray " + rayDir [0];
				RayInfo ri=go.AddComponent<RayInfo> ();
				ri.unfoldedPathLength = distance;
				if (type==Type.Reflection) {
					ri.reflections=int.Parse(rayDir[2]);
				}

			}

			go.transform.parent = root.transform;



		}
		public void ShowAll (int trNumber)
		{
			string line;
			int i = 0;
			bool found = false;
			string rootName="reflections";
			string rfile=pFile;
			if (type==Type.Diffraction) {
				rootName="diffractions";
				rfile="dif-trace.txt";

			}	
			if (type==Type.External) {
				rootName="External_"+transform.name;
				rfile=eFile;

			}	
			GameObject root = new GameObject (rootName+trNumber);
			if (File.Exists(rfile)) {
				using (System.IO.StreamReader file = new System.IO.StreamReader (rfile, System.Text.Encoding.ASCII)) {

					while ((line = file.ReadLine ()) != null) {
						//Debug.Log ("line=" + line);
						i++;
						ShowRay (line, root);


					}


				}
				Debug.Log (i + " line renderers created");
			} else {
				Debug.Log("No trace file was found or generated for "+rootName+"with fileName= "+rfile);
			}



		}
		protected void OnDestroy() {
			if (deleteTraceAtEnd) {
				string rfile=pFile;
				if (type==Type.Diffraction) {
					rfile="dif-trace.txt";

				}	
				if (File.Exists(rfile)) {
					File.Delete(rfile);
				}
			}
		} 
	}
}

