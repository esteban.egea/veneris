
/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Opal
{
	[System.Serializable]
		public class OpalTriangleFace {
			public int[] tris;
			public uint id;
		}
	public class ReverseEdgeSorter: IComparer<MeshExtrusion.Edge> {
		public int Compare(MeshExtrusion.Edge x, MeshExtrusion.Edge  y) {
			if (x.vertexIndex[0]==y.vertexIndex[0] && x.vertexIndex[1]==y.vertexIndex[1]) {
				return 0;
			}
			if (x.vertexIndex[0]<y.vertexIndex[0] && x.vertexIndex[1]<y.vertexIndex[1]) {
				return -1;
			} else {
				return 1;
			}



		}
	}
	public class EdgeSorter: IComparer<MeshExtrusion.Edge> {
		public int Compare(MeshExtrusion.Edge x, MeshExtrusion.Edge  y) {
			if (x.vertexIndex[0]==y.vertexIndex[0] && x.vertexIndex[1]==y.vertexIndex[1]) {
				return 0;
			}
			if (x.vertexIndex[0]>y.vertexIndex[0] && x.vertexIndex[1]>y.vertexIndex[1]) {
				return -1;
			} else {
				return 1;
			}



		}
	}
	public class EqualFace : EqualityComparer<Vector3>
	{
		public float epsilon=1e-6f;

		bool esentiallyEqual(float a,  float b) {
			return Mathf.Abs(a - b) <= ( (Mathf.Abs(a) > Mathf.Abs(b) ? Mathf.Abs(b) : Mathf.Abs(a)) * epsilon);
		}

		public override bool Equals(Vector3 f1, Vector3 f2)
		{
			return (esentiallyEqual(f1.x, f2.x)  && esentiallyEqual(f1.y, f2.y) && esentiallyEqual(f1.z, f2.z)  ) ;
		}

		public override int GetHashCode(Vector3 v)
		{
			unchecked // disable overflow, for the unlikely possibility that you
			{         // are compiling with overflow-checking enabled
				int hash = 27;
				hash = (13 * hash) + v.x.GetHashCode();
				hash = (13 * hash) + v.y.GetHashCode();
				hash = (13 * hash) + v.z.GetHashCode();
				return hash;
			}
		}
	}
	//AnotherComparer
	public class EqualFaceDifferenceVector : EqualityComparer<Vector3>
	{
		public float epsilon=1e-10f;//square of 1e-5;


		public override bool Equals(Vector3 f1, Vector3 f2)
		{
			Vector3 dif=f1-f2;
			return (dif.sqrMagnitude<epsilon);
		}

		public override int GetHashCode(Vector3 v)
		{
			unchecked // disable overflow, for the unlikely possibility that you
			{         // are compiling with overflow-checking enabled
				int hash = 27;
				hash = (13 * hash) + v.x.GetHashCode();
				hash = (13 * hash) + v.y.GetHashCode();
				hash = (13 * hash) + v.z.GetHashCode();
				return hash;
			}
		}
	}

	public class OpalMeshUtil {

		public static void TransformOpalEdge(Transform t, OpalEdge e) {
			//TODO: Assuming we are just translating and/or rotating, not scaling or searing, otherwise the normal transform is not correct and the angles change
			e.info.v=t.TransformVector(e.info.v);
			e.info.a=t.TransformVector(e.info.a);
			e.info.b=t.TransformVector(e.info.b);
			e.info.o=t.TransformPoint(e.info.o);
			e.info.n_a=t.TransformDirection(e.info.n_a);
			e.info.n_b=t.TransformDirection(e.info.n_b);
		}
		public static void TransformStaticMesh(Transform t, Mesh m) {
			Vector3[] v = m.vertices;
			for (int i=0; i<v.Length; i++ ) {
				v[i]=t.TransformPoint(v[i]);
			}
			m.vertices=v;
			m.RecalculateNormals ();
		}

		public static List<OpalTriangleFace>  ComputeFaceIds(Mesh m, ref uint currentId, Dictionary<Vector3, uint> faceMap) {
			//EqualFace eqF= new EqualFace();	
			//Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
			List<OpalTriangleFace> fl = new List<OpalTriangleFace>();
			int[] t = m.triangles;
			Vector3[] v=m.vertices;
			for (int i=0; i< t.Length; i=i+3) {	
				//Get the normal of the triangle plane
				Vector3 p0 = v[t[i]];
				Vector3 p1 = v[t[i+1]];
				Vector3 p2 = v[t[i+2]];
				Vector3 e0 = p1 - p0;
				Vector3 e1 = p0 - p2;

				Vector3 normal = Vector3.Cross(e1, e0).normalized; //We usually have to normalize if we want to properly compare faces in the map below. The normal is defined by the direction, no the magnitude
				uint cid;
				OpalTriangleFace tf = new OpalTriangleFace();
				tf.tris=new int[3];
				tf.tris[0]= t[i];
				tf.tris[1]= t[i+1];
				tf.tris[2]= t[i+2];
				if (faceMap.TryGetValue(normal, out cid)) {
					tf.id=cid;
					fl.Add(tf);
				} else {
					//New element
					faceMap.Add(normal,currentId);
					tf.id=currentId;
					fl.Add(tf);
					currentId++;
				}
			}
			foreach( KeyValuePair<Vector3, uint> kvp in faceMap )
			{
				Debug.Log( kvp.Key+ "="+ kvp.Value);
			}
			return fl; 
		}
		//Pass south-west point of cube and side length
		public static void  AddCubeEdges(Vector3 sw, float length, uint index, GameObject cube, ref uint eid) {
			//Only 8 edges, the edges on the floor are not considered
			Vector3 x=length*Vector3.right;
			Vector3 up=length*Vector3.up;
			Vector3 z=length*Vector3.forward;
			
			Vector3 xu=Vector3.right;
			Vector3 yu=Vector3.up;
			Vector3 zu=Vector3.forward;
			
			//Points
			Vector3 nw=sw+z;
			Vector3 ne=nw+x;
			Vector3 se=ne-z;
			//Add verticals
			OpalEdge e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(sw,up,index+4,index+2,z,x,-xu,-zu,eid);
			eid++;
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(nw,up,index+4,index,-z,x,-xu,zu,eid);
			eid++;
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(ne,up,index,index+5,-x,-z, zu,xu, eid);
			eid++;
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(se,up,index+5,index+2,z,-x,xu,-zu,eid);
			eid++;
			//Add horizontals at elevation
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(sw+up,z,index+4,index+1,-up,x,-xu,yu, eid);
			eid++;
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(nw+up,x,index,index+1,-up,-z,zu,yu, eid);
			eid++;
			e = cube.AddComponent<OpalEdge>();
			eid++;
			e.SetInfoValues(ne+up,-z,index+5,index+1,-up,-x,xu,yu,eid);
			eid++;
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(se+up,-x,index+2,index+1,-up,z,-zu,yu, eid);
			eid++;
		}
	}
}
