/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Opal
{
	public class OpalMeshProperties : MonoBehaviour
	{
		public bool sendToOpal=true;
		public bool sendWithFaces=true;
		public bool curved=false; 
		public MaterialEMProperties emProperties;
		public List<OpalTriangleFace> faceIds;
		public void SetFaceIds(List<OpalTriangleFace> l) {
			faceIds=l;
		}


	}
}
