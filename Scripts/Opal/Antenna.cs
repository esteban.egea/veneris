/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Veneris;

namespace Opal
{
	public class Antenna : MonoBehaviour {
		// Use this for initialization
		public int id=0;
		public float txPower=0.158f; //22 dBm
		public float radius=1f;
		public int transmissions=0;
		public bool receiver=true;
		public bool transmitter=true;
		public bool linkPolarizationToTransform = true;
		public Vector3 polarization = new Vector3(0.0f,1.0f,0.0f); //When linkPolarizationToTransform is true, the orientation of the rigibody will change the antenna orientation (and so the linear polarization) 
 		public bool useAntennaGain;
		public string antennaGainPath;
		protected Vector3ToMarshal polarizationM;
		protected GCHandle callbackHandle;
		protected bool registeredRx=false;
		protected bool registeredTx=false;

		public SphereCollider sc;

		public delegate void OnPowerReceivedId (int rxId, float power, int txId);
		public delegate void OnPowerReceived ( float power, int txId);
		protected OnPowerReceivedId onPowerReceivedIdListeners;
		protected OnPowerReceived powerReceivedCallback;


		void Start ()
		{

//
			//Debug.Log ("Start() Antenna: " + id + ". Polarization=" + polarization+"Tx Power="+txPower +"; receiver="+receiver+";transmitter="+transmitter);
			polarizationM = OpalInterface.ToMarshal(polarization);
			//Debug.DrawRay (transform.position, Vector3.forward*10f,Color.blue);
			//Debug.DrawRay (transform.position, Quaternion.Euler (45f, 0.0f, 0.0f) * Vector3.forward*10f, Color.red);
		}


		/*void FixedUpdate ()
		  {

		  var watch = System.Diagnostics.Stopwatch.StartNew ();
		  Transmit ();
		  watch.Stop ();
		  Debug.Log ("Time to transmit=" + (watch.ElapsedMilliseconds / 1000f).ToString("E") + " s");



		  }*/



		public void Transmit() {


			OpalManager.Instance.Transmit(id,txPower,transform.position,polarizationM);
			OpalManager.Instance.CallReceiverCallbacks(id, transmissions);	
			transmissions++;
			//OpalManager.Instance.Transmit(id,txPower,transform.position,polarization);

		}
		public void Transmit(float p) {
			Debug.Log (Time.time+"\t. Transmit:"+transform.position );
			OpalManager.Instance.Transmit(id,p,transform.position,polarizationM);
			transmissions++;
			//OpalManager.Instance.Transmit(id,txPower,transform.position,polarization);
		}
		public Vector3ToMarshal GetPolarizationMarshaled() {
			return polarizationM;
		}




		// Use this for initialization
		protected virtual void Awake ()
		{
			//For visualization purposes

			sc = GetComponent<SphereCollider> ();
			if (sc != null) {
				sc.radius = radius;
			}


			powerReceivedCallback = ReceivedPower;
			AllocateHandle ();

			polarizationM = OpalInterface.ToMarshal(polarization);
			Debug.Log ("Awake() Antenna: " + id + ". Polarization=" + polarization+"Tx Power="+txPower +"; receiver="+receiver+";transmitter="+transmitter);

		}
		public virtual void AllocateHandle() {
			if (powerReceivedCallback != null) {
				callbackHandle = GCHandle.Alloc (powerReceivedCallback);
			}
		}


		protected virtual void OnEnable() {
			if (receiver) {
				//Debug.Log (Time.time + ":Registering receiver " + id);
				if (registeredRx == false) {
					OpalManager.Instance.RegisterReceiver (this);
					registeredRx = true;
					transform.hasChanged = false;
				}
			}
			if (transmitter) {
				//Debug.Log (Time.time + ":Registering receiver " + id);
				if (registeredTx == false) {
					OpalManager.Instance.RegisterTransmitter (this);
					registeredTx = true;
					transform.hasChanged = false;
				}
			}



		}
		public void RegisterPowerListener(OnPowerReceivedId l) {
			onPowerReceivedIdListeners += l;
		}
		public void RemovePowerListener(OnPowerReceivedId l) {
			onPowerReceivedIdListeners -= l;
		}



		public HandleRef GetCallback ()
		{
			if (powerReceivedCallback != null) {
				//	return Marshal.GetFunctionPointerForDelegate (powerReceivedCallback);
				return new HandleRef(this, Marshal.GetFunctionPointerForDelegate (powerReceivedCallback));
			} else {
				throw new System.InvalidOperationException ("Callback null for receiver: " + id);
			}
		}




		public void recordPower (float power, int txId)
		{
			string m = Time.time+"\tRx[" + id + "]. Received p=" + power + " from " + txId;
			//SimulationManager.Instance.GetGeneralResultLogger ().Record (m);
		}
		public void printPower (float power, int txId)
		{
			string m = Time.time+"\tRx[" + id + "] ("+transform.position+","+radius+"). Received p=" + power + " from " + txId;

			Debug.Log (m);
		}
		public void ReceivedPower(float power, int txId) {
			printPower (power, txId);
			if (onPowerReceivedIdListeners != null) {
				//printPower (power, txId);
				//Invoke listeners
				onPowerReceivedIdListeners (id,power, txId);
			}
		}
		public void ReceivedE(int txId, float Exr,  float Exi,   float Eyr,  float Eyi,  float Ezr,  float Ezi  ) {
			string m = Time.time+"\tRx[" + id + "]. Received field components Ex=(" +Exr.ToString("E8")+","+Exi.ToString("E8")+"); Ey=("+Eyr.ToString("E8")+","+Eyi.ToString("E8")+"); Ey=("+Ezr.ToString("E8")+","+Ezi.ToString("E8")+"); from " + txId;
			Debug.Log(m);
		}
		public void UpdateTransform() {
			//Note that the transform may have been changed by other component prior to this call. If a call to Transmit() has been done in between, the power has been computed with the previous position
			transform.hasChanged = false;
			if (linkPolarizationToTransform) {
				polarizationM = OpalInterface.ToMarshal(transform.rotation*polarization);
				//Debug.Log("UpdateTransform(). polarization="+polarization+"rotation="+transform.rotation+"polarizationM.=("+polarizationM.x+","+polarizationM.y+","+polarizationM.z);
				Debug.DrawRay (transform.position, polarization*10f,Color.blue);

			}
			if (OpalManager.isInitialized && receiver) {
				OpalManager.Instance.UpdateReceiver (this);
			}
		}
		protected void FixedUpdate() {
			if (transform.hasChanged) {
				//Debug.Log ("transform has changed");

				//	Debug.Log (Time.time+"\t"+(transform.position - transmitter.position).magnitude );
				UpdateTransform ();
			}
		}
		protected virtual void OnDestroy ()
		{
			if (powerReceivedCallback != null) {
				callbackHandle.Free ();
			}
			if (OpalManager.isInitialized) {
				if (registeredRx) {
					Debug.Log (Time.time+": Removing receiver"+id+" on destroy");
					OpalManager.Instance.UnregisterReceiver (this);
					registeredRx = false;
				}
				if (registeredTx) {
					Debug.Log (Time.time+": Removing transmitter"+id+" on destroy");
					OpalManager.Instance.UnregisterTransmitter (this);
					registeredTx = false;
				}
			}
		}
		protected virtual void OnDisable ()
		{
			if (OpalManager.isInitialized) {
				if (registeredRx) {
					Debug.Log (Time.time+": Removing receiver"+id+" on destroy");
					OpalManager.Instance.UnregisterReceiver (this);
					registeredRx = false;
				}
				if (registeredTx) {
					Debug.Log (Time.time+": Removing transmitter"+id+" on destroy");
					OpalManager.Instance.UnregisterTransmitter (this);
					registeredTx = false;
				}
			}

		}
	}
}
