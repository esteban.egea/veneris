/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Veneris.Communications;
using FlatBuffers;

namespace Opal
{
	public class VenerisOpalManager : OpalManager
	{

	
		// Use this for initialization
		//void Start ()
		//{
		//	if (useOpal) {
		//		InitOpal ();
		//	} 
		//	isInitialized = true;
		//	//Register modules/vehicles, independently of using Opal
		//	if (cachedReceivers != null) {
		//		for (int i = 0; i < cachedReceivers.Count; i++) {
		//			RegisterReceiver (cachedReceivers [i]);
		//		}
		//	}
		//
		//	if (cachedDynamicMeshes != null) {
		//		for (int i = 0; i < cachedDynamicMeshes.Count; i++) {
		//			if (useOpal) {
		//				RegisterDynamicMesh (cachedDynamicMeshes [i]);
		//			} else {
		//				//Disable component
		//				cachedDynamicMeshes [i].enabled=false;
		//				Debug.Log ("Disabling Dynamic Mesh");
		//			}
		//		}
		//	}
		//	
		//}

		//public override void InitOpal ()
		//{
		//	//Enquee intialization 
		//	FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) + sizeof(int) * 3 + sizeof(bool));
		//	UseOpal.StartUseOpal (fbb);
		//	UseOpal.AddAzimuthDelta (fbb, (uint)raySphere.azimuthDelta);
		//	UseOpal.AddElevationDelta (fbb, (uint)raySphere.elevationDelta);
		//	UseOpal.AddFrequency (fbb, frequency);
		//	UseOpal.AddMaxReflections (fbb, maxReflections);
		//	UseOpal.AddUseDecimalDegrees (fbb, true);
		//	var uo = UseOpal.EndUseOpal (fbb);
		//	UseOpal.FinishUseOpalBuffer (fbb, uo);

		//	MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.UseOpal);
		//	Debug.Log ("Enqueuing UseOpal");

		//	CollectAndSendStaticMeshes ();

		//	MessageManager.enqueue (null, VenerisMessageTypes.FinishOpalContext);
		//	Debug.Log ("Enqueuing FinishOpalContext");

		//
	
		//
		//}
		public override int SendFinishContext() {
			if (useOpal) {
				MessageManager.enqueue (null, VenerisMessageTypes.FinishOpalContext);
				Debug.Log ("Enqueuing FinishOpalContext");
			}
			return 0;
		}
		public override void SendInit( float frequency, int simType,  int computeMode,  bool useExactSpeedOfLight,  bool useDiffraction,  bool enableFastMath,  bool generateRaysOnLaunch,  bool enableMultiGPU,  bool logTrace,  bool enableMultitransmitter,  bool useAntennaGain, uint m, float minE) {
			if (useOpal) {
				//Enquee intialization 
				FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) + sizeof(int) * 2 + sizeof(uint)  + sizeof(bool)*8);
				ConfigOpal.StartConfigOpal (fbb);
				ConfigOpal.AddFrequency (fbb, frequency);
				ConfigOpal.AddSimType(fbb,simType);
				ConfigOpal.AddComputeMode(fbb,computeMode);
				ConfigOpal.AddMaxReflections(fbb,m);
				ConfigOpal.AddUseExactSpeedOfLight(fbb,useExactSpeedOfLight);
				ConfigOpal.AddUseDiffraction(fbb,useDiffraction);
				ConfigOpal.AddEnableFastMath(fbb,enableFastMath);
				ConfigOpal.AddGenerateRaysOnLaunch(fbb,generateRaysOnLaunch);
				ConfigOpal.AddMultiGPU(fbb,multiGPU);
				ConfigOpal.AddLogTrace(fbb,logTrace);
				ConfigOpal.AddMultiTransmitter(fbb,multiTransmitter);
				ConfigOpal.AddUseAntennaGain(fbb,useAntennaGain);
				ConfigOpal.AddMinEpsilon(fbb,minE);
				var uo = ConfigOpal.EndConfigOpal (fbb);
				ConfigOpal.FinishConfigOpalBuffer (fbb, uo);
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.ConfigOpal);
			}
		}
		public override void CollectAndSendStaticMeshes ()
		{
			if (useOpal) {
				base.CollectAndSendStaticMeshes();
			}
		}
		public override void CollectAndSendEdges ()
		{
			if (useOpal) {
				base.CollectAndSendEdges();
			}
		}	
		public override int SendEdgeToGroup ( Vector3ToMarshal p,  Vector3ToMarshal v,  uint faid,  uint fbid, Vector3ToMarshal face_a,  Vector3ToMarshal face_b, Vector3ToMarshal normal_a,  Vector3ToMarshal normal_b,  MaterialEMProperties emProperties,  int id, int groupId)  {
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 *6 + sizeof(uint) * 2 + sizeof(float) * 4 + 2*sizeof(int));
			Veneris.Communications.OpalEdgeToGroup.StartOpalEdgeToGroup(fbb);
			Veneris.Communications.OpalEdgeToGroup.AddP (fbb, Vec3.CreateVec3 (fbb, p.x, p.y, p.z));
			Veneris.Communications.OpalEdgeToGroup.AddV (fbb, Vec3.CreateVec3 (fbb, v.x, v.y, v.z));
			Veneris.Communications.OpalEdgeToGroup.AddFaceA (fbb, Vec3.CreateVec3 (fbb, face_a.x, face_a.y, face_a.z));
			Veneris.Communications.OpalEdgeToGroup.AddFaceB (fbb, Vec3.CreateVec3 (fbb, face_b.x, face_b.y, face_b.z));
			Veneris.Communications.OpalEdgeToGroup.AddNormalA(fbb, Vec3.CreateVec3 (fbb, normal_a.x, normal_a.y, normal_a.z));
			Veneris.Communications.OpalEdgeToGroup.AddNormalB(fbb, Vec3.CreateVec3 (fbb, normal_b.x, normal_b.y, normal_b.z));
			Veneris.Communications.OpalEdgeToGroup.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, emProperties.a, emProperties.b, emProperties.c, emProperties.d));
			Veneris.Communications.OpalEdgeToGroup.AddFaid(fbb,faid );
			Veneris.Communications.OpalEdgeToGroup.AddFbid(fbb,fbid );
			Veneris.Communications.OpalEdgeToGroup.AddId(fbb,id );
			Veneris.Communications.OpalEdgeToGroup.AddGroupId(fbb,groupId );
				var uo = Veneris.Communications.OpalEdgeToGroup.EndOpalEdgeToGroup (fbb);
				Veneris.Communications.OpalEdgeToGroup.FinishOpalEdgeToGroupBuffer (fbb, uo);
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.OpalEdgeToGroup);
			return 0;
}
		public override int SendEdge ( Vector3ToMarshal p,  Vector3ToMarshal v,  uint faid,  uint fbid, Vector3ToMarshal face_a,  Vector3ToMarshal face_b, Vector3ToMarshal normal_a,  Vector3ToMarshal normal_b,  MaterialEMProperties emProperties,  int id)  {
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 *6 + sizeof(uint) * 2 + sizeof(float) * 4 + sizeof(int));
			Veneris.Communications.OpalEdge.StartOpalEdge(fbb);
			Veneris.Communications.OpalEdge.AddP (fbb, Vec3.CreateVec3 (fbb, p.x, p.y, p.z));
			Veneris.Communications.OpalEdge.AddV (fbb, Vec3.CreateVec3 (fbb, v.x, v.y, v.z));
			Veneris.Communications.OpalEdge.AddFaceA (fbb, Vec3.CreateVec3 (fbb, face_a.x, face_a.y, face_a.z));
			Veneris.Communications.OpalEdge.AddFaceB (fbb, Vec3.CreateVec3 (fbb, face_b.x, face_b.y, face_b.z));
			Veneris.Communications.OpalEdge.AddNormalA(fbb, Vec3.CreateVec3 (fbb, normal_a.x, normal_a.y, normal_a.z));
			Veneris.Communications.OpalEdge.AddNormalB(fbb, Vec3.CreateVec3 (fbb, normal_b.x, normal_b.y, normal_b.z));
			Veneris.Communications.OpalEdge.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, emProperties.a, emProperties.b, emProperties.c, emProperties.d));
			Veneris.Communications.OpalEdge.AddFaid(fbb,faid );
			Veneris.Communications.OpalEdge.AddFbid(fbb,fbid );
			Veneris.Communications.OpalEdge.AddId(fbb,id );
				var uo = Veneris.Communications.OpalEdge.EndOpalEdge (fbb);
				Veneris.Communications.OpalEdge.FinishOpalEdgeBuffer (fbb, uo);
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.OpalEdge);
			return 0;
		}


		public override void OnDestroy ()
		{
	
				Debug.Log ("Exiting VenerisOpal");
				MessageManager.enqueue (null, VenerisMessageTypes.End);
				isInitialized = false;

		}
		/*public override void Transmit (int id, float txPower, Vector3ToMarshal txPosition, Vector3ToMarshal polarization)
		{
			Debug.LogError ("VenerisOpalManager does not transmit. Transmission is delegated to Veneris (OMNET++)");
		}
		*/
		public int SendLoadAndRegisterGain (string path) {
			//TODO: we are sending a path to a file. It is not going to work if we are using a server and the path does not exist in the server
			//At the moment, make sure the path exists in the server side
			if (useOpal) {
				byte[] b=System.Text.Encoding.ASCII.GetBytes(path);
				FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(byte)*b.Length);
				Veneris.Communications.RegisterGain.StartPathVector(fbb,b.Length);
				for (int i = b.Length - 1; i >= 0; i--) {
					fbb.AddByte(b[i]);
				}
				var bof=fbb.EndVector();	
				Veneris.Communications.RegisterGain.StartRegisterGain(fbb);	
				Veneris.Communications.RegisterGain.AddPath(fbb,bof);	
				var rof=Veneris.Communications.RegisterGain.EndRegisterGain(fbb);
				Veneris.Communications.RegisterGain.FinishRegisterGainBuffer(fbb,rof);	
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.RegisterGain);

			}
			return 0;
		}
		public override int SendSetRayRange ( float initElevation,  float endElevation,  float initAzimuth,   float endAzimuth,  int rayD) {
			if (useOpal) {
				FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float)*6);
				Veneris.Communications.RDNRayRange.StartRDNRayRange(fbb);
				Veneris.Communications.RDNRayRange.AddInitElevation(fbb,initElevation);	
				Veneris.Communications.RDNRayRange.AddEndElevation(fbb,endElevation);	
				Veneris.Communications.RDNRayRange.AddInitAzimuth(fbb,initAzimuth);	
				Veneris.Communications.RDNRayRange.AddEndAzimuth(fbb,endAzimuth);
				Veneris.Communications.RDNRayRange.AddRayD(fbb,rayD);
				var vof = Veneris.Communications.RDNRayRange.EndRDNRayRange(fbb);
				Veneris.Communications.RDNRayRange.FinishRDNRayRangeBuffer(fbb,vof);	
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.RDNRayRange);
			}
			return 0;
		}
		public override int SendCreateRaySphereRange ( float initElevation,  float elevationDelta,  float endElevation,  float initAzimuth,  float azimuthDelta,  float endAzimuth) {
			if (useOpal) {
				FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float)*6);
				Veneris.Communications.RaySphereRange.StartRaySphereRange(fbb);
				Veneris.Communications.RaySphereRange.AddInitElevation(fbb,initElevation);	
				Veneris.Communications.RaySphereRange.AddElevationDelta(fbb,elevationDelta);	
				Veneris.Communications.RaySphereRange.AddEndElevation(fbb,endElevation);	
				Veneris.Communications.RaySphereRange.AddInitAzimuth(fbb,initAzimuth);	
				Veneris.Communications.RaySphereRange.AddAzimuthDelta(fbb,azimuthDelta);	
				Veneris.Communications.RaySphereRange.AddEndAzimuth(fbb,endAzimuth);
				var vof = Veneris.Communications.RaySphereRange.EndRaySphereRange(fbb);
				Veneris.Communications.RaySphereRange.FinishRaySphereRangeBuffer(fbb,vof);	
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.RaySphereRange);
			}
			return 0;
		}
		public virtual int SendMesh ( int meshVertexCount,  Vector3ToMarshal[] v,  int meshTriangleCount, int[] indices,  Matrix4x4ToMarshal u,  MaterialEMProperties emProperties) {
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 * v.Length + sizeof(int) * indices.Length + sizeof(float) * 20);


			//Keep the order of triangles an vertices. Loop in forward or reverse order for both of them
			Veneris.Communications.StaticMesh.StartVerticesVector (fbb, v.Length);
			for (int i = v.Length - 1; i >= 0; i--) {
				float x = v [i].x;
				float y = v [i].y;
				float z = v [i].z;
				/*Debug.Log (v [i].x.ToString ("E8"));
				Debug.Log (v [i].y.ToString ("E8"));
				Debug.Log (v [i].z.ToString ("E8"));
				*/
				if (Mathf.Abs (v [i].x) < 1E-15f) {
					x = 0.0f;
				}
				if (Mathf.Abs (v [i].y) < 1E-15f) {
					y = 0.0f;
				}
				if (Mathf.Abs (v [i].z) < 1E-15f) {
					z = 0.0f;
				}
				Vec3.CreateVec3 (fbb, x, y, z);
			}
			var vof = fbb.EndVector ();

		
			Veneris.Communications.StaticMesh.StartIndexesVector (fbb, indices.Length);
			for (int i = indices.Length - 1; i >= 0; i--) {
				fbb.AddInt (indices [i]);
			}
			var iof = fbb.EndVector ();
			//Now serialize all the fields
			Veneris.Communications.StaticMesh.StartStaticMesh (fbb);
			Veneris.Communications.StaticMesh.AddVertices (fbb, vof);
			Veneris.Communications.StaticMesh.AddIndexes (fbb, iof);
			Veneris.Communications.StaticMesh.AddTransform (fbb, Veneris.Communications.Matrix4x4.CreateMatrix4x4 (fbb, u.m00, u.m01, u.m02, u.m03, u.m10, u.m11, u.m12, u.m13, u.m20, u.m21, u.m22, u.m23, u.m30, u.m31, u.m32, u.m33));
			Veneris.Communications.StaticMesh.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, emProperties.a, emProperties.b, emProperties.c, emProperties.d));
			var stof = Veneris.Communications.StaticMesh.EndStaticMesh (fbb);

			Veneris.Communications.StaticMesh.FinishStaticMeshBuffer (fbb, stof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.StaticMesh);
			return 0;
		
		}
		public override int SendStaticCurvedMesh ( int meshVertexCount,  Vector3ToMarshal[] v,  int meshTriangleCount, int[] indices, int pd1Count, Vector4ToMarshal[] pd1, int pd2Count, Vector4ToMarshal[] pd2,  Matrix4x4ToMarshal u,  MaterialEMProperties emProperties, bool makeSingleFace, int faceId) {
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 * v.Length + sizeof(int) * indices.Length + sizeof(float) * 20 + sizeof(float)*4*2*pd1.Length + sizeof(bool) + sizeof(int));


			//Keep the order of triangles an vertices. Loop in forward or reverse order for both of them
			Veneris.Communications.StaticCurvedMesh.StartVerticesVector (fbb, v.Length);
			for (int i = v.Length - 1; i >= 0; i--) {
				float x = v [i].x;
				float y = v [i].y;
				float z = v [i].z;
				/*Debug.Log (v [i].x.ToString ("E8"));
				Debug.Log (v [i].y.ToString ("E8"));
				Debug.Log (v [i].z.ToString ("E8"));
				*/
				if (Mathf.Abs (v [i].x) < 1E-15f) {
					x = 0.0f;
				}
				if (Mathf.Abs (v [i].y) < 1E-15f) {
					y = 0.0f;
				}
				if (Mathf.Abs (v [i].z) < 1E-15f) {
					z = 0.0f;
				}
				Vec3.CreateVec3 (fbb, x, y, z);
			}
			var vof = fbb.EndVector ();

		
			Veneris.Communications.StaticCurvedMesh.StartIndexesVector (fbb, indices.Length);
			for (int i = indices.Length - 1; i >= 0; i--) {
				fbb.AddInt (indices [i]);
			}
			var iof = fbb.EndVector ();
			Veneris.Communications.StaticCurvedMesh.StartPd1Vector (fbb, pd1.Length);
			for (int i = pd1.Length - 1; i >= 0; i--) {
				float x = pd1 [i].x;
				float y = pd1 [i].y;
				float z = pd1 [i].z;
				float w = pd1 [i].w;
				/*Debug.Log (v [i].x.ToString ("E8"));
				Debug.Log (v [i].y.ToString ("E8"));
				Debug.Log (v [i].z.ToString ("E8"));
				*/
				if (Mathf.Abs (pd1 [i].x) < 1E-15f) {
					x = 0.0f;
				}
				if (Mathf.Abs (pd1 [i].y) < 1E-15f) {
					y = 0.0f;
				}
				if (Mathf.Abs (pd1 [i].z) < 1E-15f) {
					z = 0.0f;
				}
				if (Mathf.Abs (pd1 [i].z) < 1E-15f) {
					w=0.0f;
				}
				Vec4.CreateVec4 (fbb, x, y, z,w);
			}
			var pd1of = fbb.EndVector ();
			Veneris.Communications.StaticCurvedMesh.StartPd2Vector (fbb, pd2.Length);
			for (int i = pd2.Length - 1; i >= 0; i--) {
				float x = pd2[i].x;
				float y = pd2[i].y;
				float z = pd2[i].z;
				float w = pd2[i].w;
				/*Debug.Log (v [i].x.ToString ("E8"));
				Debug.Log (v [i].y.ToString ("E8"));
				Debug.Log (v [i].z.ToString ("E8"));
				*/
				if (Mathf.Abs (pd2 [i].x) < 1E-15f) {
					x = 0.0f;
				}
				if (Mathf.Abs (pd2 [i].y) < 1E-15f) {
					y = 0.0f;
				}
				if (Mathf.Abs (pd2 [i].z) < 1E-15f) {
					z = 0.0f;
				}
				if (Mathf.Abs (pd2 [i].z) < 1E-15f) {
					w=0.0f;
				}
				Vec4.CreateVec4 (fbb, x, y, z,w);
			}
			var pd2of = fbb.EndVector ();

			//Now serialize all the fields
			Veneris.Communications.StaticCurvedMesh.StartStaticCurvedMesh (fbb);
			Veneris.Communications.StaticCurvedMesh.AddVertices (fbb, vof);
			Veneris.Communications.StaticCurvedMesh.AddIndexes (fbb, iof);
			Veneris.Communications.StaticCurvedMesh.AddTransform (fbb, Veneris.Communications.Matrix4x4.CreateMatrix4x4 (fbb, u.m00, u.m01, u.m02, u.m03, u.m10, u.m11, u.m12, u.m13, u.m20, u.m21, u.m22, u.m23, u.m30, u.m31, u.m32, u.m33));
			Veneris.Communications.StaticCurvedMesh.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, emProperties.a, emProperties.b, emProperties.c, emProperties.d));
			Veneris.Communications.StaticCurvedMesh.AddPd1 (fbb, pd1of);
			Veneris.Communications.StaticCurvedMesh.AddPd2 (fbb, pd2of);
			Veneris.Communications.StaticCurvedMesh.AddSingleFace (fbb, makeSingleFace);
			Veneris.Communications.StaticCurvedMesh.AddFaceId (fbb, faceId);
			

			var stof = Veneris.Communications.StaticCurvedMesh.EndStaticCurvedMesh (fbb);

			Veneris.Communications.StaticCurvedMesh.FinishStaticCurvedMeshBuffer (fbb, stof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.StaticCurvedMesh);
			return 0;
		
}
		public override int SendStaticMeshWithFaces ( int meshVertexCount,  Vector3ToMarshal[] v,  int meshTriangleCount, int[] indices, int faceIdCount, int[] faceIds,  Matrix4x4ToMarshal u,  MaterialEMProperties emProperties) {
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 * v.Length + sizeof(int) * indices.Length + sizeof(float) * 20 + sizeof(int)*faceIds.Length);


			//Keep the order of triangles an vertices. Loop in forward or reverse order for both of them
			Veneris.Communications.StaticMeshWithFaces.StartVerticesVector (fbb, v.Length);
			for (int i = v.Length - 1; i >= 0; i--) {
				float x = v [i].x;
				float y = v [i].y;
				float z = v [i].z;
				/*Debug.Log (v [i].x.ToString ("E8"));
				Debug.Log (v [i].y.ToString ("E8"));
				Debug.Log (v [i].z.ToString ("E8"));
				*/
				if (Mathf.Abs (v [i].x) < 1E-15f) {
					x = 0.0f;
				}
				if (Mathf.Abs (v [i].y) < 1E-15f) {
					y = 0.0f;
				}
				if (Mathf.Abs (v [i].z) < 1E-15f) {
					z = 0.0f;
				}
				Vec3.CreateVec3 (fbb, x, y, z);
			}
			var vof = fbb.EndVector ();

		
			Veneris.Communications.StaticMeshWithFaces.StartIndexesVector (fbb, indices.Length);
			for (int i = indices.Length - 1; i >= 0; i--) {
				fbb.AddInt (indices [i]);
			}
			var iof = fbb.EndVector ();
			Veneris.Communications.StaticMeshWithFaces.StartFaceIdsVector (fbb, faceIds.Length);
			for (int i = faceIds.Length - 1; i >= 0; i--) {
				fbb.AddInt (faceIds [i]);
			}
			var fiof=fbb.EndVector();	
			
			//Now serialize all the fields
			Veneris.Communications.StaticMeshWithFaces.StartStaticMeshWithFaces (fbb);
			Veneris.Communications.StaticMeshWithFaces.AddVertices (fbb, vof);
			Veneris.Communications.StaticMeshWithFaces.AddIndexes (fbb, iof);
			Veneris.Communications.StaticMeshWithFaces.AddTransform (fbb, Veneris.Communications.Matrix4x4.CreateMatrix4x4 (fbb, u.m00, u.m01, u.m02, u.m03, u.m10, u.m11, u.m12, u.m13, u.m20, u.m21, u.m22, u.m23, u.m30, u.m31, u.m32, u.m33));
			Veneris.Communications.StaticMeshWithFaces.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, emProperties.a, emProperties.b, emProperties.c, emProperties.d));
			Veneris.Communications.StaticMeshWithFaces.AddFaceIds(fbb,fiof);
			var stof = Veneris.Communications.StaticMeshWithFaces.EndStaticMeshWithFaces (fbb);
			Veneris.Communications.StaticMeshWithFaces.FinishStaticMeshWithFacesBuffer (fbb, stof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.StaticMeshWithFaces);
			return 0;
		}
		//protected override void SendStaticMeshToOpal (Transform t, StaticMesh sm)
		//{
		//	
		//	Vector3[] v = sm.meshFilter.mesh.vertices;
		//	int[] indices = sm.meshFilter.mesh.triangles;
		//	Debug.Log (t.name + ".Triangle length=" + indices.Length);

		//	FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 * v.Length + sizeof(int) * indices.Length + sizeof(float) * 20);


		//	//Keep the order of triangles an vertices. Loop in forward or reverse order for both of them
		//	Veneris.Communications.StaticMesh.StartVerticesVector (fbb, v.Length);
		//	for (int i = v.Length - 1; i >= 0; i--) {
		//		float x = v [i].x;
		//		float y = v [i].y;
		//		float z = v [i].z;
		//		/*Debug.Log (v [i].x.ToString ("E8"));
		//		Debug.Log (v [i].y.ToString ("E8"));
		//		Debug.Log (v [i].z.ToString ("E8"));
		//		*/
		//		if (Mathf.Abs (v [i].x) < 1E-15f) {
		//			x = 0.0f;
		//		}
		//		if (Mathf.Abs (v [i].y) < 1E-15f) {
		//			y = 0.0f;
		//		}
		//		if (Mathf.Abs (v [i].z) < 1E-15f) {
		//			z = 0.0f;
		//		}
		//		Vec3.CreateVec3 (fbb, x, y, z);
		//	}
		//	var vof = fbb.EndVector ();

		//
		//	Veneris.Communications.StaticMesh.StartIndexesVector (fbb, indices.Length);
		//	for (int i = indices.Length - 1; i >= 0; i--) {
		//		fbb.AddInt (indices [i]);
		//	}
		//	var iof = fbb.EndVector ();
	
		//
		//	UnityEngine.Matrix4x4 u = t.transform.localToWorldMatrix;
		//	//var tof=Veneris.Communications.Matrix4x4.CreateMatrix4x4(fbb, u [0, 0], u [0, 1], u [0, 2], u [0, 3], u [1, 0],u [1, 1],u [1, 2], u [1, 3], u [2, 0], u [2, 1], u[2, 2], u [2, 3],u [3, 0], u [3, 1], u [3, 2],u [3, 3]);
		//
		//	//	var emof=Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, sm.GetOpalMeshProperties ().emProperties.a, sm.GetOpalMeshProperties ().emProperties.b, sm.GetOpalMeshProperties ().emProperties.c, sm.GetOpalMeshProperties ().emProperties.d);

		//	//Now serialize all the fields
		//	Veneris.Communications.StaticMesh.StartStaticMesh (fbb);
		//	Veneris.Communications.StaticMesh.AddVertices (fbb, vof);
		//	Veneris.Communications.StaticMesh.AddIndexes (fbb, iof);
		//	Veneris.Communications.StaticMesh.AddTransform (fbb, Veneris.Communications.Matrix4x4.CreateMatrix4x4 (fbb, u [0, 0], u [0, 1], u [0, 2], u [0, 3], u [1, 0], u [1, 1], u [1, 2], u [1, 3], u [2, 0], u [2, 1], u [2, 2], u [2, 3], u [3, 0], u [3, 1], u [3, 2], u [3, 3]));
		//	Veneris.Communications.StaticMesh.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, sm.GetOpalMeshProperties ().emProperties.a, sm.GetOpalMeshProperties ().emProperties.b, sm.GetOpalMeshProperties ().emProperties.c, sm.GetOpalMeshProperties ().emProperties.d));
		//	var stof = Veneris.Communications.StaticMesh.EndStaticMesh (fbb);

		//	Veneris.Communications.StaticMesh.FinishStaticMeshBuffer (fbb, stof);
		//	MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.StaticMesh);
		//	//Debug.Log ("Enqueuing StaticMesh");
		//}

		public override int SendRegisterReceiverGain ( int rxId,  int gainId) {
			if (useOpal) {
				FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(int)*2);
				Veneris.Communications.RegisterReceiverGain.StartRegisterReceiverGain(fbb);
				Veneris.Communications.RegisterReceiverGain.AddRxId(fbb,rxId);
				Veneris.Communications.RegisterReceiverGain.AddGainId(fbb,gainId);
				var cvo = Veneris.Communications.RegisterReceiverGain.EndRegisterReceiverGain(fbb);
				Veneris.Communications.RegisterReceiverGain.FinishRegisterReceiverGainBuffer(fbb,cvo);
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.RegisterReceiverGain);
			}
			return 0;
		}
		public override void SendReceiverToOpal (Antenna rec)
		{
			
			//Send a new vehicle 
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 + sizeof(uint));
			CreateVehicle.StartCreateVehicle (fbb);
			CreateVehicle.AddId (fbb, (uint)rec.id);
			CreateVehicle.AddPosition (fbb, Vec3.CreateVec3 (fbb, rec.transform.position.x, rec.transform.position.y, rec.transform.position.z));
			CreateVehicle.AddPolarization (fbb, Vec3.CreateVec3 (fbb, rec.polarization.x, rec.polarization.y, rec.polarization.z));
			CreateVehicle.AddRadius (fbb, rec.radius);
			var cvo = CreateVehicle.EndCreateVehicle (fbb);
			CreateVehicle.FinishCreateVehicleBuffer (fbb, cvo);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.Create);
			//Debug.Log ("Enqueuing receiver (Create) " + rec.id + ". radius=" + rec.radius);

		}
		public override int SendRegisterTransmitterGain ( int txId,  int gainId) {
			if (useOpal) {
				FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(int)*2);
				Veneris.Communications.RegisterTransmitterGain.StartRegisterTransmitterGain(fbb);
				Veneris.Communications.RegisterTransmitterGain.AddTxId(fbb,txId);
				Veneris.Communications.RegisterTransmitterGain.AddGainId(fbb,gainId);
				var cvo = Veneris.Communications.RegisterTransmitterGain.EndRegisterTransmitterGain(fbb);
				Veneris.Communications.RegisterTransmitterGain.FinishRegisterTransmitterGainBuffer(fbb,cvo);
				MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.RegisterTransmitterGain);
			}
			return 0;
		}
		public override  int  SendRegisterTransmitter( int txId,  Vector3ToMarshal origin, Vector3ToMarshal  polarization,  float transmitPower) {
			//With Veneris a vehicle is already a transmitter
			return 0;
		} 

		public override void UpdateReceiver (Antenna rec)
		{
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 * 2 + sizeof(uint));
			VehicleState.StartVehicleState (fbb);
			VehicleState.AddId (fbb, (uint)rec.id);
			VehicleState.AddPosition (fbb, Vec3.CreateVec3 (fbb, rec.transform.position.x, rec.transform.position.y, rec.transform.position.z));
			Rigidbody rb = rec.GetComponent<Rigidbody> ();
			if (rb != null) {
				VehicleState.AddVelocity (fbb, Vec3.CreateVec3 (fbb, rb.velocity.x, rb.velocity.y, rb.velocity.z));

			}
			var vso = VehicleState.EndVehicleState (fbb);
			VehicleState.FinishVehicleStateBuffer (fbb, vso);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.VehicleState);
			//Debug.Log ("Enqueuing UpdateReceiver " + rec.id);
			
		}

		public override void UnregisterReceiver (Antenna rec)
		{
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(int));
			DestroyVehicle.StartDestroyVehicle (fbb);
			DestroyVehicle.AddId (fbb, rec.id);
			var dvo = DestroyVehicle.EndDestroyVehicle (fbb);
			DestroyVehicle.FinishDestroyVehicleBuffer (fbb, dvo);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.Destroy);
			//Debug.Log ("Enqueuing Destroy Vehicle " + rec.id);

		}

		public override void RemoveDynamicMeshGroup (int id)
		{
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(int));
			Veneris.Communications.RemoveDynamicMeshGroup.StartRemoveDynamicMeshGroup (fbb);
			Veneris.Communications.RemoveDynamicMeshGroup.AddId (fbb, id);
			var rdo = Veneris.Communications.RemoveDynamicMeshGroup.EndRemoveDynamicMeshGroup (fbb);
			Veneris.Communications.RemoveDynamicMeshGroup.FinishRemoveDynamicMeshGroupBuffer (fbb, rdo);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.RemoveDynamicMeshGroup);
			//Debug.Log ("Enqueuing RemoveDynamicMeshGroup" + id);


		}

		public override void AddDynamicMeshGroup (int id)
		{

			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(int));
			Veneris.Communications.AddDynamicMeshGroup.StartAddDynamicMeshGroup (fbb);
			Veneris.Communications.AddDynamicMeshGroup.AddId (fbb, id);
			var adof = Veneris.Communications.AddDynamicMeshGroup.EndAddDynamicMeshGroup (fbb);
			Veneris.Communications.AddDynamicMeshGroup.FinishAddDynamicMeshGroupBuffer (fbb, adof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.AddDynamicMeshGroup);
			//Debug.Log ("Enqueueing AddDynamicMeshGroup:" + id);	
		
		}

		public override void FinishDynamicMeshGroup (int id)
		{
		
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(int));

			Veneris.Communications.FinishDynamicMeshGroup.StartFinishDynamicMeshGroup (fbb);
			Veneris.Communications.FinishDynamicMeshGroup.AddId (fbb, id);
			var adof = Veneris.Communications.FinishDynamicMeshGroup.EndFinishDynamicMeshGroup (fbb);
			Veneris.Communications.FinishDynamicMeshGroup.FinishFinishDynamicMeshGroupBuffer (fbb, adof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.FinishDynamicMeshGroup);
			//Debug.Log ("Enqueueing FinishDynamicMeshGroup:" + id);	
			
		}

		public override void UpdateTransformInGroup (int id, Transform t)
		{
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 16 + sizeof(int));
			Veneris.Communications.UpdateTransformInGroup.StartUpdateTransformInGroup (fbb);
			Veneris.Communications.UpdateTransformInGroup.AddId (fbb, id);
			UnityEngine.Matrix4x4 u = t.localToWorldMatrix;
			Veneris.Communications.UpdateTransformInGroup.AddTransform (fbb, Veneris.Communications.Matrix4x4.CreateMatrix4x4 (fbb, u [0, 0], u [0, 1], u [0, 2], u [0, 3], u [1, 0], u [1, 1], u [1, 2], u [1, 3], u [2, 0], u [2, 1], u [2, 2], u [2, 3], u [3, 0], u [3, 1], u [3, 2], u [3, 3]));
			var stof = Veneris.Communications.UpdateTransformInGroup.EndUpdateTransformInGroup (fbb);
			Veneris.Communications.UpdateTransformInGroup.FinishUpdateTransformInGroupBuffer (fbb, stof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.UpdateTransformInGroup);
			//Debug.Log ("Enqueueing UpdateTransformInGroup:" + id);	
			
		}
		public override int SendMeshWithFacesToGroup (int id, Vector3ToMarshal[] v, int[] indices, int faceIdCount, int[] faceIds, MaterialEMProperties em) {
			//Debug.Log ("Mesh Group:" + id + ". " + meshT.name + ".Triangle length=" + indices.Length);
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 * v.Length + sizeof(int) * indices.Length + sizeof(float) * 4 + sizeof(int));


			//Keep the order of triangles an vertices. Loop in forward or reverse order for both of them
			Veneris.Communications.DynamicMeshWithFaces.StartVerticesVector (fbb, v.Length);
			for (int i = v.Length - 1; i >= 0; i--) {
				float x = v[i].x;
				float y = v[i].y;
				float z = v[i].z;
				/*Debug.Log (x.ToString ("E8"));
				Debug.Log (y.ToString ("E8"));
				Debug.Log (z.ToString ("E8"));
				*/

				if (Mathf.Abs (v [i].x) < 1E-15f) {
					x = 0.0f;
				}
				if (Mathf.Abs (v [i].y) < 1E-15f) {
					y = 0.0f;
				}
				if (Mathf.Abs (v [i].z) < 1E-15f) {
					z = 0.0f;
				}
				Vec3.CreateVec3 (fbb, x, y, z);
			}
			var vof = fbb.EndVector ();


			Veneris.Communications.DynamicMeshWithFaces.StartIndexesVector (fbb, indices.Length);
			for (int i = indices.Length - 1; i >= 0; i--) {
				fbb.AddInt (indices [i]);
			}
			var iof = fbb.EndVector ();
			Veneris.Communications.DynamicMeshWithFaces.StartFaceIdsVector (fbb, faceIds.Length);
			for (int i = faceIds.Length - 1; i >= 0; i--) {
				fbb.AddInt (faceIds [i]);
			}
			var fiof=fbb.EndVector();	

			//Now serialize all the fields

			Veneris.Communications.DynamicMeshWithFaces.StartDynamicMeshWithFaces (fbb);
			Veneris.Communications.DynamicMeshWithFaces.AddId (fbb, id);
			Veneris.Communications.DynamicMeshWithFaces.AddVertices (fbb, vof);
			Veneris.Communications.DynamicMeshWithFaces.AddIndexes (fbb, iof);
			Veneris.Communications.DynamicMeshWithFaces.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, em.a, em.b, em.c, em.d));
			Veneris.Communications.DynamicMeshWithFaces.AddFaceIds (fbb, fiof);

			var stof = Veneris.Communications.DynamicMeshWithFaces.EndDynamicMeshWithFaces (fbb);

			Veneris.Communications.DynamicMeshWithFaces.FinishDynamicMeshWithFacesBuffer (fbb, stof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.DynamicMeshWithFaces);
			return 0;
		}

		public override void SendMeshToGroup (int id, Transform rootT, Transform meshT, Mesh mesh, MaterialEMProperties em)
		{
			Vector3[] v = mesh.vertices;
			int[] indices = mesh.triangles;
			//Debug.Log ("Mesh Group:" + id + ". " + meshT.name + ".Triangle length=" + indices.Length);
			FlatBufferBuilder fbb = new FlatBufferBuilder (sizeof(float) * 3 * v.Length + sizeof(int) * indices.Length + sizeof(float) * 4 + sizeof(int));


			//Keep the order of triangles an vertices. Loop in forward or reverse order for both of them
			Veneris.Communications.DynamicMesh.StartVerticesVector (fbb, v.Length);
			for (int i = v.Length - 1; i >= 0; i--) {
				Vector3 aux = rootT.InverseTransformPoint (meshT.TransformPoint (v [i]));
				float x = aux.x;
				float y = aux.y;
				float z = aux.z;
				/*Debug.Log (x.ToString ("E8"));
				Debug.Log (y.ToString ("E8"));
				Debug.Log (z.ToString ("E8"));
				*/

				if (Mathf.Abs (v [i].x) < 1E-15f) {
					x = 0.0f;
				}
				if (Mathf.Abs (v [i].y) < 1E-15f) {
					y = 0.0f;
				}
				if (Mathf.Abs (v [i].z) < 1E-15f) {
					z = 0.0f;
				}
				Vec3.CreateVec3 (fbb, x, y, z);
			}
			var vof = fbb.EndVector ();


			Veneris.Communications.DynamicMesh.StartIndexesVector (fbb, indices.Length);
			for (int i = indices.Length - 1; i >= 0; i--) {
				fbb.AddInt (indices [i]);
			}
			var iof = fbb.EndVector ();

			//Now serialize all the fields

			Veneris.Communications.DynamicMesh.StartDynamicMesh (fbb);
			Veneris.Communications.DynamicMesh.AddId (fbb, id);
			Veneris.Communications.DynamicMesh.AddVertices (fbb, vof);
			Veneris.Communications.DynamicMesh.AddIndexes (fbb, iof);
			Veneris.Communications.DynamicMesh.AddMaterial (fbb, Veneris.Communications.MaterialEMP.CreateMaterialEMP (fbb, em.a, em.b, em.c, em.d));

			var stof = Veneris.Communications.DynamicMesh.EndDynamicMesh (fbb);

			Veneris.Communications.DynamicMesh.FinishDynamicMeshBuffer (fbb, stof);
			MessageManager.enqueue (fbb.SizedByteArray (), VenerisMessageTypes.DynamicMesh);
			//Debug.Log ("Enqueuing DynamicMesh: " + id);
		}
	}
}
