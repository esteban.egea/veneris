﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Opal
{
	public class MeshCurvatureData : MonoBehaviour
	{
		public string pathToFile;
		public bool exportToFile=false;
		public bool showData=false;
		public int pd1Size;
		public int pd2Size;
		public List<Vector4> principalDirection1;
		public List<Vector4> principalDirection2;
		public List<Vector3> centers;
		public List<Vector4> getPd1() {
			return principalDirection1;
		}
		public List<Vector4> getPd2() {
			return principalDirection2;
		}
		public void setPd1(List<Vector4> p) {
			 principalDirection1=p;
			pd1Size = principalDirection1.Count;
		}
		public void setPd2(List<Vector4> p) {
			principalDirection2=p;
			pd2Size = principalDirection2.Count;
		}
		//For debug
		void Start ()
		{
			if (exportToFile) {
				if (pathToFile.Length != 0) {
					SaveMeshToFile (pathToFile+"/"+transform.name);
				}
			}
			if (showData) {
				ShowData ();
			}
		}

		public void ShowData ()
		{
			Matrix4x4 tm = transform.localToWorldMatrix;
			for (int i = 0; i < centers.Count; i++) {
				
				Debug.Log ("center[" + i + "]=" + centers [i].ToString("F6"));
				if (Mathf.Infinity != principalDirection1 [i].w) {
					Debug.DrawRay (tm*centers [i], tm*principalDirection1 [i] * 5f, Color.blue);
				}
				if (Mathf.Infinity != principalDirection2 [i].w) {
					Debug.DrawRay (tm*centers [i], tm*principalDirection2 [i], Color.red);
					Vector3 u = principalDirection2 [i];
					Debug.Log("pd="+(tm*u).normalized.ToString("F6") );
				}
			}
		}

		public void SaveMeshToFile (string name)
		{
			
			FileStream m_FileStream4;
			FileStream m_FileStream5;
			StreamWriter m_pd1;
			StreamWriter m_pd2;

		
			m_FileStream4 = new FileStream (name + "-pd1.txt", FileMode.Create, FileAccess.ReadWrite);
			m_pd1 = new StreamWriter (m_FileStream4, System.Text.Encoding.ASCII);
			m_FileStream5 = new FileStream (name + "-pd2.txt", FileMode.Create, FileAccess.ReadWrite);
			m_pd2 = new StreamWriter (m_FileStream5, System.Text.Encoding.ASCII);
			for (int i = 0; i < principalDirection1.Count; i++) {
				m_pd1.WriteLine (principalDirection1 [i].x.ToString ("E20") + "\t" + principalDirection1 [i].y.ToString ("E20") + "\t" + principalDirection1 [i].z.ToString ("E20")+"\t"+principalDirection1[i].w.ToString ("E20"));
				m_pd2.WriteLine (principalDirection2 [i].x.ToString ("E20") + "\t" + principalDirection2 [i].y.ToString ("E20") + "\t" + principalDirection2 [i].z.ToString ("E20")+"\t"+principalDirection2[i].w.ToString ("E20"));
			}
			m_pd1.Flush ();
			m_pd1.Close ();
			m_pd2.Flush ();
			m_pd2.Close ();


		}
	}
}
