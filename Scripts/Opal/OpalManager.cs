/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
namespace Opal
{
	public enum OpalSimulationTypes {
		Basic, //Only flat surfaces and horizontal or vertical polarization, equal for both transmitters and receivers
		Depolarization, //Flat surfaces with arbitrary polarizations for transmitter and receiver
		RDN, //Flat and curved surfaces with arbitrary polarizations
		SingleDiffraction //Only single diffraction if selected. Diffraction can be used with the other simulation by checking useDiffraction		
	}
	public enum OpalComputeModes {
		Voltage, //Induced voltage on the antenna and corresponding received power
		Field //X,Y,Z components of the electric field at the receiver point
	}
	[System.Serializable]
	public class OpalRaySphere {
		public float initAzimuth=0f;
		public float azimuthDelta = 0.1f;
		public float endAzimuth = 360f;
		public float initElevation = 0f;
		public float elevationDelta = 0.1f;
		public float endElevation = 180f;
		//This number of rays is used with RDN, the total number of rays generated per launch is rayD*rayD
		public int rayD=10000;
	}
	[System.Serializable]
	public class OpalAntennaGainFile {
		public string path;
		public int id;
	}
	public class OpalManager :Singleton<OpalManager>
	{
		
		public bool useOpal=true;

		public static bool isInitialized = false;


		public OpalSimulationTypes simType=OpalSimulationTypes.Depolarization;
		public OpalComputeModes computeMode=OpalComputeModes.Voltage;
		public float frequency = 5.9e9f;
		public uint maxReflections = 10;
		public bool useExactSpeedOfLight = true;
		public bool useDiffraction = true;
		public bool enableFastMath = false;
		public bool generateRaysOnLaunch=true;
		public bool multiGPU = true;
		public bool logTrace = false;
		public bool useAntennaGain = false;
		//To transmit with multiple transmitters in parallel
		public bool multiTransmitter = false;
		public float minEpsilon = 1e-3f;		
		public OpalRaySphere raySphere;
		public List<OpalAntennaGainFile> antennaGainFiles; 
		public delegate void OpalInitialized();
		protected OpalInitialized initializedListeners=null;

		protected List<Antenna> cachedReceivers = null;
		protected List<Antenna> cachedTransmitters = null;
		protected List<DynamicMesh> cachedDynamicMeshes = null;
		protected List<OpalLogTraceManager> logTracers = null;
	        protected HashSet<uint> globalFaces= null; //To make sure that the faceIds are unique
		protected List<StaticMesh> delayedMeshes = null;
		
		public uint GetLastGlobalFace() {
			return globalFaces.Max();
		}	
		public void UpdateGlobalFaces(HashSet<uint> faces) {
			if (faces.Overlaps(globalFaces)) {
				throw new System.InvalidOperationException ("UpdateGlobalFaces(): Error: face Ids in this mesh already sent in another mesh. This will result in Opal errors" );
			} else {
				globalFaces.UnionWith(faces);
			}
		}
		// Use this for initialization
		void Start ()
		{
			if (logTrace) {
				OpalLogTraceManager[] tracers = FindObjectsOfType<OpalLogTraceManager> (); 
				logTracers = new List<OpalLogTraceManager>(tracers);	
			}
			InitOpal ();
			if (initializedListeners != null) {
				initializedListeners ();
			}

		}
		public void RegisterOpalInitializedListener(OpalInitialized l) {
			initializedListeners += l;
		}
		
		public virtual void InitOpal ()
		{
			Debug.Log ("Initializing opal: simType=" +simType +"computeMode="+computeMode+  ";useExactSpeedOfLight="+useExactSpeedOfLight);
	        	globalFaces=new HashSet<uint>();	
			int stype=0;
			switch (simType) {
				case OpalSimulationTypes.Basic:
					stype=0;
					break;
				case OpalSimulationTypes.Depolarization:
					stype=1;
					break;	
				case OpalSimulationTypes.RDN:
					stype=2;
					break;
				case OpalSimulationTypes.SingleDiffraction:
 					useDiffraction=true;
					stype=3;
					break;
			}
			int mode=0;
			switch(computeMode) {
				case OpalComputeModes.Voltage:
					mode=0;
					break;
				case OpalComputeModes.Field:
					mode=1;
					break;
			}
	
			SendInit (frequency,stype ,mode,  useExactSpeedOfLight, useDiffraction, enableFastMath,generateRaysOnLaunch,multiGPU,logTrace, multiTransmitter, useAntennaGain, maxReflections, minEpsilon); 

			isInitialized = true;
			CollectAndSendStaticMeshes ();
			if (useDiffraction) {
				CollectAndSendEdges ();
			}
			if (generateRaysOnLaunch) {	
				SendFinishContext ();
			} else {
				CreateRaySphere ();
			}

			if (useAntennaGain) {
				RegisterAntennaGains();
			}
			//Create ray sphere has to be called after FinishSceneContext
			if (generateRaysOnLaunch) {
				CreateRaySphere ();
			} else {
				SendFinishContext ();
			}
			
			//WARNING: enabling printing with substep ray sphere and more than a few transmits will probably hang your applicatio due to the large number of lines logged
		//	Debug.Log ("Enabled printing");
			OpalInterface.SetPrintEnabled (1024 * 1024 * 1024);

			if (cachedReceivers != null) {
				for (int i = 0; i < cachedReceivers.Count; i++) {
					RegisterReceiver (cachedReceivers [i]);
				}
			}
			if (cachedTransmitters != null) {
				for (int i = 0; i < cachedTransmitters.Count; i++) {
					RegisterTransmitter (cachedTransmitters [i]);
				}
			}


			if (cachedDynamicMeshes != null) {
				for (int i = 0; i < cachedDynamicMeshes.Count; i++) {
					RegisterDynamicMesh (cachedDynamicMeshes [i]);
				}
			}


			Debug.Log ("Opal initialized");
		}
		public virtual int SendFinishContext() {
			int r = OpalInterface.FinishSceneContext ();
			if (r != 0) {
				throw new System.InvalidOperationException ("Error in FinishSceneContext:" + r);
			}
			return r;
		}
		public virtual void SendInit( float frequency, int simType,  int computeMode,  bool useExactSpeedOfLight,  bool useDiffraction,  bool enableFastMath,  bool generateRaysOnLaunch,  bool enableMultiGPU,  bool logTrace,  bool enableMultitransmitter,  bool useAntennaGain, uint m, float minE) {
			int r = OpalInterface.Init (frequency,simType ,computeMode,  useExactSpeedOfLight, useDiffraction, enableFastMath,generateRaysOnLaunch,multiGPU,logTrace, multiTransmitter, useAntennaGain, m,minE ); 

			if (r != 0) {
				throw new System.InvalidOperationException ("Could not initialize Opal:" + r);
			}
			//Debug.Log ("Max reflections=" + maxReflections);
			//r=OpalInterface.SetMaxReflections (maxReflections);
			//if (r != 0) {
			//	throw new System.InvalidOperationException ("Could not set max reflections:" + r);
			//}
		}
		
		public override void OnDestroy ()
		{
			base.OnDestroy ();
			Debug.Log ("Exiting opal");

			OpalInterface.Exit ();
			isInitialized = false;
		}

		public virtual void RegisterAntennaGains() {
			foreach (OpalAntennaGainFile f in antennaGainFiles) {
				Debug.Log("Registering gain from file "+f.path);
				f.id=SendLoadAndRegisterGain(f.path);
				if (f.id<0) {
					throw new System.InvalidOperationException ("Error in RegisterAntennaGains: path="+f.path+", gainId= "+f.id);
				}
			} 
		}
		public virtual int SendLoadAndRegisterGain (string path) {
				return OpalInterface.LoadAndRegisterGain(path);
		}
		
		public virtual void RegisterReceiverGain(Antenna a, string path) {
			foreach (OpalAntennaGainFile f in antennaGainFiles) {
				//Debug.Log("a gain="+path+" f gain="+f.path);
				if (f.path.Equals(path)) {
					int r=SendRegisterReceiverGain(a.id,f.id);
					if (r != 0) {
						throw new System.InvalidOperationException ("Error in RegisterReceiverGain: rxId="+a.id+", gainId= "+f.id+" Error:" + r);
					}
					
					return;
				}
			}
			throw new System.InvalidOperationException ("Error in RegisterReceiverGain: antennaGain not found  " + path);
		}
		public virtual  int SendRegisterReceiverGain ( int rxId,  int gainId) {
			return OpalInterface.RegisterReceiverGain(rxId,gainId);
		}
		public virtual void RegisterTransmitterGain(Antenna a, string path) {
			foreach (OpalAntennaGainFile f in antennaGainFiles) {
				//Debug.Log("transmitter a gain="+path+" f gain="+f.path);
				if (f.path.Equals(path)) {
					int r=SendRegisterTransmitterGain(a.id,f.id);
					if (r != 0) {
						throw new System.InvalidOperationException ("Error in RegisterTransmitterGain: rxId="+a.id+", gainId= "+f.id+" Error:" + r);
					}
					
					return;
				}
			}
			throw new System.InvalidOperationException ("Error in RegisterTransmitterGain: antennaGain not found  " + path);
		}
		public virtual int SendRegisterTransmitterGain ( int txId,  int gainId) {
				return OpalInterface.RegisterTransmitterGain(txId,gainId);
		}
		public virtual void Transmit (int id, float txPower, Vector3 txPosition, Vector3ToMarshal polarization)
		{
			if (isInitialized) {
				Debug.Log (Time.time+"\t. Transmit:"+txPosition );
				int r = OpalInterface.Transmit (id, txPower, OpalInterface.ToMarshal(txPosition), polarization);
				if (r != 0) {
					throw new System.InvalidOperationException ("Error in Transmit: id=" + id + ". Error:" + r);
				}
			}
		}

		/*public virtual void Transmit (int id, float txPower, Vector3 pos, Vector3 pol)
		{
			if (isInitialized) {

				int r = OpalInterface.Transmit (id, txPower, pos.x,pos.y,pos.z, pol.x,pol.y,pol.z);
				if (r != 0) {
					throw new System.InvalidOperationException ("Error in Transmit: id=" + id + ". Error:" + r);
				}
			}
		}
		*/
		public void CreateCustomRaySphere ()
		{


			Vector3ToMarshal[] rays = new Vector3ToMarshal[2];
			rays [0] = OpalInterface.ToMarshal (Vector3.forward);
			rays [1] = OpalInterface.ToMarshal ((Quaternion.Euler (45f, 0.0f, 0.0f) * Vector3.forward).normalized);

			int r = OpalInterface.FillRaySphere2D (2, 1, rays);
			if (r != 0) {
				throw new System.InvalidOperationException ("Error in CreateCustomRaySphere: " + r);
			}
		}
		public void CallReceiverCallbacks(int txId, int trNumber) {
			foreach (Antenna r in  cachedReceivers) {
				if (computeMode==OpalComputeModes.Voltage) {
					float p=OpalInterface.GetReceivedPower(r.id,txId);
					r.ReceivedPower(p,txId);
				} else {
					float Exr=0.0f;
					float Exi=0.0f;
					float Eyr=0.0f;
					float Eyi=0.0f;
					float Ezr=0.0f;
					float Ezi=0.0f;
					int re=OpalInterface.GetReceivedE(r.id,txId, ref  Exr, ref  Exi, ref Eyr, ref Eyi, ref Ezr, ref Ezi);
					r.ReceivedE(txId, Exr, Exi, Eyr,  Eyi,   Ezr,  Ezi  );
					if (re != 0) {
						throw new System.InvalidOperationException ("Error in CallReceiverCallbacks with Field compute mode: " + re);
					}
				}
			}
			if (logTrace) {
				foreach (OpalLogTraceManager m in logTracers) {
					m.ShowAll(trNumber);
				}
			}
		}	
		public void RegisterReceiver (Antenna rec)
		{
			if (isInitialized) {
				SendReceiverToOpal (rec);
				if (useAntennaGain && rec.useAntennaGain) {
					RegisterReceiverGain(rec,rec.antennaGainPath);
				}		
			} else {
				if (cachedReceivers == null) {
					cachedReceivers = new List<Antenna> ();
				}
				if (cachedReceivers.Contains (rec)) {
					Debug.Log ("Receiver is already cached " + rec.id);
					return;
				}
				Debug.Log ("Caching receiver " + rec.id);
				cachedReceivers.Add (rec);
			}

		}

		public void RegisterDynamicMesh (DynamicMesh dm)
		{
			if (useOpal) {
				if (isInitialized) {
					dm.CreateGroup ();
				} else {
					if (cachedDynamicMeshes == null) {
						cachedDynamicMeshes = new List<DynamicMesh> ();
					}
					if (cachedDynamicMeshes.Contains (dm)) {
						Debug.Log ("DynamicMesh is already cached " + dm.id);
						return;
					}
					Debug.Log ("Caching DynamicMesh " + dm.id);
					cachedDynamicMeshes.Add (dm);
				
				}
			} else {
				dm.enabled=false;
				//Debug.Log ("Disabling Dynamic Mesh "+dm.id);
			}

		}

		public virtual void SendReceiverToOpal (Antenna rec)
		{
			Debug.Log ("SendReceiverToOpal receiver " + rec.id + ". radius=" + rec.radius + ". polarization="+rec.polarization);
			int r = OpalInterface.AddReceiverFromUnity (rec.id, OpalInterface.ToMarshal (rec.transform), rec.GetPolarizationMarshaled(),rec.radius);
			if (r != 0) {
				throw new System.InvalidOperationException ("Error in AddReceiverFromUnity: id=" + rec.id + ". Error:" + r);
			}
		}
		public virtual void CollectAntennaGains() {
			Antenna[] antennas = FindObjectsOfType<Antenna> (); 
			foreach (Antenna rec in antennas) {
				if (rec.useAntennaGain) {
					if (rec.receiver) {
						RegisterReceiverGain(rec,rec.antennaGainPath);
					}
					if (rec.transmitter) {
						RegisterTransmitterGain(rec,rec.antennaGainPath);
					}
				}
			}
		}
		public virtual void UpdateReceiver (Antenna rec)
		{
			if (OpalManager.isInitialized) {
				Vector3ToMarshal pol=rec.GetPolarizationMarshaled();
				Debug.Log ("Updating receiver " + rec.id +"; polarization="+rec.polarization+"polM=("+pol.x+","+pol.y+","+pol.z);
				
				OpalInterface.UpdateReceiverWithRadius (rec.id, OpalInterface.ToMarshal (rec.transform.position), pol, rec.radius);
			}
		}

		public virtual void UnregisterReceiver (Antenna rec)
		{
			if (isInitialized) {
				int r = OpalInterface.RemoveReceiverFromUnity (rec.id);
				if (r != 0) {
					throw new System.InvalidOperationException ("Error in RemoveReceiverFromUnity: id=" + rec.id + ". Error:" + r);
				}
			}

		}

		public void RegisterTransmitter (Antenna tx)
		{
			if (isInitialized) {
				//TODO: reimplement when adapted
				if (multiTransmitter) { //Register for simultaneous transmission
					SendTransmitterToOpal (tx);
				}
				if (useAntennaGain) {
					if (tx.useAntennaGain) {
						RegisterTransmitterGain(tx,tx.antennaGainPath);

					}
				}
			} else {
				if (cachedTransmitters == null) {
					cachedTransmitters = new List<Antenna> ();
				}
				if (cachedTransmitters.Contains (tx)) {
					Debug.Log ("Transmitter is already cached " + tx.id);
					return;
				}
				Debug.Log ("Caching Transmitter " + tx.id);
				cachedTransmitters.Add (tx);
			}

		}


		public void SendTransmitterToOpal (Antenna tx)
		{
			Debug.Log ("Adding transmitter " + tx.id);
			int r = SendRegisterTransmitter (tx.id, OpalInterface.ToMarshal (tx.transform), tx.GetPolarizationMarshaled (), tx.txPower);
			if (r != 0) {
				throw new System.InvalidOperationException ("Error in RegisterTransmitter: id=" + tx.id + ". Error:" + r);
			}
		}
		public virtual  int  SendRegisterTransmitter( int txId,  Vector3ToMarshal origin, Vector3ToMarshal  polarization,  float transmitPower) {
			return OpalInterface.RegisterTransmitter (txId, origin, polarization, transmitPower);
		} 

		public virtual void UnregisterTransmitter (Antenna tx)
		{
			if (isInitialized) {
				if (multiTransmitter) {
					int r = OpalInterface.RemoveTransmitter (tx.id);
					if (r != 0) {
						throw new System.InvalidOperationException ("Error in RemoveTransmitter: id=" + tx.id + ". Error:" + r);
					}
				}
			}

		}

		public void AddTransmitterToGroup (Antenna tx)
		{
			if (multiTransmitter) {
				OpalInterface.AddTransmitterToGroup (tx.id, tx.txPower, OpalInterface.ToMarshal (tx.transform), tx.GetPolarizationMarshaled ()); 
			}
		}

		public void ClearGroup ()
		{
			if (multiTransmitter) {
				OpalInterface.ClearGroup ();
			}
		}

		public void GroupTransmit ()
		{
			if (multiTransmitter) {
				OpalInterface.GroupTransmit ();
			}
		}

		public void CollectAndSendReceivers ()
		{
			Receiver[] receivers = FindObjectsOfType<Receiver> (); 
			for (int i = 0; i < receivers.Length; i++) {
				Debug.Log ("Adding receiver " + receivers [i].id + ". radius=" + receivers [i].radius);
				int r = OpalInterface.AddReceiverFromUnity (receivers [i].id, OpalInterface.ToMarshal (receivers [i].transform), receivers[i].GetPolarizationMarshaled(),receivers [i].radius);
				if (r != 0) {
					throw new System.InvalidOperationException ("Error in AddReceiverFromUnity: id=" + receivers [i].id + ". Error:" + r);
				}
			}
		}

		public virtual void CollectAndSendEdges ()
		{
			//Get all edges in the opal layer
			OpalEdge[] edges = FindObjectsOfType<OpalEdge> (); 
			int m = 0;
			for (int i = 0; i < edges.Length; i++) {
				if (edges[i].opalMeshProperties.sendToOpal) {
					SendEdgeToOpal (edges[i].transform, edges [i]);
					m++;
				}

			}
			Debug.Log (m + " edges sent");
		}
		public virtual void CollectAndSendStaticMeshes ()
		{
			//Prepare list for delayed
			delayedMeshes= new List<StaticMesh>();
			
			//Get all static meshes in the opal layer
			StaticMesh[] staticMeshes = FindObjectsOfType<StaticMesh> (); 
			int m = 0;
			for (int i = 0; i < staticMeshes.Length; i++) {

				int sent=SendStaticMeshToOpal (staticMeshes [i].transform, staticMeshes [i], false);
				m += sent;
			}
			for (int i = 0; i < delayedMeshes.Count; i++) {
				int sent=SendStaticMeshToOpal (delayedMeshes [i].transform, delayedMeshes [i], true);
				m +=sent;
			}
			delayedMeshes.Clear();
			delayedMeshes = null;
			Debug.Log (m + " static meshes sent");
		}

		public void CreateRaySphere ()
		{
			int r;
			if (simType==OpalSimulationTypes.RDN) {
				r = SendSetRayRange (raySphere.initElevation,  raySphere.endElevation, raySphere.initAzimuth,  raySphere.endAzimuth, raySphere.rayD);
			} else {
				r = SendCreateRaySphereRange (raySphere.initElevation, raySphere.elevationDelta, raySphere.endElevation, raySphere.initAzimuth, raySphere.azimuthDelta, raySphere.endAzimuth);
			}
			if (r != 0) {
				throw new System.InvalidOperationException ("Error in CreateRaySphere2D:" + r);
			}
		}
		public virtual int SendSetRayRange ( float initElevation,  float endElevation,  float initAzimuth,   float endAzimuth,  int rayD) {
				return OpalInterface.SetRayRange (initElevation,  endElevation, initAzimuth,  endAzimuth, rayD);
		}
		public virtual int SendCreateRaySphereRange ( float initElevation,  float elevationDelta,  float endElevation,  float initAzimuth,  float azimuthDelta,  float endAzimuth) {
				return OpalInterface.CreateRaySphereRange (initElevation, elevationDelta, endElevation, initAzimuth, azimuthDelta, endAzimuth);
		}

		public void SaveMeshToFile (string name, Vector3ToMarshal[] vertices, int[] indices, Matrix4x4ToMarshal tm, MaterialEMProperties em)
		{
			FileStream m_FileStream = new FileStream (name + "-v.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_mesh = new StreamWriter (m_FileStream, System.Text.Encoding.ASCII);
			FileStream m_FileStream2 = new FileStream (name + "-i.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_tri = new StreamWriter (m_FileStream2, System.Text.Encoding.ASCII);
			FileStream m_FileStream3 = new FileStream (name + "-t.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_tm = new StreamWriter (m_FileStream3, System.Text.Encoding.ASCII);
			FileStream m_FileStream4 = new FileStream (name + "-em.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_em = new StreamWriter (m_FileStream4, System.Text.Encoding.ASCII);
			for (int i = 0; i < indices.Length; i++) {
				//Debug.Log ("index=" + indices [i]);
				m_tri.WriteLine (indices [i]);

			}
			for (int i = 0; i < vertices.Length; i++) {
				m_mesh.WriteLine (vertices [i].x.ToString ("E8") + "\t" + vertices [i].y.ToString ("E8") + "\t" + vertices [i].z.ToString ("E8"));
			}
			//translation matrix
			m_tm.WriteLine (tm.m00.ToString ("E8") + "\t" + tm.m01.ToString ("E8") + "\t" + tm.m02.ToString ("E8") + "\t" + tm.m03.ToString ("E8"));
			m_tm.WriteLine (tm.m10.ToString ("E8") + "\t" + tm.m11.ToString ("E8") + "\t" + tm.m12.ToString ("E8") + "\t" + tm.m13.ToString ("E8"));
			m_tm.WriteLine (tm.m20.ToString ("E8") + "\t" + tm.m21.ToString ("E8") + "\t" + tm.m22.ToString ("E8") + "\t" + tm.m23.ToString ("E8"));
			m_tm.WriteLine (tm.m30.ToString ("E8") + "\t" + tm.m31.ToString ("E8") + "\t" + tm.m32.ToString ("E8") + "\t" + tm.m33.ToString ("E8"));

			m_em.WriteLine (em.a);
			m_em.WriteLine (em.b);
			m_em.WriteLine (em.c);
			m_em.WriteLine (em.d);

			m_mesh.Flush ();
			m_mesh.Close ();
			m_tri.Flush ();
			m_tri.Close ();
			m_tm.Flush ();
			m_tm.Close ();
			m_em.Flush ();
			m_em.Close ();
		}
		public virtual void SendMeshToGroup (int id, Transform rootT, Transform meshT, Mesh mesh, List<int> faceIds, MaterialEMProperties em)
		{
			Vector3[] v = mesh.vertices;
			Vector3ToMarshal[] vertices = new Vector3ToMarshal[v.Length];
			int[] indices = mesh.triangles;
			//Debug.Log ("Sending mesh " + transform.name + " to group " + id + ". vertices=" + v.Length);
			for (int j = 0; j < v.Length; j++) {
				//Get the vertex position relative to the root
				//Debug.Log("v="+v [j].x.ToString ("E2") + "\t" + v [j].y.ToString ("E2") + " \t" + v [j].z.ToString ("E2"));
				Vector3 aux = rootT.InverseTransformPoint (meshT.TransformPoint (v [j]));
				//Debug.Log ("aux="+aux.x.ToString ("E2") + "\t" + aux.y.ToString ("E2") + " \t" + aux.z.ToString ("E2"));
				if (Mathf.Abs (aux.x) < 1E-15f) {
					aux.x = 0.0f;
				}
				if (Mathf.Abs (aux.y) < 1E-15f) {
					aux.y = 0.0f;
				}
				if (Mathf.Abs (aux.z) < 1E-15f) {
					aux.z = 0.0f;
				}
				vertices [j] = OpalInterface.ToMarshal (aux);
				//Debug.Log (vertices [i]);
			}

			int r=SendMeshWithFacesToGroup (id, vertices, indices, faceIds.Count, faceIds.ToArray(),em);
			if (r != 0) {
				throw new System.InvalidOperationException ("Could not send mesh "+ rootT.name+"  to group: " + id +". Error:" + r);
			}
		}

		public virtual void SendMeshToGroup (int id, Transform rootT, Transform meshT, Mesh mesh, MaterialEMProperties em)
		{
			Vector3[] v = mesh.vertices;
			Vector3ToMarshal[] vertices = new Vector3ToMarshal[v.Length];
			int[] indices = mesh.triangles;
			//Debug.Log ("Sending mesh " + transform.name + " to group " + id + ". vertices=" + v.Length);
			for (int j = 0; j < v.Length; j++) {
				//Get the vertex position relative to the root
				//Debug.Log("v="+v [j].x.ToString ("E2") + "\t" + v [j].y.ToString ("E2") + " \t" + v [j].z.ToString ("E2"));
				Vector3 aux = rootT.InverseTransformPoint (meshT.TransformPoint (v [j]));
				//Debug.Log ("aux="+aux.x.ToString ("E2") + "\t" + aux.y.ToString ("E2") + " \t" + aux.z.ToString ("E2"));
				if (Mathf.Abs (aux.x) < 1E-15f) {
					aux.x = 0.0f;
				}
				if (Mathf.Abs (aux.y) < 1E-15f) {
					aux.y = 0.0f;
				}
				if (Mathf.Abs (aux.z) < 1E-15f) {
					aux.z = 0.0f;
				}
				vertices [j] = OpalInterface.ToMarshal (aux);
				//Debug.Log (vertices [i]);
			}

			SendMeshToGroup (id, vertices, indices, em);

		}

		public virtual void AddDynamicMeshGroup (int id)
		{
			int dmg = OpalInterface.AddDynamicMeshGroup (id);
			if (dmg != 0) {
				throw new System.InvalidOperationException ("Error in DynamicMesh::CreateGroup():" + dmg);
			}

		}

		public virtual void FinishDynamicMeshGroup (int id)
		{
			OpalInterface.FinishDynamicMeshGroup (id);
		}

		public virtual void UpdateTransformInGroup (int id, Transform t)
		{
			Matrix4x4 tm = t.localToWorldMatrix;
			Matrix4x4ToMarshal matrix = new Matrix4x4ToMarshal ();

			//Debug.Log ("DynamicMesh Matrix of " + t.name + " is " + tm);
			OpalInterface.MarshalMatrix4x4 (ref tm, ref matrix);
			OpalInterface.UpdateTransformInGroup (id, matrix);
			
		}

		public virtual void RemoveDynamicMeshGroup (int id)
		{
			int r=OpalInterface.RemoveDynamicMeshGroup (id);
			if (r != 0) {
				throw new System.InvalidOperationException ("Could Remove dinamic mesh group:"+id+" Error:" + r);
			}
		}
		//We get the vertices already transformed
		public virtual int SendMeshWithFacesToGroup (int id, Vector3ToMarshal[] vertices, int[] indices, int faceIdCount, int[] faceIds, MaterialEMProperties em) {
			return OpalInterface.AddMeshWithFacesToGroupFromUnity(id, vertices.Length, vertices, indices.Length, indices, faceIdCount, faceIds, em);

		}
		//We get the vertices already transformed
		public void SendMeshToGroup (int id, Vector3ToMarshal[] vertices, int[] indices, MaterialEMProperties em)
		{

			//Debug.Log ("Sending " + vertices.Length + " vertices. indices=" + indices.Length);
			int r = OpalInterface.AddMeshToGroupFromUnity (id, vertices.Length, vertices, indices.Length, indices, em);
			if (r != 0) {
				throw new System.InvalidOperationException ("Could not add dynamic mesh to opal: " + id + ". Error:" + r);
			}


		}

		protected virtual void SendEdgeToOpal (Transform t, OpalEdge sm)
		{
			Vector3ToMarshal v=OpalInterface.ToMarshal(sm.info.v);
			Vector3ToMarshal p=OpalInterface.ToMarshal(sm.info.o);
			Vector3ToMarshal face_a=OpalInterface.ToMarshal(sm.info.a);
			Vector3ToMarshal face_b=OpalInterface.ToMarshal(sm.info.b);
			Vector3ToMarshal normal_a=OpalInterface.ToMarshal(sm.info.n_a);
			Vector3ToMarshal normal_b=OpalInterface.ToMarshal(sm.info.n_b);
			int r=SendEdge(p, v, sm.info.face_a, sm.info.face_b, face_a, face_b,normal_a, normal_b, sm.opalMeshProperties.emProperties, (int) sm.info.id );
			if (r != 0) {
				
				throw new System.InvalidOperationException ("Could not add edge to opal: " + sm.info.id + "on Building:" +t.parent.name +". Error:" + r);
			}

		}
		public virtual int SendEdge ( Vector3ToMarshal p,  Vector3ToMarshal v,  uint faid,  uint fbid, Vector3ToMarshal face_a,  Vector3ToMarshal face_b, Vector3ToMarshal normal_a,  Vector3ToMarshal normal_b,  MaterialEMProperties emProp,  int id)  {
			int r=OpalInterface.AddEdge(p, v, faid, fbid, face_a, face_b,normal_a, normal_b, emProp, id );
			return r;
		}
		public virtual int SendEdgeToGroup ( Vector3ToMarshal p,  Vector3ToMarshal v,  uint faid,  uint fbid, Vector3ToMarshal face_a,  Vector3ToMarshal face_b, Vector3ToMarshal normal_a,  Vector3ToMarshal normal_b,  MaterialEMProperties emProp,  int id, int groupId)  {
			int r=OpalInterface.AddEdgeToGroup(p, v, faid, fbid, face_a, face_b,normal_a, normal_b, emProp, id, groupId );
			return r;
		}
		protected virtual int SendStaticMeshToOpal (Transform t, StaticMesh sm, bool sendDelayed)
		{
			//Debug.Log("Sending static mesh "+t.name);
			OpalMeshProperties mp = sm.GetOpalMeshProperties();
			if (!mp.sendToOpal) {
				Debug.Log("Not sending static mesh "+t.name);
				return 0;
			}
			if (mp.sendWithFaces==false && sendDelayed==false) {
				delayedMeshes.Add(sm);
				Debug.Log("Delayed Sending static mesh "+t.name);
				return 0;
			} 
			Vector3[] v = sm.meshFilter.mesh.vertices;
			Vector3ToMarshal[] vertices = new Vector3ToMarshal[v.Length];
			int[] indices = sm.meshFilter.mesh.triangles;
			//if (t.gameObject.name.Equals ("Cube")) {
			//	FileStream m_FileStream = new FileStream ("vert.txt", FileMode.Create, FileAccess.ReadWrite);
			//	StreamWriter m_mesh = new StreamWriter (m_FileStream, System.Text.Encoding.ASCII);
			//	FileStream m_FileStream2 = new FileStream ("tri.txt", FileMode.Create, FileAccess.ReadWrite);
			//	StreamWriter m_tri = new StreamWriter (m_FileStream2, System.Text.Encoding.ASCII);
			//	for (int i = 0; i < indices.Length; i++) {
			//		Debug.Log ("index=" + indices [i]);
			//		m_tri.WriteLine (indices [i]);
			//	}
			Matrix4x4 tm = t.transform.localToWorldMatrix;
			for (int i = 0; i < v.Length; i++) {
				if (Mathf.Abs (v [i].x) < 1E-15f) {
					v [i].x = 0.0f;
				}
				if (Mathf.Abs (v [i].y) < 1E-15f) {
					v [i].y = 0.0f;
				}
				if (Mathf.Abs (v [i].z) < 1E-15f) {
					v [i].z = 0.0f;
				}
				vertices [i] = OpalInterface.ToMarshal (v [i]);
				//vertices [i].x = v [i].x;
				//vertices [i].y = v [i].y;
				//vertices [i].z = v [i].z;
				//Debug.Log (v [i].x.ToString ("E2"));
				//Debug.Log (v [i].y.ToString ("E2"));
				//Debug.Log (v [i].z.ToString ("E2"));
				//Debug.Log (vertices [i].z.ToString ("E2"));
				//		m_mesh.WriteLine (v [i].x.ToString ("E2") + "\t" + v [i].y.ToString ("E2") + "\t" + v [i].z.ToString ("E2"));
				//Debug.Log(v [i].x.ToString ("E2") + "," + v [i].y.ToString ("E2") + "," + v [i].z.ToString ("E2"));
				//Vector4 nv = new Vector4 (v [i].x, v [i].y, v [i].z, 1.0f);
				//Debug.Log (tm *nv);
				//Debug.Log (tm.MultiplyPoint3x4 (v [i]));

			}
			//	m_mesh.Flush ();
			//	m_mesh.Close ();
			//	m_tri.Flush ();
			//	m_tri.Close ();

			//}

			Matrix4x4ToMarshal matrix = new Matrix4x4ToMarshal ();

			//Debug.Log("Matrix of"+t.transform.name+"is "+tm);
			OpalInterface.MarshalMatrix4x4 (ref tm, ref matrix);


			//SaveMeshToFile (t.transform.name, vertices, indices, matrix, sm.GetOpalMeshProperties ().emProperties);
			//Debug.Log("Sending "+t.name);

			if (mp.sendWithFaces) {
				if (useDiffraction) {
					//This is overkill but we have to be sure that the triangles coincide
					List<int> fids = new List<int>();
					List<OpalTriangleFace> faceIds= mp.faceIds;
					//foreach (OpalTriangleFace otf in faceIds) {
					//	Debug.Log("tris="+otf.tris[0]+","+otf.tris[1]+","+otf.tris[2]+"="+otf.id);
					//}
					HashSet<uint> faces=new HashSet<uint>();	
					for (int i=0; i<indices.Length; i++) {	
						foreach (OpalTriangleFace otf in faceIds) {
							if (indices[i] == otf.tris[0] ||indices[i] == otf.tris[1] || indices[i] == otf.tris[2]) {
								fids.Add((int)otf.id);
								//Add to list of faces...
								faces.Add(otf.id);
								break;
							} 

						}
					}

					//Now check that the faces are unique so far
					if (faces.Overlaps(globalFaces)) {
						throw new System.InvalidOperationException ("SendStaticMeshToOpal: " + t.name + ". Error: face Ids in this mesh already sent in another mesh. This will result in Opal errors" );
					} else {
						globalFaces.UnionWith(faces);
					}
					//Debug.Log("Sending static mesh with faces "+t.name);
					int r=SendStaticMeshWithFaces(vertices.Length, vertices, indices.Length, indices, fids.Count, fids.ToArray(),  matrix, sm.GetOpalMeshProperties ().emProperties);
					if (r != 0) {
						throw new System.InvalidOperationException ("Could not add static mesh to opal: " + t.name + ". Error:" + r);
					}
				} else {
					int r=SendMesh (vertices.Length, vertices, indices.Length, indices, matrix, sm.GetOpalMeshProperties ().emProperties);
					if (r != 0) {
						throw new System.InvalidOperationException ("Could not add static mesh to opal: " + t.name + ". Error:" + r);
					}
				}

			} else {

				if (mp.curved) {
					MeshCurvatureData cd=t.GetComponent<MeshCurvatureData>();
					if (cd==null) {
						throw new System.InvalidOperationException ("Could not add find mesh curvature data for: " + t.name );
					} else {	
						Vector4ToMarshal[] pd1 = new Vector4ToMarshal[cd.principalDirection1.Count];
						for (int i=0; i< cd.principalDirection1.Count; i++) {
							pd1[i]=OpalInterface.ToMarshal(cd.principalDirection1[i]);
						}
						Vector4ToMarshal[] pd2 = new Vector4ToMarshal[cd.principalDirection2.Count];
						for (int i=0; i< cd.principalDirection2.Count; i++) {
							pd2[i]=OpalInterface.ToMarshal(cd.principalDirection2[i]);
						}
						//Debug.Log("Sending static curved mesh  "+t.name);
						int r=SendStaticCurvedMesh (vertices.Length, vertices, indices.Length, indices, pd1.Length, pd1, pd2.Length, pd2, matrix, sm.GetOpalMeshProperties ().emProperties, true, -1);
						if (r != 0) {
							throw new System.InvalidOperationException ("Could not add curved static mesh to opal: " + t.name + ". Error:" + r);

						}
					}

				} else {


					//Debug.Log("Sending static mesh  "+t.name);
					int r=SendMesh (vertices.Length, vertices, indices.Length, indices, matrix, sm.GetOpalMeshProperties ().emProperties);
					if (r != 0) {
						throw new System.InvalidOperationException ("Could not add static mesh to opal: " + t.name + ". Error:" + r);
					}
				}
			}
			return 1;

		}
		public virtual int SendStaticMeshWithFaces ( int meshVertexCount,  Vector3ToMarshal[] vertices,  int meshTriangleCount, int[] indices, int faceIdCount, int[] fids,  Matrix4x4ToMarshal transformationMatrix,  MaterialEMProperties emProp) {
				int r = OpalInterface.AddStaticMeshWithFacesFromUnity (vertices.Length, vertices, indices.Length, indices, fids.Length, fids, transformationMatrix, emProp);
				return r;
		}
		public virtual int SendMesh ( int meshVertexCount,  Vector3ToMarshal[] vertices,  int meshTriangleCount, int[] indices,  Matrix4x4ToMarshal transformationMatrix,  MaterialEMProperties emProp) {
				int r = OpalInterface.AddStaticMeshFromUnity (vertices.Length, vertices, indices.Length, indices, transformationMatrix, emProp);
				return r;
		}
		public virtual int SendStaticCurvedMesh ( int meshVertexCount,  Vector3ToMarshal[] vertices,  int meshTriangleCount, int[] indices, int pd1Count, Vector4ToMarshal[] pd1, int pd2Count, Vector4ToMarshal[] pd2,  Matrix4x4ToMarshal transformationMatrix,  MaterialEMProperties emProp, bool makeSingleFace, int faceId) {
				int r=OpalInterface.AddStaticCurvedMeshFromUnity (meshVertexCount, vertices, meshTriangleCount, indices, pd1Count, pd1, pd2Count, pd2, transformationMatrix, emProp, makeSingleFace, faceId);
				return r;
		}
		
	}
}
