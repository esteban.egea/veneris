// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Veneris.Communications
{

using global::System;
using global::System.Collections.Generic;
using global::FlatBuffers;

public struct RDNRayRange : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static void ValidateVersion() { FlatBufferConstants.FLATBUFFERS_2_0_0(); }
  public static RDNRayRange GetRootAsRDNRayRange(ByteBuffer _bb) { return GetRootAsRDNRayRange(_bb, new RDNRayRange()); }
  public static RDNRayRange GetRootAsRDNRayRange(ByteBuffer _bb, RDNRayRange obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p = new Table(_i, _bb); }
  public RDNRayRange __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public float InitElevation { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float EndElevation { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float InitAzimuth { get { int o = __p.__offset(8); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float EndAzimuth { get { int o = __p.__offset(10); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public int RayD { get { int o = __p.__offset(12); return o != 0 ? __p.bb.GetInt(o + __p.bb_pos) : (int)0; } }

  public static Offset<Veneris.Communications.RDNRayRange> CreateRDNRayRange(FlatBufferBuilder builder,
      float initElevation = 0.0f,
      float endElevation = 0.0f,
      float initAzimuth = 0.0f,
      float endAzimuth = 0.0f,
      int rayD = 0) {
    builder.StartTable(5);
    RDNRayRange.AddRayD(builder, rayD);
    RDNRayRange.AddEndAzimuth(builder, endAzimuth);
    RDNRayRange.AddInitAzimuth(builder, initAzimuth);
    RDNRayRange.AddEndElevation(builder, endElevation);
    RDNRayRange.AddInitElevation(builder, initElevation);
    return RDNRayRange.EndRDNRayRange(builder);
  }

  public static void StartRDNRayRange(FlatBufferBuilder builder) { builder.StartTable(5); }
  public static void AddInitElevation(FlatBufferBuilder builder, float initElevation) { builder.AddFloat(0, initElevation, 0.0f); }
  public static void AddEndElevation(FlatBufferBuilder builder, float endElevation) { builder.AddFloat(1, endElevation, 0.0f); }
  public static void AddInitAzimuth(FlatBufferBuilder builder, float initAzimuth) { builder.AddFloat(2, initAzimuth, 0.0f); }
  public static void AddEndAzimuth(FlatBufferBuilder builder, float endAzimuth) { builder.AddFloat(3, endAzimuth, 0.0f); }
  public static void AddRayD(FlatBufferBuilder builder, int rayD) { builder.AddInt(4, rayD, 0); }
  public static Offset<Veneris.Communications.RDNRayRange> EndRDNRayRange(FlatBufferBuilder builder) {
    int o = builder.EndTable();
    return new Offset<Veneris.Communications.RDNRayRange>(o);
  }
  public static void FinishRDNRayRangeBuffer(FlatBufferBuilder builder, Offset<Veneris.Communications.RDNRayRange> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedRDNRayRangeBuffer(FlatBufferBuilder builder, Offset<Veneris.Communications.RDNRayRange> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
