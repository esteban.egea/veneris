// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Veneris.Communications
{

using global::System;
using global::System.Collections.Generic;
using global::FlatBuffers;

public struct RaySphereRange : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static void ValidateVersion() { FlatBufferConstants.FLATBUFFERS_2_0_0(); }
  public static RaySphereRange GetRootAsRaySphereRange(ByteBuffer _bb) { return GetRootAsRaySphereRange(_bb, new RaySphereRange()); }
  public static RaySphereRange GetRootAsRaySphereRange(ByteBuffer _bb, RaySphereRange obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p = new Table(_i, _bb); }
  public RaySphereRange __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public float InitElevation { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float ElevationDelta { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float EndElevation { get { int o = __p.__offset(8); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float InitAzimuth { get { int o = __p.__offset(10); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float AzimuthDelta { get { int o = __p.__offset(12); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }
  public float EndAzimuth { get { int o = __p.__offset(14); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }

  public static Offset<Veneris.Communications.RaySphereRange> CreateRaySphereRange(FlatBufferBuilder builder,
      float initElevation = 0.0f,
      float elevationDelta = 0.0f,
      float endElevation = 0.0f,
      float initAzimuth = 0.0f,
      float azimuthDelta = 0.0f,
      float endAzimuth = 0.0f) {
    builder.StartTable(6);
    RaySphereRange.AddEndAzimuth(builder, endAzimuth);
    RaySphereRange.AddAzimuthDelta(builder, azimuthDelta);
    RaySphereRange.AddInitAzimuth(builder, initAzimuth);
    RaySphereRange.AddEndElevation(builder, endElevation);
    RaySphereRange.AddElevationDelta(builder, elevationDelta);
    RaySphereRange.AddInitElevation(builder, initElevation);
    return RaySphereRange.EndRaySphereRange(builder);
  }

  public static void StartRaySphereRange(FlatBufferBuilder builder) { builder.StartTable(6); }
  public static void AddInitElevation(FlatBufferBuilder builder, float initElevation) { builder.AddFloat(0, initElevation, 0.0f); }
  public static void AddElevationDelta(FlatBufferBuilder builder, float elevationDelta) { builder.AddFloat(1, elevationDelta, 0.0f); }
  public static void AddEndElevation(FlatBufferBuilder builder, float endElevation) { builder.AddFloat(2, endElevation, 0.0f); }
  public static void AddInitAzimuth(FlatBufferBuilder builder, float initAzimuth) { builder.AddFloat(3, initAzimuth, 0.0f); }
  public static void AddAzimuthDelta(FlatBufferBuilder builder, float azimuthDelta) { builder.AddFloat(4, azimuthDelta, 0.0f); }
  public static void AddEndAzimuth(FlatBufferBuilder builder, float endAzimuth) { builder.AddFloat(5, endAzimuth, 0.0f); }
  public static Offset<Veneris.Communications.RaySphereRange> EndRaySphereRange(FlatBufferBuilder builder) {
    int o = builder.EndTable();
    return new Offset<Veneris.Communications.RaySphereRange>(o);
  }
  public static void FinishRaySphereRangeBuffer(FlatBufferBuilder builder, Offset<Veneris.Communications.RaySphereRange> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedRaySphereRangeBuffer(FlatBufferBuilder builder, Offset<Veneris.Communications.RaySphereRange> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
