// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Veneris.Communications
{

using global::System;
using global::System.Collections.Generic;
using global::FlatBuffers;

public struct Header : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static void ValidateVersion() { FlatBufferConstants.FLATBUFFERS_2_0_0(); }
  public static Header GetRootAsHeader(ByteBuffer _bb) { return GetRootAsHeader(_bb, new Header()); }
  public static Header GetRootAsHeader(ByteBuffer _bb, Header obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p = new Table(_i, _bb); }
  public Header __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public Veneris.Communications.VenerisMessageTypes Type { get { int o = __p.__offset(4); return o != 0 ? (Veneris.Communications.VenerisMessageTypes)__p.bb.GetUint(o + __p.bb_pos) : Veneris.Communications.VenerisMessageTypes.Reserved; } }
  public uint Size { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetUint(o + __p.bb_pos) : (uint)0; } }
  public float Time { get { int o = __p.__offset(8); return o != 0 ? __p.bb.GetFloat(o + __p.bb_pos) : (float)0.0f; } }

  public static Offset<Veneris.Communications.Header> CreateHeader(FlatBufferBuilder builder,
      Veneris.Communications.VenerisMessageTypes type = Veneris.Communications.VenerisMessageTypes.Reserved,
      uint size = 0,
      float time = 0.0f) {
    builder.StartTable(3);
    Header.AddTime(builder, time);
    Header.AddSize(builder, size);
    Header.AddType(builder, type);
    return Header.EndHeader(builder);
  }

  public static void StartHeader(FlatBufferBuilder builder) { builder.StartTable(3); }
  public static void AddType(FlatBufferBuilder builder, Veneris.Communications.VenerisMessageTypes type) { builder.AddUint(0, (uint)type, 0); }
  public static void AddSize(FlatBufferBuilder builder, uint size) { builder.AddUint(1, size, 0); }
  public static void AddTime(FlatBufferBuilder builder, float time) { builder.AddFloat(2, time, 0.0f); }
  public static Offset<Veneris.Communications.Header> EndHeader(FlatBufferBuilder builder) {
    int o = builder.EndTable();
    return new Offset<Veneris.Communications.Header>(o);
  }
  public static void FinishHeaderBuffer(FlatBufferBuilder builder, Offset<Veneris.Communications.Header> offset) { builder.Finish(offset.Value); }
  public static void FinishSizePrefixedHeaderBuffer(FlatBufferBuilder builder, Offset<Veneris.Communications.Header> offset) { builder.FinishSizePrefixed(offset.Value); }
};


}
