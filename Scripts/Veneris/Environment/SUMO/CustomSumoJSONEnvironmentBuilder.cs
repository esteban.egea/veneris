/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Opal;
namespace Veneris
{
	public class CustomSumoJSONEnvironmentBuilder : SumoJSONEnvironmentBuilder 
	{
		public override void BuildPolygons (SumoBuilder builder)
		{
			numberOfObjects = 0;
			numberOfOpalEdges=0;
			opalStaticMeshes=0;
			this.builder = builder;
			root = new GameObject ("Environment");
			osmIdToJSON = new Dictionary<long, JSONObject> ();
			var watch = System.Diagnostics.Stopwatch.StartNew ();
			if (string.IsNullOrEmpty (pathToOSMJSON)) {
				throw new System.InvalidOperationException ("pathToOSMJSON is empty");

			}
			Debug.Log ("Parsing Custom JSON sumo environment from "+ pathToOSMJSON);
			polygons = new JSONObject (System.IO.File.ReadAllText (pathToOSMJSON));
			if (polygons.IsNull) {
				throw new System.InvalidOperationException ("Error reading JSON file. JSONObject is null");
			} 
			CreateOSMWayDictionary();
			ProcessPolygons ();
			if (buildTrafficLights) {
				BuildTrafficLightPosts ();
			}

		}
		protected virtual void CreateOSMWayDictionary ()
		{
			Debug.Log("Creating custom OSM Way Dictionary");
			List<JSONObject> pols;
			if (polygons ["additional"] ["poly"].IsArray) {
				pols = polygons ["additional"] ["poly"].list;
			} else if (polygons ["additional"] ["poly"].IsObject) {
				pols = new List<JSONObject> ();
				pols.Add (polygons ["additional"] ["poly"]);
			} else {
				Debug.Log ("JSON builder: no polygons found");
				throw new UnityException ("JSON builder: no polygons found");

				return;
			}
			foreach (JSONObject o in pols ) {
				//Debug.Log("o="+o);
				osmIdToJSON.Add (long.Parse(o["_attributes"]["id"].str),o["_attributes"]);
			}
		}
		protected override string GetBuildingName (string i)
		{
			return null;
		}
		protected virtual GameObject CreatePolygonWithHole (List<string> innerShapes, string outerShape, string type, string id)
		{
			//Vector3[] innerVertices = SumoUtils.SumoShapeToVector3Array (innerShape);
			Vector3[] outerVertices = SumoUtils.SumoShapeToVector3Array (outerShape);
			List<Vector3[]> ishape = new List<Vector3[]>();
			int totalInner=0;
			foreach (string s in innerShapes) {
				Vector3[] innerVertices = SumoUtils.SumoShapeToVector3Array (s); //Trim last one
				if (innerVertices.Length >=3) {
					totalInner += innerVertices.Length;
					ishape.Add(innerVertices);
				}
			}
			//Debug.Log("totalInner="+totalInner+"outer="+outerVertices.Length);
			if (outerShape.Length >= 3 ) {
				GameObject go = SumoUtils.CreateTriangulationWithHole (type, id,  outerVertices, ishape, totalInner);
				if (!useOPAL) {
					go.isStatic = true;
				} else {

					AssignOpalProperties (go, type);
				}
				if (type.Contains ("building")) {
					ElevateBuildingWithHole (go, id, outerVertices, ishape);
					//Use name 
					string bname=GetBuildingName(id);
					if (bname != null) {
						go.name = id + "-" + bname;
					}
				} else {
					//Update face ids
					if (useOPAL) {
						UnityEngine.Mesh srcMesh=GetComponentMesh(go);
						EqualFace eqF=new EqualFace();
						Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
						List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(srcMesh,ref globalFaceId, faceMap);
						Debug.Log("globalFaceId="+globalFaceId);
						OpalMeshProperties mp = go.GetComponent<Opal.OpalMeshProperties>();
						mp.SetFaceIds(tf);
					}
				}

				return go;
			} else {
				Debug.Log (id + " has fewer than 3 corners");
				return null;
			}
		}
		protected override GameObject CreatePolygon (string shape, string type, string id)
		{
			Vector3[] corners = SumoUtils.SumoShapeToVector3Array (shape);
			if (corners.Length >= 3) {
				GameObject go = SumoUtils.CreateTriangulation (type, id, corners);
				if (!useOPAL) {
					go.isStatic = true;
				} else {

					AssignOpalProperties (go, type);
				}
				if (type.Contains ("building")) {
					ElevateBuilding (go, id, corners);
					//Use name 
					string bname=GetBuildingName(id);
					if (bname != null) {
						go.name = id + "-" + bname;
					}
				} else {
					//Update face ids
					if (useOPAL) {
						UnityEngine.Mesh srcMesh=GetComponentMesh(go);
						EqualFace eqF=new EqualFace();
						Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
						List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(srcMesh,ref globalFaceId, faceMap);
						Debug.Log("globalFaceId="+globalFaceId);
						OpalMeshProperties mp = go.GetComponent<Opal.OpalMeshProperties>();
						mp.SetFaceIds(tf);
					}
				}

				return go;
			} else {
				Debug.Log (id + " has fewer than 3 corners");
				return null;
			}
		}
		//protected override float GetBuildingHeight (string i)
		//{
		//	return -1.0f;
		//}
		//protected int GetBuildingLevels (string i) {
		//	return -1;
		//}
		public virtual void CreateFloor() {
			GameObject floor=GameObject.Find ("Floor");
			if (floor==null) {
				Debug.Log ("No Terrain or Floor found. Creating a plane as terrain");
				floor = GameObject.CreatePrimitive (PrimitiveType.Plane);
				floor.name = "Floor";
				floor.tag = "SimTerrain";
				float[] bound = SumoUtils.SumoConvBoundaryToFloat (polygons["additional"]["location"]["_attributes"]["convBoundary"].str);

				floor.transform.localScale = new  Vector3 (bound [2] * 0.15f, 1.0f, bound [3] * 0.15f); //give some margin
				floor.transform.position = new Vector3 (bound [2] * 0.5f, 0.0f, bound [3] * 0.5f);
				SumoScenarioInfo si = floor.AddComponent<SumoScenarioInfo> ();
				si.SetBounds (bound);
				if (useOPAL) {

					Opal.StaticMesh opalsm = floor.AddComponent<Opal.StaticMesh> ();
					Opal.OpalMeshProperties opalmp = floor.AddComponent<Opal.OpalMeshProperties> ();
					//Concrete
					opalmp.emProperties.a = 5.31f;
					opalmp.emProperties.b = 0f;
					opalmp.emProperties.c = 0.0326f;
					opalmp.emProperties.d = 0.8905f;
				}



			}
			if (useOPAL) {
				UnityEngine.Mesh srcMesh=GetComponentMesh(floor);
				EqualFace eqF=new EqualFace();
				Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
				List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(srcMesh,ref globalFaceId, faceMap);
				OpalMeshProperties mp = floor.GetComponent<Opal.OpalMeshProperties>();
				mp.SetFaceIds(tf); 
				Debug.Log("Found floor and added face ids");
			}
		}
		protected override void ProcessPolygons ()
		{
			Debug.Log("Processing custom JSON polygons");
			List<JSONObject> pols;
			if (polygons ["additional"] ["poly"].IsArray) {
				pols = polygons ["additional"] ["poly"].list;
			} else if (polygons ["additional"] ["poly"].IsObject) {
				pols = new List<JSONObject> ();
				pols.Add (polygons ["additional"] ["poly"]);
			} else {
				Debug.Log ("JSON builder: no polygons found");
				throw new UnityException ("JSON builder: no polygons found");

				return;
			}
			CreateFloor();
			foreach (JSONObject o in pols ) {
				string type = o ["_attributes"] ["type"].str;
				if (InNoBuildList(type))  {
					//Do not create landuses
					continue;
				}
				GameObject polygon =null;
				if (o ["_attributes"].GetField("inner_shape")) {
					Debug.Log(o ["_attributes"] ["id"].str +" has inner_shape");
					List<string> innerShapes = new List<string>();
					foreach (JSONObject sh in o ["_attributes"] ["inner_shape"].list) {
						//Debug.Log(sh);
						innerShapes.Add(sh.str);
					} 
					polygon = CreatePolygonWithHole (innerShapes, o ["_attributes"] ["outer_shape"].str, type, o ["_attributes"] ["id"].str);
				} else {
					//Polygon without hole
					Debug.Log(o ["_attributes"] ["id"].str);
					polygon = CreatePolygon (o ["_attributes"] ["outer_shape"].str, type, o ["_attributes"] ["id"].str);
				}

				if (polygon != null) {
					SetPolygonMaterial (polygon, type, o ["_attributes"] ["color"].str);
					polygon.transform.SetParent (root.transform);
					numberOfObjects++;
				}

			}
			root.name = "Environment (" + numberOfObjects + " objects. " + opalStaticMeshes + " Opal StaticMeshes. " +numberOfOpalEdges +" OpalEdges)";
		}
		protected virtual void ElevateBuildingWithHole (GameObject go, string id, Vector3[] outerVertices, List<Vector3[]> innerVertices)
		{
			List<Matrix4x4> sections = new List<Matrix4x4> ();
			float totalHeight = CreateSections(sections, id);
			UnityEngine.Mesh m = new UnityEngine.Mesh ();
			if (useOPAL) {
				List<MeshExtrusion.Edge> outerEdges = new List<MeshExtrusion.Edge>(); //For diffraction
				List<MeshExtrusion.Edge> totalEdges = new List<MeshExtrusion.Edge>(); //For extrusion
				for (int i = 0; i < outerVertices.Length; i++) { 
					MeshExtrusion.Edge me=	new MeshExtrusion.Edge();
					me.vertexIndex[0]=i;
					me.vertexIndex[1]=(i+1) % outerVertices.Length;
					outerEdges.Add(me);

				}
				totalEdges.AddRange(outerEdges);
				int li = outerVertices.Length;
				outerEdges.RemoveAt(outerEdges.Count-1); //Remove last for diffraction
				List<List<MeshExtrusion.Edge>> innerEdges = new List<List<MeshExtrusion.Edge>>(); //For diffraction. Remove last edge
				foreach (Vector3[] innerShape in innerVertices) {
					List<MeshExtrusion.Edge> lis=new List<MeshExtrusion.Edge>();		
					List<MeshExtrusion.Edge> lisTotal=new List<MeshExtrusion.Edge>();		
					for (int i = 0; i < innerShape.Length; i++) {
						MeshExtrusion.Edge me=	new MeshExtrusion.Edge();
						me.vertexIndex[0]=i;
						me.vertexIndex[1]=(i + 1) % innerShape.Length;
						me.vertical=false;
						lis.Add(me);
						MeshExtrusion.Edge meTotal=	new MeshExtrusion.Edge();
						meTotal.vertexIndex[0]=li+i;
						meTotal.vertexIndex[1]=li+((i + 1) % innerShape.Length);
						meTotal.vertical=false;
						lisTotal.Add(meTotal);
					}
					li += innerShape.Length;
					if (lis.Count>0) {
						totalEdges.AddRange(lisTotal);
						lis.RemoveAt(lis.Count-1);
						innerEdges.Add(lis);
					}

				}
				//Extrusion
				UnityEngine.Mesh srcMesh=GetComponentMesh(go);
				MeshExtrusion.Edge[] edges= totalEdges.ToArray();
				List<OpalTriangleFace> tf=new List<OpalTriangleFace>();
				MeshExtrusion.ExtrudeVerticalMesh (srcMesh, m, sections.ToArray (), edges, false, tf,  globalFaceId);
				//Create outer edges
				CreateOpalEdges(go, srcMesh, m, outerEdges.ToArray() ,totalHeight, tf);
				//Update globalFaceId	
				globalFaceId += (uint) outerEdges.Count +2;
				Debug.Log("globalFaceId="+globalFaceId);
				//Create inner edges
				for (int i=0; i < innerEdges.Count; i++) {
					CreateInnerOpalEdges(go, innerVertices[i], innerEdges[i].ToArray() ,totalHeight, tf);
					//Update globalFaceId	
					globalFaceId += (uint) innerEdges[i].Count +2;
					Debug.Log("globalFaceId="+globalFaceId);
				}

				//Now inner meshes
				//	UnityEngine.Mesh mi = new UnityEngine.Mesh ();
				//	Vector3[] miv=GetVerticesFromEdges(srcMesh, innerEdges[0]);
				//	MeshExtrusion.ExtrudeInnerVerticalMesh (miv, mi, sections.ToArray (), innerEdges[0].ToArray(), true, tf,  globalFaceId);
				//	CombineInstance[] combine = new CombineInstance[2];
				//	combine[0].mesh=m;
				//	combine[1].mesh=mi;
				go.GetComponent<MeshFilter> ().mesh = m;
				//GetComponentMesh(go).CombineMeshes(combine,true,false) ;
			} else {
				MeshExtrusion.ExtrudeMesh (GetComponentMesh (go), m, sections.ToArray (), false);
				go.GetComponent<MeshFilter> ().mesh = m;
			}
			//go.GetComponent<MeshFilter> ().mesh = m;

		}
		protected virtual List<OpalEdge> CreateInnerOpalEdges(GameObject goParent, Vector3[] vertices, MeshExtrusion.Edge[] edges, float height, List<OpalTriangleFace> tf) {
			List<OpalEdge> opalEdges = new List<OpalEdge>();
			OpalMeshProperties mp = goParent.GetComponent<Opal.OpalMeshProperties>();
			mp.SetFaceIds(tf); 
			//uint eid=0;
			//for (int i=1; i<2; i++)
			GameObject go = goParent;
			for (int i=0; i<edges.Length; i++)
			{	

				//First vertical edges at outline vertices
				MeshExtrusion.Edge e=edges[i];

				//Debug.Log("e="+e.faceIndex[0]+","+e.faceIndex[1]);
				if (debugEdges) {
					go = new GameObject("Inner OpalEdge "+opalEdges.Count);
				}
				Debug.Log("Inner OpalEdge "+opalEdges.Count);
				OpalEdge oe=go.AddComponent<OpalEdge>();
				oe.opalMeshProperties=mp;
				//edge
				Vector3 v=Vector3.up*height;
				Vector3 o=vertices[e.vertexIndex[0]];
				//face a
				Vector3 a=vertices[e.vertexIndex[1]]-vertices[e.vertexIndex[0]];
				//Debug.Log("a="+a+"v1="+vertices[e.vertexIndex[1]]+"v0="+vertices[e.vertexIndex[0]]);
				uint aid=globalFaceId+(uint) e.faceIndex[0];
				int j=i-1;
				if (i==0) {
					j=edges.Length -1;
				}
				Vector3 na= Vector3.Cross(a,v).normalized;
				//face b
				MeshExtrusion.Edge prev=edges[j];
				Vector3 b = vertices[prev.vertexIndex[0]]-vertices[prev.vertexIndex[1]];
				Vector3 nb= Vector3.Cross(v,b).normalized;
				uint bid=globalFaceId + (uint) prev.faceIndex[0];
				//Check conventions
				float an=Vector3.SignedAngle(a,b,v);
				//Internal angle has to be positive from face A to face B. Reversing the edge orientation makes it
				bool revertedEdge=false;
				if (an<0) {
					o=o+v;
					v=-v;
					an=Vector3.SignedAngle(a,b,v);
					revertedEdge=true;
					//Debug.Log("i="+i+"revert edge orientation");
				}
				//Internal angle has to be less than 180 degrees. Since SignedAngle does not return angles > 180, the previous condition ensure this. Check anyway
				if (an>180.0f) {
					throw new System.InvalidOperationException ("CreateOpalEdges: internal angle (from face A to B) is greater than 180 degrees  " + an);
				}
				float anb=Vector3.SignedAngle(a,b,Vector3.Cross(na, a));
				//Normals has to point out according to the internal angle
				if (anb <0) {
					//Reverse normal
					na=-na;
					anb=Vector3.SignedAngle(a,b,Vector3.Cross(na, a));
					//Debug.Log("i="+i+"revert A normal");
					if (anb <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: A normal does not point out even after reversing  " + anb);
					}
				}
				float bna=Vector3.SignedAngle(b,a,Vector3.Cross(nb, b));
				//Normals has to point out according to the internal angle
				if (bna <0) {
					//Reverse normal
					nb=-nb;
					bna=Vector3.SignedAngle(b,a,Vector3.Cross(nb, b));
					//	Debug.Log("i="+i+"revert B normal");
					if (bna <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: B normal does not point out even after reversing  " + bna);
					}
				}
				oe.SetInfoValues(o,v,aid,bid,a,b,na,nb,globalEdgeId);	
				//TODO: This is awful but it is easier to just remove them after creating. Keep it here until a rewriting of the generation is done
				if (!e.vertical ) {
					if (debugEdges) {
						//Debug.Log("Destroying vertical" + opalEdges.Count);
#if UNITY_EDITOR
						DestroyImmediate(go);
#else
						Destroy(go);
#endif
					} else {
#if UNITY_EDITOR
						DestroyImmediate(oe);
#else
						Destroy(oe);
#endif
					}
				} else {

					opalEdges.Add(oe);
					numberOfOpalEdges++;
					globalEdgeId++;
					if (debugEdges) {
						go.transform.parent=goParent.transform;
					}
				}
				//Now compute corresponding horizontal edge on top
				if (debugEdges) {
					go = new GameObject("Inner OpalEdge "+opalEdges.Count);	
				}
				OpalEdge he=go.AddComponent<OpalEdge>();
				he.opalMeshProperties=mp;
				//edge
				o.y=height;
				Vector3 vh=b;
				Vector3 ah=-v;
				if (revertedEdge) {
					ah=v;
				}
				uint aidh=bid;
				uint bidh= globalFaceId +(uint) edges.Length;
				Vector3 bh=Vector3.Cross(ah,vh).normalized;
				na=nb;
				nb=Vector3.Cross(bh,vh).normalized;
				//Debug.DrawRay(o,vh,Color.blue, 100);
				//Debug.DrawRay(o,ah,Color.green, 100);
				//Debug.DrawRay(o,bh,Color.cyan, 100);
				//Debug.DrawRay(o,na,Color.red, 100);
				//Debug.DrawRay(o,nb,Color.red, 100);
				//Assing face_id
				//Check conventions
				an=Vector3.SignedAngle(ah,bh,vh);
				//Internal angle has to be positive from face A to face B. Reversing the edge orientation makes it
				if (an<0) {
					o=o+vh;
					vh=-vh;
					an=Vector3.SignedAngle(ah,bh,vh);
					//Debug.Log("i="+i+"hor revert edge orientation");
				}
				//Internal angle has to be less than 180 degrees. Since SignedAngle does not return angles > 180, the previous condition ensure this. Check anyway
				if (an>180.0f) {
					throw new System.InvalidOperationException ("CreateOpalEdges: horizontal internal angle (from face A to B) is greater than 180 degrees  " + an);
				}
				anb=Vector3.SignedAngle(ah,bh,Vector3.Cross(na, ah));
				//Normals has to point out according to the internal angle
				if (anb <0) {
					//Reverse normal
					na=-na;
					anb=Vector3.SignedAngle(ah,bh,Vector3.Cross(na, ah));
					//Debug.Log("i="+i+" hor revert A normal");
					if (anb <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: horizontal A normal does not point out even after reversing  " + anb);
					}
				}
				bna=Vector3.SignedAngle(bh,ah,Vector3.Cross(nb, bh));
				//Normals has to point out according to the internal angle
				if (bna <0) {
					//Reverse normal
					nb=-nb;
					bna=Vector3.SignedAngle(bh,ah,Vector3.Cross(nb, bh));
					//Debug.Log("i="+i+" hor revert B normal");
					if (bna <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: horizontal B normal does not point out even after reversing  " + bna);
					}
				}
				he.SetInfoValues(o,vh,aidh,bidh,ah,bh,na,nb,globalEdgeId);	
				if (!e.horizontal ) {
					if (debugEdges) {
#if UNITY_EDITOR
						DestroyImmediate(go);
#else
						Destroy(go);

#endif
					} else {
#if UNITY_EDITOR
						DestroyImmediate(he);
#else
						Destroy(he);

#endif
					}	
				} else {
					opalEdges.Add(he);
					numberOfOpalEdges++;
					globalEdgeId++;
					if (debugEdges) {
						go.transform.parent=goParent.transform;
					}
				}

			}	
			//Now filter potentially equal edges. It may be redundant in most cases
			//opalEdges=filterEdges(opalEdges);
			DebugEdges(opalEdges);
			return opalEdges;
		}
		public virtual Vector3[] GetVerticesFromEdges(UnityEngine.Mesh m, List<MeshExtrusion.Edge> edges) {
			List<Vector3> v= new List<Vector3>();
			Vector3[] vm=m.vertices;
			int nv=0;
			foreach (MeshExtrusion.Edge e in edges) {
				Debug.Log("e="+e.vertexIndex[0]+","+e.vertexIndex[1]);
				Debug.Log("vi="+vm[e.vertexIndex[0]]);
				v.Add(vm[e.vertexIndex[0]]);		
				e.vertexIndex[0]=nv;
				e.vertexIndex[1]=(nv+1) % edges.Count;
				nv++;
			}
			return v.ToArray();
		}
	}

}
