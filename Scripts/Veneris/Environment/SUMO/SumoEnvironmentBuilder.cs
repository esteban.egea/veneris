/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using Veneris.Osm;
using UnityEngine.Networking;
using System.IO;
using System;
using System.Xml.Serialization;
using System.Linq;
using System.Text.RegularExpressions;
using Opal;
namespace Veneris
{
	public class SumoEnvironmentBuilder : MonoBehaviour
	{
		
		public string pathToPolys = "";
		public string pathToOSMJSON = "";
		public bool saveOSMDataFromQuery = false;
		public string savedOSMDataPath = "";
		public float defaultBuildingHeight = 10f;
		public float defaultLevelHeight = 3f;
		public bool buildTrafficLights = true;
		public bool useOPAL = true;
		public bool onlyBuildingsWithOpal = false;
		public bool debugEdges = false;
		//TODO: this static variables do not seem to work with multiple SumoEnvironmentBuilder on the editor. Just made them public and YOU have to make sure that every editor starts in a different range
		public uint globalFaceId=0;
		public  uint globalEdgeId=0;
		public GameObject root = null;
		public SumoBuilder builder = null;
		protected Dictionary<long,JSONObject> osmIdToJSON = null;
		protected additionalType polygons = null;
		protected JSONObject osmJSONData = null;
		protected int numberOfObjects = 0;
		protected uint opalStaticMeshes = 0;
		protected uint numberOfOpalEdges = 0;
		//Make sure this is shared by all instances of SumoBuilder. Otherwise, the face ids will be repeated among meshes
		protected string[] noBuildTags = {
			"population",      
			"landuse",  
			"man_made",
			"amenity",

		};

		public bool InNoBuildList(string t) {
			for (int i = 0; i < noBuildTags.Length; i++) {
				if (t.Contains (noBuildTags [i])) {
					return true;
				}
			}
			return false;
		}

		public struct TrafficLightIndexPair
		{
			public TrafficLight tl;
			public int index;
		}

		public virtual void BuildPolygons (SumoBuilder builder)
		{
			numberOfObjects = 0;
			numberOfOpalEdges=0;
			opalStaticMeshes=0;
			this.builder = builder;
			root = new GameObject ("Environment");
			osmIdToJSON = new Dictionary<long, JSONObject> ();
			polygons = ReadPolygons ();
			locationType loc = GetLocationElement ();
			string bb = loc.origBoundary;
			if (string.IsNullOrEmpty (pathToOSMJSON)) {
				DownloadOSMBuildingData ();
			} else {
				osmJSONData = new JSONObject (File.ReadAllText (pathToOSMJSON));
				CreateOSMWayDictionary ();
				ProcessPolygons ();

			}
			if (buildTrafficLights) {
				BuildTrafficLightPosts ();
			}
			root.name = "Environment (" + numberOfObjects + " objects)";
		}

		public void BuildOnlyTrafficLights (SumoBuilder builder)
		{
			this.builder = builder;
			if (root == null) {
				root = new GameObject ("Environment");
			}
			BuildTrafficLightPosts ();
		}

		protected virtual void CreateOSMWayDictionary ()
		{
			List<JSONObject> elements = osmJSONData ["elements"].list;
			for (int i = 0; i < elements.Count; i++) {
				if ("way".Equals (elements [i] ["type"].str)) {
					//					Debug.Log (i + " " + elements [i] ["type"].str);

					//					Debug.Log (Convert.ToInt64 (elements [i] ["id"].n));
					osmIdToJSON.Add (Convert.ToInt64 (elements [i] ["id"].n), elements [i] ["tags"]);
				}
			}
			//Null osmJSONData??
		}

		public virtual additionalType ReadPolygons ()
		{

			if (string.IsNullOrEmpty (pathToPolys)) {
				Debug.Log ("No polygons file");
				return null;
			}
			XmlSerializer serializer = new XmlSerializer (typeof(additionalType));
			XmlReader reader = XmlReader.Create (pathToPolys);
			try {
				additionalType poly = (additionalType)serializer.Deserialize (reader);
				reader.Close ();
				return poly;
			} catch (System.Exception e) {
				Debug.LogError (e);
				reader.Close ();
				return null;
			}

		}

		public locationType GetLocationElement ()
		{
			//There might be more than one location element
			return polygons.Items.Where (x => x.GetType () == typeof(locationType)).ToArray () [0] as locationType;

		}

		public float[] originalBoundaryToNSWE (string b)
		{
			float[] NSWE = new float[4];
			string[] val = b.Split (',');
			NSWE [0] = float.Parse (val [3]); //North
			NSWE [1] = float.Parse (val [1]); //South
			NSWE [2] = float.Parse (val [0]); //West
			NSWE [3] = float.Parse (val [2]); //East
			return NSWE;


		}

		protected virtual void ProcessPolygons ()
		{
			GameObject floor=GameObject.Find ("Floor");
			if (floor!=null) {
				if (useOPAL) {
					UnityEngine.Mesh srcMesh=GetComponentMesh(floor);
					EqualFace eqF=new EqualFace();
                        		Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
					List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(srcMesh,ref globalFaceId, faceMap);
					OpalMeshProperties mp = floor.GetComponent<Opal.OpalMeshProperties>();
					mp.SetFaceIds(tf); 
				Debug.Log("Found floor and added face ids");
				}
			}
			foreach (object o in polygons.Items) {
				if (o.GetType () == typeof(polygonType)) {
					polygonType pol = o as polygonType;
					//Debug.Log(pol.id);
					//if (pol.type.Contains ("landuse") || pol.type.Contains ("population") || pol.type.Contains("amenity")) {
					if (InNoBuildList(pol.type)) {
						//Do not create, they are only informational overlays
						continue;
					}
					GameObject polygon = CreatePolygon (pol.shape, pol.type, pol.id);

					if (polygon != null) {
						SetPolygonMaterial (polygon, pol.type, pol.color);
						polygon.transform.SetParent (root.transform);
						numberOfObjects++;
					}
				}
			}
			root.name = "Environment (" + numberOfObjects + " objects. " + opalStaticMeshes + " Opal StaticMeshes. " +numberOfOpalEdges +" OpalEdges)";
		}

		protected virtual GameObject CreatePolygon (string shape, string type, string id)
		{
			Vector3[] corners = SumoUtils.SumoShapeToVector3Array (shape);
			if (corners.Length >= 3) {
				GameObject go = SumoUtils.CreateTriangulation (type, id, corners);
				if (!useOPAL) {
					go.isStatic = true;
				} else {
		
					AssignOpalProperties (go, type);
				}
				if (type.Contains ("building")) {
					ElevateBuilding (go, id, corners);
					//Use name 
					string bname=GetBuildingName(id);
					if (bname != null) {
						go.name = id + "-" + bname;
					}
				} else {
					//Update face ids
					if (useOPAL) {
						UnityEngine.Mesh srcMesh=GetComponentMesh(go);
						EqualFace eqF=new EqualFace();
						Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
						List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(srcMesh,ref globalFaceId, faceMap);
						Debug.Log("globalFaceId="+globalFaceId);
						OpalMeshProperties mp = go.GetComponent<Opal.OpalMeshProperties>();
						mp.SetFaceIds(tf);
					}
				}

				return go;
			} else {
				Debug.Log (id + " has fewer than 3 corners");
				return null;
			}
		}

		protected GameObject AssignOpalProperties (GameObject go, string type)
		{
			if (onlyBuildingsWithOpal) {
				if (type.Contains ("building")) {
					Opal.StaticMesh opalsm = go.AddComponent<Opal.StaticMesh> ();
					opalsm.meshId=opalStaticMeshes;
					Opal.OpalMeshProperties opalmp = go.AddComponent<Opal.OpalMeshProperties> ();
					//Brick
					opalmp.emProperties.a = 3.75f;
					opalmp.emProperties.c = 0.038f;
					opalsm.opalMeshProperties = opalmp;
					opalStaticMeshes++;
				}
			} else {
				Opal.StaticMesh opalsm = go.AddComponent<Opal.StaticMesh> ();
				opalsm.meshId=opalStaticMeshes;
				Opal.OpalMeshProperties opalmp = go.AddComponent<Opal.OpalMeshProperties> ();
				opalsm.opalMeshProperties = opalmp;

				if (type.Contains ("building")) {
					//Brick
					opalmp.emProperties.a = 3.75f;
					//opalmp.emProperties.c = 0.038f;
					opalmp.emProperties.c = 0.0f;
				} else if (type.Contains ("leisure.park") || type.Contains ("leisure.garden")) {
					//Dry ground...here it is very dry
					opalmp.emProperties.a = 3f;
					opalmp.emProperties.b = 0f;
					opalmp.emProperties.c = 0.00015f;
					opalmp.emProperties.d = 2.52f;
				} else if (type.Contains ("parking")) {
					//Concrete
					opalmp.emProperties.a = 5.31f;
					opalmp.emProperties.b = 0f;
					opalmp.emProperties.c = 0.0326f;
					opalmp.emProperties.d = 0.8905f;
				} else {
					//Some default
					//Dry ground...here it is very dry
					opalmp.emProperties.a = 3f;
					opalmp.emProperties.b = 0f;
					opalmp.emProperties.c = 0.00015f;
					opalmp.emProperties.d = 2.52f;
				}
				opalStaticMeshes++;
			}
			return go;
		}

		protected virtual void SetPolygonMaterial (GameObject go, string type, string colorVal)
		{ 
//			Debug.Log ("type=" + type);
			Material mat = Resources.Load<Material> ("Materials/" + type) as Material;
			if (mat == null) {
				// Create a new 2x2 texture ARGB32 (32 bit with alpha) and no mipmaps
				Texture2D texture = new Texture2D (2, 2, TextureFormat.ARGB32, false);
				string[] colorvalues = colorVal.Split (',');
				Color32 color = new Color32 (byte.Parse (colorvalues [0]), byte.Parse (colorvalues [1]), byte.Parse (colorvalues [2]), 255);
				// set the pixel values
				texture.SetPixel (0, 0, color);
				texture.SetPixel (1, 0, color);
				texture.SetPixel (0, 1, color);
				texture.SetPixel (1, 1, color);

				// Apply all SetPixel calls
				texture.Apply ();


				go.GetComponent<MeshRenderer> ().material.mainTexture = texture;


			} else {
				go.GetComponent<MeshRenderer> ().material = mat;
			}
		}
		protected virtual float CreateSections(List<Matrix4x4> sections, string id) {
			float h = GetBuildingHeight (id);
			int levels = GetBuildingLevels (id);
			float totalHeight=h;
			if (levels > 0) { //Have levels
				float levelHeight = defaultLevelHeight;//We do not have height. Just multiply levels by  defaultLevelHeight
				if (h > 0) {
					//Divide height by level and create sections
					levelHeight = h / levels;
				} else {
					totalHeight=levels*levelHeight;
				}




				//Debug.Log (id + " levelHeight=" + levelHeight + "h=" + h + " levels=" + levels);
				//Do not do separte triangles by levels
				//for (int l = levels; l > 0; l--) {
				//	sections.Add (Matrix4x4.TRS (new Vector3 (0f, levelHeight * l, 0f), Quaternion.identity, Vector3.one));

				//}

				
			} 
				if (h > 0) {
					sections.Add (Matrix4x4.TRS (new Vector3 (0f, h, 0f), Quaternion.identity, Vector3.one));
				} else {
					sections.Add (Matrix4x4.TRS (new Vector3 (0f, defaultBuildingHeight, 0f), Quaternion.identity, Vector3.one));
					totalHeight=defaultBuildingHeight;
				}
			
			/*if (h != null) {
				sections.Add (Matrix4x4.TRS (new Vector3 (0f, h, 0f), Quaternion.identity, Vector3.one));
			} else {
				sections.Add (Matrix4x4.TRS (new Vector3 (0f, 10f, 0f), Quaternion.identity, Vector3.one));
			}
			*/
			sections.Add (Matrix4x4.TRS (Vector3.zero, Quaternion.identity, Vector3.one));
			return totalHeight;

		}

		protected virtual void ElevateBuilding (GameObject go, string id, Vector3[] corners)
		{
			List<Matrix4x4> sections = new List<Matrix4x4> ();
			float totalHeight = CreateSections(sections, id);
			UnityEngine.Mesh m = new UnityEngine.Mesh ();
			if (useOPAL) {
				UnityEngine.Mesh srcMesh=GetComponentMesh(go);
				MeshExtrusion.Edge[] edges = MeshExtrusion.ExtractEdges(srcMesh);
				
				
				
				edges=SortEdges(edges);
				//for (int i=0; i<2; i++) {
				//for (int i=0; i<edges.Length; i++) {
				//	MeshExtrusion.Edge e=edges[i];
				//	Debug.Log ("e=" + e.vertexIndex [0] + "," + e.vertexIndex [1] +"facei="+e.faceIndex[0]+","+e.faceIndex[1]);
				//	Debug.DrawLine (srcMesh.vertices [e.vertexIndex [0]], srcMesh.vertices [e.vertexIndex [1]],Color.blue, 50);
				//}
				List<OpalTriangleFace> tf=new List<OpalTriangleFace>();
				MeshExtrusion.ExtrudeVerticalMesh (srcMesh, m, sections.ToArray (), edges, false, tf,  globalFaceId);
				CreateOpalEdges(go, srcMesh, m,  edges,totalHeight, tf);
				//Update globalFaceId	
				globalFaceId += (uint) edges.Length +2;
				Debug.Log("globalFaceId="+globalFaceId);
			} else {
				MeshExtrusion.ExtrudeMesh (GetComponentMesh (go), m, sections.ToArray (), false);
			}
			go.GetComponent<MeshFilter> ().mesh = m;

		}
		protected virtual MeshExtrusion.Edge[] SortEdges(MeshExtrusion.Edge[] edges) {
			List<MeshExtrusion.Edge> sorted= new List<MeshExtrusion.Edge>();
			MeshExtrusion.Edge last = new MeshExtrusion.Edge();
			bool reverse=false;
			for (int i=0; i<edges.Length; i++) {
				//Debug.Log("sort="+edges[i].vertexIndex[0]+","+edges[i].vertexIndex[1]);
				if (edges[i].vertexIndex[0]==edges.Length-1 &&  edges[i].vertexIndex[1]==0){ 
					//This is the last one
					last=edges[i];
					reverse=true; 
				} else if((edges[i].vertexIndex[1]==edges.Length-1 &&  edges[i].vertexIndex[0]==0) ) {
					last=edges[i]; 

				} else {
					sorted.Add(edges[i]);
				}

			}
			if (reverse) {
				sorted.Sort(new ReverseEdgeSorter());
			} else {
				sorted.Sort(new EdgeSorter());
			}
			sorted.Add(last);
			for (int i=0; i<sorted.Count; i++) {				
				sorted[i].faceIndex[0]=i;
				sorted[i].faceIndex[1]=((i+1) % sorted.Count);
			}
			
			return sorted.ToArray();
		}
		protected virtual List<OpalEdge> CreateOpalEdges(GameObject goParent, UnityEngine.Mesh srcMesh, UnityEngine.Mesh extrudedMesh, MeshExtrusion.Edge[] edges, float height, List<OpalTriangleFace> tf) {
			//EqualFace eqF= new EqualFace();
			//eqF.epsilon=1e-4f;
			//EqualFaceDifferenceVector eqF=new EqualFaceDifferenceVector();
                        //Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
			//uint cid=0;
			//Extract faces
			//List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(extrudedMesh,ref globalFaceId, faceMap);
			List<OpalEdge> opalEdges = new List<OpalEdge>();
			OpalMeshProperties mp = goParent.GetComponent<Opal.OpalMeshProperties>();
			mp.SetFaceIds(tf); 
			Vector3[] vertices = srcMesh.vertices;
			//uint eid=0;
			//for (int i=1; i<2; i++)
			GameObject go = goParent;
			for (int i=0; i<edges.Length; i++)
			{	
				
				//First vertical edges at outline vertices
				MeshExtrusion.Edge e=edges[i];
				
				//Debug.Log("e="+e.faceIndex[0]+","+e.faceIndex[1]);
				if (debugEdges) {
					 go = new GameObject("OpalEdge "+opalEdges.Count);
				}
				Debug.Log("OpalEdge "+opalEdges.Count);
				OpalEdge oe=go.AddComponent<OpalEdge>();
				oe.opalMeshProperties=mp;
				//edge
				Vector3 v=Vector3.up*height;
				Vector3 o=vertices[e.vertexIndex[0]];
				//face a
				Vector3 a=vertices[e.vertexIndex[1]]-vertices[e.vertexIndex[0]];
				//Debug.Log("a="+a+"v1="+vertices[e.vertexIndex[1]]+"v0="+vertices[e.vertexIndex[0]]);
				uint aid=globalFaceId+(uint) e.faceIndex[0];
				int j=i-1;
				if (i==0) {
					j=edges.Length -1;
				}
				Vector3 na= Vector3.Cross(a,v).normalized;
				//face b
				MeshExtrusion.Edge prev=edges[j];
				Vector3 b = vertices[prev.vertexIndex[0]]-vertices[prev.vertexIndex[1]];
				Vector3 nb= Vector3.Cross(v,b).normalized;
				uint bid=globalFaceId + (uint) prev.faceIndex[0];
				//Check conventions
				float an=Vector3.SignedAngle(a,b,v);
				//Internal angle has to be positive from face A to face B. Reversing the edge orientation makes it
				bool revertedEdge=false;
				if (an<0) {
					o=o+v;
					v=-v;
				 	an=Vector3.SignedAngle(a,b,v);
					revertedEdge=true;
					//Debug.Log("i="+i+"revert edge orientation");
				}
				//Internal angle has to be less than 180 degrees. Since SignedAngle does not return angles > 180, the previous condition ensure this. Check anyway
				if (an>180.0f) {
					throw new System.InvalidOperationException ("CreateOpalEdges: internal angle (from face A to B) is greater than 180 degrees  " + an);
				}
				float anb=Vector3.SignedAngle(a,b,Vector3.Cross(na, a));
				//Normals has to point out according to the internal angle
				if (anb <0) {
					//Reverse normal
					na=-na;
				 	anb=Vector3.SignedAngle(a,b,Vector3.Cross(na, a));
					//Debug.Log("i="+i+"revert A normal");
					if (anb <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: A normal does not point out even after reversing  " + anb);
					}
				}
				float bna=Vector3.SignedAngle(b,a,Vector3.Cross(nb, b));
				//Normals has to point out according to the internal angle
				if (bna <0) {
					//Reverse normal
					nb=-nb;
					bna=Vector3.SignedAngle(b,a,Vector3.Cross(nb, b));
					//	Debug.Log("i="+i+"revert B normal");
					if (bna <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: B normal does not point out even after reversing  " + bna);
					}
				}
				oe.SetInfoValues(o,v,aid,bid,a,b,na,nb,globalEdgeId);	
				//TODO: This is awful but it is easier to just remove them after creating. Keep it here until a rewriting of the generation is done
				if (!e.vertical ) {
					if (debugEdges) {
						//Debug.Log("Destroying vertical" + opalEdges.Count);
					#if UNITY_EDITOR
						DestroyImmediate(go);
					#else
						Destroy(go);
					#endif
					} else {
					#if UNITY_EDITOR
						DestroyImmediate(oe);
					#else
						Destroy(oe);
					#endif
					}
				} else {

					opalEdges.Add(oe);
					numberOfOpalEdges++;
					globalEdgeId++;
					if (debugEdges) {
						go.transform.parent=goParent.transform;
					}
				}
				//Now compute corresponding horizontal edge on top
				if (debugEdges) {
					go = new GameObject("OpalEdge "+opalEdges.Count);	
				}
				OpalEdge he=go.AddComponent<OpalEdge>();
				he.opalMeshProperties=mp;
				//edge
				o.y=height;
				Vector3 vh=b;
				Vector3 ah=-v;
				if (revertedEdge) {
					ah=v;
				}
				uint aidh=bid;
				uint bidh= globalFaceId +(uint) edges.Length;
				Vector3 bh=Vector3.Cross(ah,vh).normalized;
				na=nb;
				nb=Vector3.Cross(bh,vh).normalized;
				//Debug.DrawRay(o,vh,Color.blue, 100);
				//Debug.DrawRay(o,ah,Color.green, 100);
				//Debug.DrawRay(o,bh,Color.cyan, 100);
				//Debug.DrawRay(o,na,Color.red, 100);
				//Debug.DrawRay(o,nb,Color.red, 100);
				//Assing face_id
				//Check conventions
				an=Vector3.SignedAngle(ah,bh,vh);
				//Internal angle has to be positive from face A to face B. Reversing the edge orientation makes it
				if (an<0) {
					o=o+vh;
					vh=-vh;
				 	an=Vector3.SignedAngle(ah,bh,vh);
					//Debug.Log("i="+i+"hor revert edge orientation");
				}
				//Internal angle has to be less than 180 degrees. Since SignedAngle does not return angles > 180, the previous condition ensure this. Check anyway
				if (an>180.0f) {
					throw new System.InvalidOperationException ("CreateOpalEdges: horizontal internal angle (from face A to B) is greater than 180 degrees  " + an);
				}
				anb=Vector3.SignedAngle(ah,bh,Vector3.Cross(na, ah));
				//Normals has to point out according to the internal angle
				if (anb <0) {
					//Reverse normal
					na=-na;
				 	anb=Vector3.SignedAngle(ah,bh,Vector3.Cross(na, ah));
					//Debug.Log("i="+i+" hor revert A normal");
					if (anb <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: horizontal A normal does not point out even after reversing  " + anb);
					}
				}
				bna=Vector3.SignedAngle(bh,ah,Vector3.Cross(nb, bh));
				//Normals has to point out according to the internal angle
				if (bna <0) {
					//Reverse normal
					nb=-nb;
					bna=Vector3.SignedAngle(bh,ah,Vector3.Cross(nb, bh));
					//Debug.Log("i="+i+" hor revert B normal");
					if (bna <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: horizontal B normal does not point out even after reversing  " + bna);
					}
				}
				he.SetInfoValues(o,vh,aidh,bidh,ah,bh,na,nb,globalEdgeId);	
				if (!e.horizontal ) {
					if (debugEdges) {
			#if UNITY_EDITOR
					DestroyImmediate(go);
			#else
					Destroy(go);

			#endif
					} else {
			#if UNITY_EDITOR
					DestroyImmediate(he);
			#else
					Destroy(he);

			#endif
					}	
				} else {
					opalEdges.Add(he);
					numberOfOpalEdges++;
					globalEdgeId++;
					if (debugEdges) {
						go.transform.parent=goParent.transform;
					}
				}
			
			}	
			//Now filter potentially equal edges. It may be redundant in most cases
			//opalEdges=filterEdges(opalEdges);
			DebugEdges(opalEdges);
			return opalEdges;
		}
		protected void DebugFaceNormals(Dictionary<Vector3, uint> faceMap, Vector3 n) {
			foreach (Vector3 v in faceMap.Keys) {
				Vector3 dif=n-v;
				Debug.Log("normal="+v.ToString("E6")+"sqdif="+dif.sqrMagnitude);
			}	
		}
		protected virtual List<OpalEdge> filterEdges(List<OpalEdge> opalEdges) {
			EqualFace eqF= new EqualFace();
			List<int> removable=new List<int>();
			for (int i=opalEdges.Count -1; i>=0; i--) {
				int next=i-2;
				if (i==1) {
					next=opalEdges.Count-1;
				} else if (i==0) {
					next=opalEdges.Count-2;
				}
				
				if ((i%2)==0) {	
					if (eqF.Equals(opalEdges[i].info.n_b,opalEdges[next].info.n_b)) {
						Debug.Log("egdes "+i+" and "+next+"are equal");
						//Merge
						opalEdges[i].info.b=opalEdges[i].info.b+opalEdges[next].info.b;
						removable.Add(next);

					}
				} else {
					if (eqF.Equals(opalEdges[i].info.n_a,opalEdges[next].info.n_a)) {
						Debug.Log("horizontal egdes "+i+" and "+next+"are equal");
						//Merge
						opalEdges[next].info.v=opalEdges[next].info.v+opalEdges[i].info.v;
						removable.Add(i);

					}
				}
			}
			foreach (int i in removable) {
					#if UNITY_EDITOR
						DestroyImmediate(opalEdges[i]);
					#else
						Destroy(opalEdges[i]);
					#endif
					opalEdges.RemoveAt(i);	
			}
			return opalEdges;
		}
		protected virtual void DebugEdges(List<OpalEdge> opalEdges) {
			foreach (OpalEdge e in opalEdges) {
				Debug.DrawRay(e.info.o,e.info.v,Color.blue, 10);
				Debug.DrawRay(e.info.o,e.info.a,Color.green, 10);
				Debug.DrawRay(e.info.o,e.info.b,Color.cyan, 10);
				Debug.DrawRay(e.info.o,e.info.n_a,Color.red, 10);
				Debug.DrawRay(e.info.o,e.info.n_b,Color.red, 10);
			}
		}
		protected virtual UnityEngine.Mesh GetComponentMesh (GameObject go)
		{
			#if UNITY_EDITOR
			return go.GetComponent<MeshFilter> ().sharedMesh;
			#else
			return go.GetComponent<MeshFilter> ().mesh;
			#endif
		}

		protected virtual float GetBuildingHeight (string i)
		{
			//Debug.Log ("GBH=" + i);
			if (i.Contains ("#")) {
				int position = i.LastIndexOf ('#');
				i = i.Substring (0, position);
			}
			long id = long.Parse (i);
			if (osmIdToJSON.ContainsKey (id)) { //The ids may change because the map has changed. If the network was generated and later OSM data is downloaded
				JSONObject o = osmIdToJSON [id];

				if (o != null && o.HasField ("height")) {
					//Sometimes units like m are used, try to match just numbers 
					Match match=Regex.Match(o ["height"].str,@"\d*\.*\d*");
					if (match.Success) {
						return float.Parse (match.Value);
					} else {
						return float.Parse (o ["height"].str);
					}
					return float.Parse (o ["height"].str);
				} else if (o != null && o.HasField ("building:height")) { //Now OSM use these tags
					//Sometimes units like m are used, try to match just numbers 
					Match match=Regex.Match(o ["building:height"].str,@"\d*\.*\d*");
					if (match.Success) {
						return float.Parse (match.Value);
					} else {
						return float.Parse (o ["building:height"].str);
					}
				} else {
					return -1f;
				}
			} else {
				return -1f;
			}
		}

		protected virtual int GetBuildingLevels (string i)
		{
			//Debug.Log ("GBL=" + i);
			if (i.Contains ("#")) {
				int position = i.LastIndexOf ('#');
				i = i.Substring (0, position);
			}

			long id = long.Parse (i);
			if (osmIdToJSON.ContainsKey (id)) {
				JSONObject o = osmIdToJSON [id];
				if (o != null && o.HasField ("building:levels")) {
					return int.Parse (o ["building:levels"].str);
				} else if (o != null && o.HasField ("levels")) {
					return int.Parse (o ["levels"].str);
				} else {
					return -1;
				}
			} else {
				return -1;
			}

		}
		protected virtual string GetBuildingName (string i)
		{
			//Debug.Log ("GBL=" + i);
			if (i.Contains ("#")) {
				int position = i.LastIndexOf ('#');
				i = i.Substring (0, position);
			}

			long id = long.Parse (i);
			if (osmIdToJSON.ContainsKey (id)) {
				JSONObject o = osmIdToJSON [id];
				if (o != null && o.HasField ("name")) {
					return o ["name"].str;
				} else {
					return null;
				}
			} else {
				return null;
			}

		}




		public void DownloadOSMData (string bb)
		{
			float[] nswe = originalBoundaryToNSWE (bb);
			string url = "http://overpass-api.de/api/interpreter";
			string query = "<osm-script output=\"json\" timeout=\"240\" element-limit=\"1073741824\"><union>";
			string bbox = "<bbox-query n=\"{0}\" s=\"{1}\" w=\"{2}\" e=\"{3}\"/><recurse type=\"node-relation\" into=\"rels\"/>";
			string queryend = "<recurse type=\"node-way\"/><recurse type=\"way-relation\"/></union><union><item/><recurse type=\"way-node\"/></union><print mode=\"body\"/></osm-script>";
			string overpassQuery = string.Format (query + bbox + queryend, nswe [0], nswe [1], nswe [2], nswe [3]);
			StartCoroutine (RunOverpassQuery (overpassQuery, DownloadOSMDataFinished));



		}

		public IEnumerator RunOverpassQuery (string overpassQuery, Action<string> f)
		{
			string url = "http://overpass-api.de/api/interpreter";
			Debug.Log (overpassQuery);
			Debug.Log ("Downloading OSM data......");
			WWWForm form = new WWWForm ();
			form.AddField ("data", overpassQuery);
			/*ObservableWWW.Post(url,form)
				.Subscribe(
					text => {f(text); }, //success
					exp => Debug.Log("Error fetching -> " + url + "query="+overpassQuery)); //failure
					*/
			using (UnityWebRequest www = UnityWebRequest.Post (url, form)) {
				yield return www.SendWebRequest ();
				//Debug.Log ("Error fetching -> " + url + "query=" + overpassQuery + "with error:" + www.error);
				if (www.isNetworkError || www.isHttpError) {
					Debug.Log ("Error fetching -> " + url + "query=" + overpassQuery + "with error:" + www.error);
				} else {
					Debug.Log ("OverpassQuery correctly finished ");
					f (www.downloadHandler.text);
				}
			}

		}

		protected virtual void DownloadOSMBuildingData ()
		{
			Debug.Log ("Downloading building data");
			string query = "[out:json];(";
			foreach (object o in polygons.Items) {
				if (o.GetType () == typeof(polygonType)) {
					polygonType pol = o as polygonType;
					if (pol.type.Contains ("building")) {
						if (pol.id.Contains ("#")) {
							int position = pol.id.LastIndexOf ('#');
							string id = pol.id.Substring (0, position);
							query += "way(" + id + ");";
						} else {
							query += "way(" + pol.id + ");";
						}
					}

				}
			}
			query += "); (._;>;); out body;";
			StartCoroutine (RunOverpassQuery (query, DownloadOSMDataFinished));

		}

		protected void DownloadOSMDataFinished (string text)
		{
			Debug.Log ("Finished downloading data");
			osmJSONData = GetOSMDataAsJSON (text);
			if (saveOSMDataFromQuery) {
				if (string.IsNullOrEmpty(savedOSMDataPath)) {
					Debug.Log("Path to saved OSM data is empty. Not saving");
				} else {
					File.WriteAllText(savedOSMDataPath,text);
				}
			}
			CreateOSMWayDictionary ();
			ProcessPolygons ();
		}

		public virtual JSONObject GetOSMDataAsJSON (string text)
		{

			return new JSONObject (text);
		}


		protected void BuildTrafficLightPosts ()
		{
			List<VenerisRoad> roads = builder.GetRoads ();

			for (int i = 0; i < roads.Count; i++) {
				
			
				if (TrafficLightsAtIntersection (roads [i].lanes) > 0) {
					GameObject tlp = BuildTrafficLightPostObject (roads [i].lanes);
					if (tlp != null) {
						tlp.transform.SetParent (root.transform);
					}
				}

					
			}
		}

	
		protected int TrafficLightsAtIntersection (VenerisLane[] lanes)
		{
			int tls = 0;
			for (int i = 0; i < lanes.Length; i++) {
				if (lanes [i].endIntersection != null) {
					tls += lanes [i].endIntersection.transform.GetComponent<StopLineInfo> ().numberOfTrafficLights;
				}
					
			}
			return tls;
		}

		protected int NumberOfTrafficLightsAtStopLine (VenerisLane lane)
		{
			

			if (lane.endIntersection != null) {
				return lane.endIntersection.transform.GetComponent<StopLineInfo> ().numberOfTrafficLights;
			} else {
				return 0;
			}

		}

		protected List<TrafficLightIndexPair> GetTrafficLightsAtStopLine (VenerisLane lane)
		{


			if (lane.endIntersection != null) {
				List<TrafficLightIndexPair> tlList = new List<TrafficLightIndexPair> ();
				PathConnector pc = lane.endIntersection.transform.GetComponentInChildren<PathConnector> ();
				List<long> pathids = pc.GetIncomingPathsToConnector ();
				for (int i = 0; i < pathids.Count; i++) {
					ConnectionInfo info = pc.GetPathsConnectedTo (pathids [i]);
					for (int j = 0; j < info.connectedPaths.Count; j++) {
						if (info.connectedPaths [j].trafficLight != null) {
							TrafficLightIndexPair pair;
							pair.tl = info.connectedPaths [j].trafficLight;
							pair.index = info.connectedPaths [j].trafficLightIndex;
							tlList.Add (pair);


						}
					}
				}
				return tlList;
			} else {
				return null;
			}

		}

		protected GameObject BuildTrafficLightPostObject (VenerisLane[] lanes)
		{
			if (lanes [lanes.Length - 1].endIntersection != null) {
				GameObject postPrefab = LoadPostPrefab (lanes.Length);
				TrafficLightObjectBuilder ob = new TrafficLightObjectBuilder ();
				GameObject tlpost = ob.CreatePostFromPrefab (postPrefab); 
				//TODO: We are assuming the leftmost lane has always the higher index...


				tlpost.transform.rotation = lanes [lanes.Length - 1].endIntersection.transform.rotation;
				tlpost.transform.position = lanes [lanes.Length - 1].endIntersection.transform.position;

				tlpost.transform.Translate (Vector3.left * lanes [lanes.Length - 1].laneWidth * 0.7f);

				//TODO: Scale a little bit, make it better
				//float fraction=3.2f/5.05f*lanes.Length; //(standard lane width/bar length)*number of lanes
				//tlpost.transform.localScale= new Vector3(fraction,1.0f,1.0f);
				//Now add traffic lights to post

				GameObject tlPrefab = LoadTrafficLightPrefab ();
				float baseOffset = 0f;
				for (int i = lanes.Length - 1; i >= 0; i--) {
					//int numberOfLights = TrafficLightsAtStopLine (lanes [i]);
					List<TrafficLightIndexPair> tlList = GetTrafficLightsAtStopLine (lanes [i]);
					if (tlList != null) {
						float laneOffset = (lanes [i].laneWidth) / (tlList.Count + 1);

						for (int j = 0; j < tlList.Count; j++) {
							GameObject tl = ob.AddTrafficLightPrefab (tlPrefab, laneOffset * (j + 1) + baseOffset);
							TrafficLightSet tlset = tl.AddComponent<TrafficLightSet> ();
							tlset.SetTrafficLight (tlList [j].tl);
							tlset.SetIndex (tlList [j].index);


						}

					}
					baseOffset += lanes [i].laneWidth;
				}

				return tlpost;
			} else {
				return null;
			}

		}

		protected virtual GameObject  LoadPostPrefab (int lanes)
		{
			if (lanes > 1) {
				return Resources.Load ("Prefabs/Signs/BasicTrafficLightPost" + lanes) as GameObject;
			} else {
				return Resources.Load ("Prefabs/Signs/BasicTrafficLightPost") as GameObject;
			}
		}

		protected virtual GameObject  LoadTrafficLightPrefab ()
		{
			return Resources.Load ("Prefabs/Signs/BasicTrafficLight") as GameObject;
		}

	}
}
