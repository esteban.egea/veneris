/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalPowerLogger : MonoBehaviour {

	// Use this for initialization
	public string path;
	public string logName;
	public Opal.Antenna[]  receivers;
	protected Veneris.FileResultLogger logger;
	protected bool ended=false;
	void Awake () {

		receivers = GameObject.FindObjectsOfType<Opal.Antenna> ();

		for (int i = 0; i < receivers.Length; i++) {
			receivers [i].RegisterPowerListener (LogPower);
		}
		Debug.Log (receivers.Length + " receivers registered");
		logger = new Veneris.FileResultLogger (path, logName, true, false);
		logger.CreateStream ();
	}
	protected void LogPower (int rxId, float power, int txId)
	{
		Debug.Log ("GlobalPowerLogger:"+txId + "\t" + rxId + "\t" + power);
		logger.RecordWithTimestamp (txId+"\t"+rxId+"\t" + power);
	}

	void Clean() {
		if (!ended) {
			Debug.Log("Cleaning logger");
			logger.Close ();
			for (int i = 0; i < receivers.Length; i++) {
				if (receivers[i]!=null) {
					receivers [i].RemovePowerListener (LogPower);
				}
			}
		}
		ended=true;
	}
	void OnApplicationQuit() {
		Clean();
	}
	void OnDestroy ()
	{
		Clean();
	}

}
