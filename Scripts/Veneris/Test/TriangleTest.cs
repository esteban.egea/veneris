using UnityEngine;
using System;
using System.Collections.Generic;
using TriangleNet;
using TriangleNet.Geometry;
using System.Linq;
namespace Veneris {
	public class TriangleTest : MonoBehaviour
	{
		public void Start() {
			GameObject eo = new GameObject("circle");
			UnityEngine.Mesh mesh = new 	UnityEngine.Mesh ();

			//UnityEngine.Mesh mesh  = eo.AddComponent<MeshFilter> ().mesh;

			eo.AddComponent<MeshRenderer> ();
			MeshData data = new MeshData ();
			var inp=CreateSquare();
			TriangleNet.Mesh _mesh = new TriangleNet.Mesh ();
			_mesh.Behavior.Algorithm = TriangulationAlgorithm.Incremental;
			_mesh.Behavior.Quality = true;
			_mesh.Triangulate (inp);
			List<Vector3> verts =_mesh.Vertices.Select (x => new Vector3 (System.Convert.ToSingle(x.X), 0, System.Convert.ToSingle(x.Y))).ToList ();
			data.Vertices.AddRange (verts);
			int vertsStartCount=0;
			foreach (var tri in _mesh.Triangles) {
				data.Indices.Add (vertsStartCount + tri.P1);
				data.Indices.Add (vertsStartCount + tri.P0);
				data.Indices.Add (vertsStartCount + tri.P2);


			}
			//Create UVs
			Vector2 min = FindMinCoordinates(verts.ToArray());
			Vector2 max = FindMaxCoordinates(verts.ToArray());
			float rangex = max.x - min.x;
			float rangey = max.y - min.y;


			foreach (Vector3 c in verts)
			{

				data.UV.Add(new Vector2((c.x - min.x)/rangex, (c.z - min.y)/rangey));
			}
			
			mesh.vertices = data.Vertices.ToArray ();
			mesh.triangles = data.Indices.ToArray ();
			mesh.SetUVs (0, data.UV);
			mesh.RecalculateNormals ();
			eo.AddComponent<MeshFilter> ().mesh = mesh;
			//var ee = _mesh.Edges;
			var ee = inp.Segments;
			//for (int i =0 ; i<4; i++) {
			int i=0;
			List<MeshExtrusion.Edge> innerEdges = new List<MeshExtrusion.Edge>();
			List<MeshExtrusion.Edge> outerEdges = new List<MeshExtrusion.Edge>();
			List<Vector3>innerVertices = new List<Vector3>();
			foreach (TriangleNet.Geometry.Edge e in ee) {
				MeshExtrusion.Edge me=	new MeshExtrusion.Edge();
				if (i<4) {
					Debug.DrawLine(mesh.vertices[e.P0],mesh.vertices[e.P1],Color.blue,10);
					me.vertexIndex[0]=e.P0;
					me.vertexIndex[1]=e.P1;
					outerEdges.Add(me);
				} else {
					Debug.DrawLine(mesh.vertices[e.P0],mesh.vertices[e.P1],Color.red,10);
					innerVertices.Add(mesh.vertices[e.P0]);
					me.vertexIndex[0]=e.P0;
					me.vertexIndex[1]=e.P1;
					innerEdges.Add(me);
				}
				i++;
			}

			Debug.Log("Centroid = " + MeshExtrusion.ComputeCentroid(innerVertices.ToArray()));

			List<Matrix4x4> sections = new List<Matrix4x4> ();
			sections.Add (Matrix4x4.TRS (new Vector3 (0f, 2.0f, 0f), Quaternion.identity, Vector3.one));
			sections.Add (Matrix4x4.TRS (Vector3.zero, Quaternion.identity, Vector3.one));
			UnityEngine.Mesh mo = new UnityEngine.Mesh ();
			UnityEngine.Mesh mi = new UnityEngine.Mesh ();
			MeshExtrusion.ExtrudeHollowMesh (mesh, mo,mi, sections.ToArray (), innerEdges.ToArray(), outerEdges.ToArray(), false);
			//MeshExtrusion.ExtrudeMesh (mesh, m, sections.ToArray (), false);
		//	eo.GetComponent<MeshFilter> ().mesh = mo;
			CombineInstance[] combine = new CombineInstance[2];
			combine[0].mesh=mo;
			combine[1].mesh=mi;
			eo.GetComponent<MeshFilter> ().mesh = new UnityEngine.Mesh();
			eo.GetComponent<MeshFilter> ().mesh.CombineMeshes(combine, false, false);
			
		}
		public static Vector2 FindMinCoordinates(Vector3[] shape) {
			Vector2 min = new Vector2 (float.MaxValue, float.MaxValue);
			foreach (Vector3 v in shape) {
				if (v.x <= min.x) {
					min.x = v.x;
				}
				if (v.z <= min.y) {
					min.y = v.z;
				}
			}
			return min;
		}
		public static Vector2 FindMaxCoordinates(Vector3[] shape) {
			Vector2 max = new Vector2 (float.MinValue, float.MinValue);
			foreach (Vector3 v in shape) {
				if (v.x >= max.x) {
					max.x = v.x;
				}
				if (v.z >= max.y) {
					max.y = v.z;
				}
			}
			return max;
		}
		public InputGeometry CreateSquare() {
			var geometry = new InputGeometry(8);

			// Add the points. 
			geometry.AddPoint(0.0, 0.0); // Index 0
			geometry.AddPoint(3.0, 0.0); // Index 1 etc.
			geometry.AddPoint(3.0, 3.0);
			geometry.AddPoint(0.0, 3.0, 1); // Add a marker.
			geometry.AddPoint(1.0, 1.0);
			geometry.AddPoint(2.0, 1.0);
			geometry.AddPoint(2.0, 2.0);
			geometry.AddPoint(1.0, 2.0);

			// Add the segments. Notice the zero based indexing (in
			// contrast to Triangle's file format).
			geometry.AddSegment(0, 1);
			geometry.AddSegment(1, 2);
			geometry.AddSegment(2, 3, 5); // Add a marker.
			geometry.AddSegment(3, 0, 5);
			geometry.AddSegment(4, 5);
			geometry.AddSegment(5, 6);
			geometry.AddSegment(6, 7);
			geometry.AddSegment(7, 4);

			// Add the hole.
			geometry.AddHole(1.5, 1.5);
			return geometry;
		}
		//public TriangleNet.Geometry.Polygon CreateRingPolygon(double r = 4.0, double h = 0.2)
		//{
		//	var poly = new Polygon();

		//	var center = new Point(0, 0);

		//	// Radius should be at least 4.0.
		//	r = Math.Max(r, 4.0);

		//	// Outer contour.
		//	poly.Add(Circle(r, center, h, 3));

		//	// Center contour (internal).
		//	poly.Add(Circle((r + 2.0) / 2, center, h, 2));

		//	// Inner contour (hole).
		//	poly.Add(Circle(2.0, center, h, 1), center);

		//	return poly;
		//}

		//public Contour Circle(double r, Point center, double h, int label = 0)
		//{
		//	int n = (int)(2 * Math.PI * r / h);

		//	var points = new List<Vertex>(n);

		//	double x, y, dphi = 2 * Math.PI / n;

		//	for (int i = 0; i < n; i++)
		//	{
		//		x = center.X + r * Math.Cos(i * dphi);
		//		y = center.Y + r * Math.Sin(i * dphi);

		//		points.Add(new Vertex(x, y, label));
		//	}

		//	return new Contour(points, label, true);
		//}
	}
}
