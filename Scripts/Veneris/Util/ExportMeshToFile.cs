﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace Veneris {
public class ExportMeshToFile : MonoBehaviour {


		public string pathToFile;
		int index=0;
		public bool showTransformationMatrix=false;
		public bool exportToFile=true;
		public bool drawVertices = false;
		public TextMesh tmPrefab;
		public void SaveMeshToFile (string name)
		{
			Debug.Log ("Saving mesh to " + pathToFile);
			MeshFilter meshFilter = GetComponent<MeshFilter> ();
			int[] indices = meshFilter.mesh.triangles;
			Vector3[] vertices = meshFilter.mesh.vertices;
			Matrix4x4 tm = transform.localToWorldMatrix;

			FileStream m_FileStream = new FileStream (name + "-v.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_mesh = new StreamWriter (m_FileStream, System.Text.Encoding.ASCII);
			FileStream m_FileStream2 = new FileStream (name + "-i.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_tri = new StreamWriter (m_FileStream2, System.Text.Encoding.ASCII);
			FileStream m_FileStream3 = new FileStream (name + "-t.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_tm = new StreamWriter (m_FileStream3, System.Text.Encoding.ASCII);
		
		
			for (int i = 0; i < indices.Length; i++) {
				//Debug.Log ("index=" + indices [i]);
				m_tri.WriteLine (indices [i]);


			}
			for (int i = 0; i < vertices.Length; i++) {
				m_mesh.WriteLine (vertices [i].x.ToString ("E20") + "\t" + vertices [i].y.ToString ("E20") + "\t" + vertices [i].z.ToString ("E20"));
			}
			//translation matrix
			m_tm.WriteLine (tm.m00.ToString ("E20") + "\t" + tm.m01.ToString ("E20") + "\t" + tm.m02.ToString ("E20") + "\t" + tm.m03.ToString ("E20"));
			m_tm.WriteLine (tm.m10.ToString ("E20") + "\t" + tm.m11.ToString ("E20") + "\t" + tm.m12.ToString ("E20") + "\t" + tm.m13.ToString ("E20"));
			m_tm.WriteLine (tm.m20.ToString ("E20") + "\t" + tm.m21.ToString ("E20") + "\t" + tm.m22.ToString ("E20") + "\t" + tm.m23.ToString ("E20"));
			m_tm.WriteLine (tm.m30.ToString ("E20") + "\t" + tm.m31.ToString ("E20") + "\t" + tm.m32.ToString ("E20") + "\t" + tm.m33.ToString ("E20"));

		

			m_mesh.Flush ();
			m_mesh.Close ();
			m_tri.Flush ();
			m_tri.Close ();
			m_tm.Flush ();
			m_tm.Close ();

		}

		public void DrawMeshVertices ()
		{
			MeshFilter meshFilter = GetComponent<MeshFilter> ();
		
			Vector3[] vertices = meshFilter.mesh.vertices;
			int[] indices = meshFilter.mesh.triangles;
			for (int i = 0; i < vertices.Length; i++) {
				GameObject go = GameObject.CreatePrimitive (PrimitiveType.Sphere);
				go.name = "Vertex " + i;
				go.transform.position = vertices [i];
				go.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
				TextMesh tm = GameObject.Instantiate (tmPrefab, vertices [i], Quaternion.identity) as TextMesh;
				tm.name = "Text "+(i).ToString ();
				tm.transform.parent = transform;
				tm.text = (i).ToString ()+":"+tm.transform.position.ToString();

			}

		}
		public void ShowTransformationMatrix()
		{
			Matrix4x4 tm = transform.localToWorldMatrix;
			Debug.Log ("Transform of " + transform.name);
			Debug.Log (tm);
		}
	// Use this for initialization
	void Start () {
			if (exportToFile) {
				if (pathToFile.Length != 0) {
					SaveMeshToFile (pathToFile+"/"+transform.name);
				}
			}
			if (drawVertices) {
				DrawMeshVertices();
			}
			if (showTransformationMatrix) {
				ShowTransformationMatrix ();
			}
	}
		/*void Update() {
			MeshFilter meshFilter = GetComponent<MeshFilter> ();

			Vector3[] vertices = meshFilter.mesh.vertices;
			int[] indices = meshFilter.mesh.triangles;
			for (int i = 0; i < 6; i++) {
				
				GameObject go = GameObject.CreatePrimitive (PrimitiveType.Sphere);
				go.name = "Vertex " + index;
				go.transform.position = vertices [index];
				go.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
				index++;
			}
			
		}*/
	
	
}
}
