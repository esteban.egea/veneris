/******************************************************************************/
// 
// Copyright (c) 2019 Esteban Egea-Lopez http://ait.upct.es/eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Globalization;
using System.Threading;
namespace Opal
{
	public class SaveStaticMeshesToFolder : MonoBehaviour
	{

		[System.Serializable]
		public struct CurvatureDataPoint {
			public string x;	
			public string y;	
			public string z;	
			public string w;	
		}
		[System.Serializable]
		public class SerializedMesh {
			public string name;
			public Vector3[] vertices;
			public int[] indices;
			public Matrix4x4 tm;
			public MaterialEMProperties em;	
			public List<OpalTriangleFace> faceIds;
			public List<OpalEdgeInfo> edges;
			//Json does not allow infinity...
			public List<CurvatureDataPoint> pd1;
			public List<CurvatureDataPoint> pd2;
		}
		[System.Serializable]
		public class SerializedScenario {
			public List<SerializedMesh> meshes;
			public SerializedScenario() {
				meshes= new List<SerializedMesh>();
			}
		}

		public static string folder;
		public static string meshFolder;
		public static string jsonName;
		[MenuItem ("Opal/Set static mesh ids")]
			public static void SetStaticMeshIds ()
			{
				StaticMesh[] staticMeshes = FindObjectsOfType<StaticMesh> ();
				uint id=0; 
				for (int i = 0; i < staticMeshes.Length; i++) {
					staticMeshes[i].meshId=id;
					++id;
				}
			} 
			public static void MenuSaveJsonScenario() {
				jsonName="scenario";
				meshFolder="saved-scenarios";
				SaveJsonScenario(meshFolder, jsonName);
				
			}
			public static void SaveJsonScenario (string meshFolder, string jsonName)
			{
				SerializedScenario root= new SerializedScenario();

				CultureInfo ci = new CultureInfo("en-US");
				Thread.CurrentThread.CurrentCulture = ci;
				Thread.CurrentThread.CurrentUICulture = ci;
				folder = Directory.GetCurrentDirectory ();
				Directory.CreateDirectory (meshFolder);
				StaticMesh[] staticMeshes = FindObjectsOfType<StaticMesh> (); 
				int m = 0;
				List<SerializedMesh> lastList = new List<SerializedMesh> ();
				//for (int i = 0; i < 1; i++) {
				for (int i = 0; i < staticMeshes.Length; i++) {
					//if (staticMeshes[i].GetComponent<OpalMeshProperties> ().sendWithFaces) {
						SaveJsonStaticMesh (staticMeshes [i].transform, staticMeshes [i],root, lastList);
					//}
				}
				foreach (SerializedMesh s in lastList) {
					root.meshes.Add(s);
				}
				string json=JsonUtility.ToJson(root, true);
				FileStream lFile = new FileStream(folder+"/"+meshFolder+"/"+jsonName+".json",FileMode.Create, FileAccess.ReadWrite);
				StreamWriter l_sw= new StreamWriter (lFile, System.Text.Encoding.ASCII);
				l_sw.WriteLine(json);
				l_sw.Flush ();
				lFile.Close ();
				root=null;
			
			}
		[MenuItem ("Opal/Save Static Meshes to Current Folder")]
			public static void SaveStaticMeshesToCurrentFolder ()
			{ 
				CultureInfo ci = new CultureInfo("en-US");
				Thread.CurrentThread.CurrentCulture = ci;
				Thread.CurrentThread.CurrentUICulture = ci;
				folder = Directory.GetCurrentDirectory ();
				meshFolder="meshes";
				Directory.CreateDirectory (meshFolder);
				StaticMesh[] staticMeshes = FindObjectsOfType<StaticMesh> (); 
				int m = 0;
				List<string> fileList = new List<string> ();
				List<string> lastList = new List<string> ();
				for (int i = 0; i < staticMeshes.Length; i++) {
					if (staticMeshes[i].GetComponent<OpalMeshProperties> ().sendWithFaces) {
						SaveStaticMesh (staticMeshes [i].transform, staticMeshes [i],true, meshFolder);
						fileList.Add ("b"+staticMeshes [i].meshId);
					} else {
						SaveStaticMesh (staticMeshes [i].transform, staticMeshes [i],false, meshFolder);
						lastList.Add ("b"+staticMeshes [i].meshId);
					}				
				}
				//Save List to file
				FileStream lFile = new FileStream(folder+"/"+meshFolder+"/names.txt",FileMode.Create, FileAccess.ReadWrite);
				StreamWriter l_sw= new StreamWriter (lFile, System.Text.Encoding.ASCII);
				for (int i = 0; i < fileList.Count; i++) {
					l_sw.WriteLine (fileList [i]);
				}
				for (int i = 0; i < lastList.Count; i++) {
					l_sw.WriteLine (lastList [i]);
				}
				l_sw.Flush ();
				lFile.Close ();

			}
		[MenuItem ("Opal/Save Meshes and Edges to Current Folder")]
			public static void SaveMeshesEdgesToCurrentFolder ()
			{ 
				CultureInfo ci = new CultureInfo("en-US");
				Thread.CurrentThread.CurrentCulture = ci;
				Thread.CurrentThread.CurrentUICulture = ci;
				folder = Directory.GetCurrentDirectory ();
				meshFolder="anvers";
				Directory.CreateDirectory (meshFolder);
				StaticMesh[] staticMeshes = FindObjectsOfType<StaticMesh> (); 
				int m = 0;
				List<string> fileList = new List<string> ();
				List<string> lastList = new List<string> ();
				for (int i = 0; i < staticMeshes.Length; i++) {
					if (staticMeshes[i].GetComponent<OpalMeshProperties> ().sendWithFaces) {
						SaveStaticMesh (staticMeshes [i].transform, staticMeshes [i],true, meshFolder);
						fileList.Add ("b"+staticMeshes [i].meshId);
					} else {
						SaveStaticMesh (staticMeshes [i].transform, staticMeshes [i],false, meshFolder);
						lastList.Add ("b"+staticMeshes [i].meshId);
					}				
//fileList.Add (staticMeshes [i].transform.name);
				}
				OpalEdge[] edges = FindObjectsOfType<OpalEdge> (); 
				List<uint> edgeList = new List<uint> ();
				for (int i = 0; i < edges.Length; i++) {
					string name=folder+"/"+meshFolder+"/";
					uint id=SaveEdge(name,edges[i]);
					edgeList.Add(id);
				}

				//Save List to file
				FileStream lFile = new FileStream(folder+"/"+meshFolder+"/names.txt",FileMode.Create, FileAccess.ReadWrite);
				StreamWriter l_sw= new StreamWriter (lFile, System.Text.Encoding.ASCII);
				for (int i = 0; i < fileList.Count; i++) {
					l_sw.WriteLine (fileList [i]);
				}
				for (int i = 0; i < lastList.Count; i++) {
					l_sw.WriteLine (lastList [i]);
				}
				l_sw.Flush ();
				lFile.Close ();

				//Save List to file
				FileStream eFile = new FileStream(folder+"/"+meshFolder+"/edges.txt",FileMode.Create, FileAccess.ReadWrite);
				StreamWriter e_sw= new StreamWriter (eFile, System.Text.Encoding.ASCII);
				edgeList.Sort();
				for (int i = 0; i < edgeList.Count; i++) {
					e_sw.WriteLine (edgeList [i].ToString());
				}
				e_sw.Flush ();
				eFile.Close ();
			}
		protected static uint SaveEdge (string name, OpalEdge edge) {
			OpalEdgeInfo e=edge.info;
			OpalMeshProperties prop=edge.GetOpalMeshProperties();
			FileStream m_FileStream = new FileStream (name +e.id+ "-ed.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_edge = new StreamWriter (m_FileStream, System.Text.Encoding.ASCII);
			m_edge.WriteLine (e.v.x.ToString ("E8") + "\t" + e.v.y.ToString ("E8") + "\t" + e.v.z.ToString ("E8") );
			m_edge.WriteLine (e.a.x.ToString ("E8") + "\t" + e.a.y.ToString ("E8") + "\t" + e.a.z.ToString ("E8") );
			m_edge.WriteLine (e.b.x.ToString ("E8") + "\t" + e.b.y.ToString ("E8") + "\t" + e.b.z.ToString ("E8") );
			m_edge.WriteLine (e.o.x.ToString ("E8") + "\t" + e.o.y.ToString ("E8") + "\t" + e.o.z.ToString ("E8") );
			m_edge.WriteLine (e.n_a.x.ToString ("E8") + "\t" + e.n_a.y.ToString ("E8") + "\t" + e.n_a.z.ToString ("E8") );
			m_edge.WriteLine (e.n_b.x.ToString ("E8") + "\t" + e.n_b.y.ToString ("E8") + "\t" + e.n_b.z.ToString ("E8") );
			m_edge.WriteLine (e.face_a + "\t" + e.face_b + "\t" + e.id );
			m_edge.WriteLine (e.n.ToString("E8") );
			m_edge.WriteLine (prop.emProperties.a.ToString ("E8") + "\t" + prop.emProperties.b.ToString ("E8") + "\t" + prop.emProperties.c.ToString ("E8") + "\t"+prop.emProperties.d.ToString ("E8") );

			m_edge.Flush();
			m_edge.Close();
			return e.id;
		}
		public static void SaveJsonStaticMesh (Transform t, StaticMesh sm, SerializedScenario root, List<SerializedMesh> delayed) {
			SerializedMesh jm=new SerializedMesh();
			MeshFilter meshFilter = sm.GetComponent<MeshFilter> ();
			Debug.Log (t.name);
			jm.name=t.name;
			jm.vertices = meshFilter.sharedMesh.vertices;
			//Vector3ToMarshal[] vertices = new Vector3ToMarshal[v.Length];
			jm.indices = meshFilter.sharedMesh.triangles;
			jm.tm = t.transform.localToWorldMatrix;
			OpalMeshProperties mp= sm.GetComponent<OpalMeshProperties>();
			jm.em=mp.emProperties;
			if (mp.sendWithFaces) {
				jm.faceIds = mp.faceIds;
			} else {
				jm.faceIds=null;
			}
			//OpalEdge[] edges=t.GetComponents<OpalEdge>();
			OpalEdge[] edges=t.GetComponentsInChildren<OpalEdge>();

			if (edges.Length>0) {
				jm.edges= new List<OpalEdgeInfo>();
				for (int i=0; i<edges.Length; i++) {
					jm.edges.Add(edges[i].info);
				}
			} else {
				jm.edges=null;
			}
			if (mp.curved) {
					MeshCurvatureData cd=t.GetComponent<MeshCurvatureData>();
					if (cd==null) {
						jm.pd1=null;
						jm.pd2=null;
					} else {
						//Have to parse because of the infinities
						List<CurvatureDataPoint> pd1 = new List<CurvatureDataPoint>();
						List<Vector4> aux=cd.getPd1(); 
						for (int i=0; i<aux.Count; i++) {
							CurvatureDataPoint a=new CurvatureDataPoint();
							a.x = aux[i].x.ToString("E8");
							a.y = aux[i].y.ToString("E8");
							a.z = aux[i].z.ToString("E8");
							if (aux[i].w==Mathf.Infinity) {
								a.w="infinity";
							} else {
								a.w = aux[i].w.ToString("E8");
							}
							pd1.Add(a);
						}
						aux=cd.getPd2(); 
						List<CurvatureDataPoint> pd2 = new List<CurvatureDataPoint>();
						for (int i=0; i<aux.Count; i++) {
							CurvatureDataPoint a=new CurvatureDataPoint();
							a.x = aux[i].x.ToString("E8");
							a.y = aux[i].y.ToString("E8");
							a.z = aux[i].z.ToString("E8");
							if (aux[i].w==Mathf.Infinity) {
								a.w="infinity";
							} else {
								a.w = aux[i].w.ToString("E8");
							}
							pd2.Add(a);
						}
						jm.pd1=pd1;
						jm.pd2=pd2;
					}

			} else {
						jm.pd1=null;
						jm.pd2=null;
			}
			if (mp.sendWithFaces) {
				root.meshes.Add(jm);
			} else {
				delayed.Add(jm);
			}

		}
		public static void SaveStaticMesh (Transform t, StaticMesh sm, bool saveFaces, string f=null) {
			MeshFilter meshFilter = sm.GetComponent<MeshFilter> ();
			Debug.Log (t.name);
			Vector3[] v = meshFilter.sharedMesh.vertices;
			Vector3ToMarshal[] vertices = new Vector3ToMarshal[v.Length];
			int[] indices = meshFilter.sharedMesh.triangles;
			//if (t.gameObject.name.Equals ("Cube")) {
			//	FileStream m_FileStream = new FileStream ("vert.txt", FileMode.Create, FileAccess.ReadWrite);
			//	StreamWriter m_mesh = new StreamWriter (m_FileStream, System.Text.Encoding.ASCII);
			//	FileStream m_FileStream2 = new FileStream ("tri.txt", FileMode.Create, FileAccess.ReadWrite);
			//	StreamWriter m_tri = new StreamWriter (m_FileStream2, System.Text.Encoding.ASCII);
			//	for (int i = 0; i < indices.Length; i++) {
			//		Debug.Log ("index=" + indices [i]);
			//		m_tri.WriteLine (indices [i]);
			//	}
			Matrix4x4 tm = t.transform.localToWorldMatrix;
			for (int i = 0; i < v.Length; i++) {
				if (Mathf.Abs (v [i].x) < 1E-15f) {
					v [i].x = 0.0f;
				}
				if (Mathf.Abs (v [i].y) < 1E-15f) {
					v [i].y = 0.0f;
				}
				if (Mathf.Abs (v [i].z) < 1E-15f) {
					v [i].z = 0.0f;
				}
				vertices [i] = OpalInterface.ToMarshal (v [i]);
				//vertices [i].x = v [i].x;
				//vertices [i].y = v [i].y;
				//vertices [i].z = v [i].z;
				//Debug.Log (v [i].x.ToString ("E2"));
				//Debug.Log (v [i].y.ToString ("E2"));
				//Debug.Log (v [i].z.ToString ("E2"));
				//Debug.Log (vertices [i].z.ToString ("E2"));
				//		m_mesh.WriteLine (v [i].x.ToString ("E2") + "\t" + v [i].y.ToString ("E2") + "\t" + v [i].z.ToString ("E2"));
				//Debug.Log(v [i].x.ToString ("E2") + "," + v [i].y.ToString ("E2") + "," + v [i].z.ToString ("E2"));
				//Vector4 nv = new Vector4 (v [i].x, v [i].y, v [i].z, 1.0f);
				//Debug.Log (tm *nv);
				//Debug.Log (tm.MultiplyPoint3x4 (v [i]));

			}
			//	m_mesh.Flush ();
			//	m_mesh.Close ();
			//	m_tri.Flush ();
			//	m_tri.Close ();

			//}

			Matrix4x4ToMarshal matrix = new Matrix4x4ToMarshal ();

			//Debug.Log("Matrix of"+t.transform.name+"is "+tm);
			OpalInterface.MarshalMatrix4x4 (ref tm, ref matrix);
			
			string path;
			if (f==null) {
				//path=folder+"/meshes/"+t.transform.name;
				path=folder+"/b"+sm.meshId;
			} else {
				//path=f+"/meshes/"+t.transform.name;
				path=f+"/b"+sm.meshId;
			}
			SaveMeshToFile (path, vertices, indices, matrix, sm.GetComponent<OpalMeshProperties> ().emProperties);
			if (saveFaces) {
				List<OpalTriangleFace> faceIds = sm.GetComponent<OpalMeshProperties>().faceIds;
				if (faceIds!=null) {
					SaveFacesToFile(path,faceIds);
				}
			}

		}
		public static void  SaveFacesToFile (string name, List<OpalTriangleFace> faceIds) {
			FileStream m_FileStream = new FileStream (name + "-fi.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_face = new StreamWriter (m_FileStream, System.Text.Encoding.ASCII);
			foreach (OpalTriangleFace f in faceIds) {
				m_face.WriteLine (f.tris[0] + "\t" +f.tris[1] + "\t"+ f.tris[2] + "\t" + f.id);
			}
			m_face.Flush();
			m_face.Close();
		}
		public static void  SaveMeshToFile (string name, Vector3ToMarshal[] vertices, int[] indices, Matrix4x4ToMarshal tm, MaterialEMProperties em) {

			FileStream m_FileStream = new FileStream (name + "-v.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_mesh = new StreamWriter (m_FileStream, System.Text.Encoding.ASCII);
			FileStream m_FileStream2 = new FileStream (name + "-i.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_tri = new StreamWriter (m_FileStream2, System.Text.Encoding.ASCII);
			FileStream m_FileStream3 = new FileStream (name + "-t.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_tm = new StreamWriter (m_FileStream3, System.Text.Encoding.ASCII);
			FileStream m_FileStream4 = new FileStream (name + "-em.txt", FileMode.Create, FileAccess.ReadWrite);
			StreamWriter m_em = new StreamWriter (m_FileStream4, System.Text.Encoding.ASCII);
			for (int i = 0; i < indices.Length; i++) {
				//Debug.Log ("index=" + indices [i]);
				m_tri.WriteLine (indices [i]);

			}
			for (int i = 0; i < vertices.Length; i++) {
				m_mesh.WriteLine (vertices [i].x.ToString ("E8") + "\t" + vertices [i].y.ToString ("E8") + "\t" + vertices [i].z.ToString ("E8"));
			}
			//translation matrix
			m_tm.WriteLine (tm.m00.ToString ("E8") + "\t" + tm.m01.ToString ("E8") + "\t" + tm.m02.ToString ("E8") + "\t" + tm.m03.ToString ("E8"));
			m_tm.WriteLine (tm.m10.ToString ("E8") + "\t" + tm.m11.ToString ("E8") + "\t" + tm.m12.ToString ("E8") + "\t" + tm.m13.ToString ("E8"));
			m_tm.WriteLine (tm.m20.ToString ("E8") + "\t" + tm.m21.ToString ("E8") + "\t" + tm.m22.ToString ("E8") + "\t" + tm.m23.ToString ("E8"));
			m_tm.WriteLine (tm.m30.ToString ("E8") + "\t" + tm.m31.ToString ("E8") + "\t" + tm.m32.ToString ("E8") + "\t" + tm.m33.ToString ("E8"));

			m_em.WriteLine (em.a);
			m_em.WriteLine (em.b);
			m_em.WriteLine (em.c);
			m_em.WriteLine (em.d);

			m_mesh.Flush ();
			m_mesh.Close ();
			m_tri.Flush ();
			m_tri.Close ();
			m_tm.Flush ();
			m_tm.Close ();
			m_em.Flush ();
			m_em.Close ();
		}
	}
}
