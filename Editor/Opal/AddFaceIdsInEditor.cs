using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Opal
{

	public class AddFaceIdsInEditor: ScriptableWizard
	{
		public uint startFaceId=0;
		public bool singleFace=false;
		public GameObject target;
		[MenuItem("Opal/Add face ids to mesh ...")]
			static void CreateWizard()
			{
				//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
				//If you don't want to use the secondary button simply leave it out:
				//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");

				ScriptableWizard.DisplayWizard<AddFaceIdsInEditor>("Add face ids to mesh", "Add");

			}
		void OnWizardCreate()
		{
			Add();
		}
		void OnWizardUpdate()
		{
			helpString = "Add face ids to mesh";
		}
		void Add() {
			UnityEngine.Mesh srcMesh=GetMyComponentMesh(target);
			OpalMeshProperties mp = target.GetComponent<Opal.OpalMeshProperties>();
			Debug.Log("triangle="+srcMesh.triangles.Length+"vertices="+srcMesh.vertices.Length);
			if (singleFace) {
			//Just fill all triangles with the same faceId
				List<OpalTriangleFace> fl = new List<OpalTriangleFace>();
				int[] t = srcMesh.triangles;
				for (int i=0; i< t.Length; i=i+3) {
					//Debug.Log("i="+i);	
					OpalTriangleFace tf = new OpalTriangleFace();
					tf.tris=new int[3];
					tf.tris[0]= t[i];
					tf.tris[1]= t[i+1];
					tf.tris[2]= t[i+2];
					tf.id=startFaceId;
					fl.Add(tf);
				}

				mp.SetFaceIds(fl);
				startFaceId++; 
			} else {
				EqualFace eqF=new EqualFace();
				Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
				List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(srcMesh,ref startFaceId, faceMap);
				mp.SetFaceIds(tf); 
			}

		}
		protected virtual UnityEngine.Mesh GetMyComponentMesh (GameObject go)
		{
#if UNITY_EDITOR
			return go.GetComponent<MeshFilter> ().sharedMesh;
#else
			return go.GetComponent<MeshFilter> ().mesh;
#endif
		}
	}
}
