﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Opal
{
	
	public class ShowRaysInEditor : ScriptableWizard
	{
		public enum Mode
		{
			All,
			Ray,
			Range}

		;
		public Color c=Color.red;

		public Mode mode;
		//public string pFile = "\\\\192.168.1.33\\eegea\\optix6.5\\SDK\\bin\\h.txt";
		public string pFile="D:\\Users\\eegea\\MyDocs\\investigacion\\vanet\\opal\\anvers\\trace.txt";
		public string init = "";
		public string end = "";
		public GameObject rayPrefab;
		public string ray;
		//public Vector3 transmitterPosition;
		public GameObject transmitter;
		public bool onlyLastSegment=false;

		GameObject root;

		[MenuItem("Opal/Load rays on editor")]
		static void CreateWizard()
		{
			//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
			//If you don't want to use the secondary button simply leave it out:
			//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");

			ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load");
		
		}

		void OnWizardCreate()
		{
			Debug.Log ("Start ShowRayPaths");
			rayPrefab = AssetDatabase.LoadAssetAtPath<GameObject> ("Assets/Resources/Prefabs/Communications/RayRenderer.prefab");

			if (mode == Mode.Ray) {
				FindRay ();
			} else if (mode == Mode.Range) {
				ShowRange ();
			} else {
				ShowAll ();
			}

			
		}

		void OnWizardUpdate()
		{
			helpString = "Load rays in editor";
		}
		public void ShowRay (string line)
		{
			List<Vector3> positions = new List<Vector3> ();
			string[] rayDir = line.Split (':');
			char[] separator = new char[]{ '\t' };
			System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo ("en-US");
			//GameObject go=new GameObject("ray");
			GameObject go = Instantiate (rayPrefab);
			LineRenderer lr = go.GetComponent<LineRenderer> ();
			lr.startColor = c;
			lr.endColor = c;


			float distance = 0f;
			string[] hitpoints = rayDir [1].Split ('|');
			if (onlyLastSegment) {
				string[] tokens = hitpoints [hitpoints.Length-2].Split (separator);
				//Debug.Log ("tokens=" + tokens.Length);

				Vector3 p = new Vector3 (float.Parse (tokens [0], ci), float.Parse (tokens [1], ci), float.Parse (tokens [2], ci));
				positions.Add (p);
				 tokens = hitpoints [hitpoints.Length-1].Split (separator);
				p = new Vector3 (float.Parse (tokens [0], ci), float.Parse (tokens [1], ci), float.Parse (tokens [2], ci));
				positions.Add (p);
			} else {
				positions.Add (transmitter.transform.position);
				for (int j = 0; j < hitpoints.Length; j++) {
					string[] tokens = hitpoints [j].Split (separator);
					//Debug.Log ("tokens=" + tokens.Length);
					//Debug.Log (tokens [0] + "," + tokens [1] + "," + tokens [2]);

					Vector3 p = new Vector3 (float.Parse (tokens [0], ci), float.Parse (tokens [1], ci), float.Parse (tokens [2], ci));
					//Debug.Log ("p=" + p);
					distance += Vector3.Distance (p, positions [j]);
					positions.Add (p);
				}

			}
			if (positions.Count > 0) { 

				lr.positionCount = positions.Count;
				lr.SetPositions (positions.ToArray ());
				lr.enabled = true;
				go.name = "ray " + rayDir [0];
				RayInfo ri=go.AddComponent<RayInfo> ();
				ri.unfoldedPathLength = distance;


			}

			go.transform.parent = root.transform;



		}

		public void FindRay ()
		{
			string line;
			//Debug.DrawRay (new Vector3 (10f, 3.7164f, 85.8302f), new Vector3(-0.5755f,0.2923f,0.7637f)*30f, Color.blue,30f);
			using (System.IO.StreamReader file = new System.IO.StreamReader (pFile, System.Text.Encoding.ASCII)) {
				while ((line = file.ReadLine ()) != null) {  

					//Debug.Log (line);
					string[] rayDir = line.Split (':');
					if (rayDir [0].Equals (ray)) {
						ShowRay (line);


						break;

					}  

				}
			}
		}

		public void ShowRange ()
		{
			Debug.Log ("Showing range from " + init);
			/*if (string.IsNullOrEmpty (ray)) {
				Debug.Log ("No ray set");
				return;
			}*/


			string line;
			int i = 0;
			bool found = false;

			using (System.IO.StreamReader file = new System.IO.StreamReader (pFile, System.Text.Encoding.ASCII)) {

				while ((line = file.ReadLine ()) != null) {
					string[] rayDir = line.Split (':');
					if (rayDir [0].Equals (init)) {
						found = true;
					} 
					if (found) {
						i++;
						ShowRay (line);
					}

					if (rayDir [0].Equals (end)) {
						break;
					}

				}


			}

			Debug.Log (i + " line renderers created");
		}

		public void ShowAll ()
		{
			string line;
			int i = 0;
			bool found = false;

			root = new GameObject ("rays");
			using (System.IO.StreamReader file = new System.IO.StreamReader (pFile, System.Text.Encoding.ASCII)) {

				while ((line = file.ReadLine ()) != null) {
					Debug.Log ("line=" + line);
					i++;
					ShowRay (line);


				}


			}
			Debug.Log (i + " line renderers created");

		}
		// When the user presses the "Apply" button OnWizardOtherButton is called.
		/*void OnWizardOtherButton()
		{
			if (Selection.activeTransform != null)
			{
				Light lt = Selection.activeTransform.GetComponent<Light>();

				if (lt != null)
				{
					lt.color = Color.red;
				}
			}
		}
		*/
	}
}
