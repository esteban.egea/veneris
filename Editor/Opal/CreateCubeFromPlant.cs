using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Veneris;
namespace Opal
{

	public class CreateCubeFromPlant : ScriptableWizard
	{

		public float length=12f;
		public float width=2f;
		public float height=4f;
		public uint globalEdgeId=0;
		public uint globalFaceId=0;
		public uint staticMeshId=0;
		public string name="cube";
		public bool applyTransform=false;
		
		public Transform targetTransform;
		[MenuItem("Opal/Create Custom Mesh/Create cube and edges...")]
			static void CreateWizard()
			{
				//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
				//If you don't want to use the secondary button simply leave it out:
				//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");

				ScriptableWizard.DisplayWizard<CreateCubeFromPlant>("Create cube and edges", "Create");

			}

		void OnWizardCreate()
		{
			Create();

		}

		void OnWizardUpdate()
		{
			helpString = "Create cube from plant";
		}
		public GameObject Create ()
		{

			Vector3[] corners = new Vector3[4];
			corners[0] = new Vector3(-width, 0.0f, -length)*0.5f;	
			corners[1] = new Vector3(width, 0.0f, -length)*0.5f;	
			corners[2] = new Vector3(width, 0.0f, length)*0.5f;	
			corners[3] = new Vector3(-width, 0.0f, length)*0.5f;
			GameObject go = SumoUtils.CreateTriangulation ("cube", "0", corners);
			Opal.OpalMeshProperties opalmp = go.AddComponent<Opal.OpalMeshProperties> ();
			Opal.StaticMesh sm = go.AddComponent<Opal.StaticMesh> ();
			sm.opalMeshProperties = opalmp;
			sm.meshId=staticMeshId;
			opalmp.emProperties.a = 6f;
			List<Matrix4x4> sections = new List<Matrix4x4> ();
			sections.Add (Matrix4x4.TRS (new Vector3 (0f, height, 0f), Quaternion.identity, Vector3.one));
			sections.Add (Matrix4x4.TRS (Vector3.zero, Quaternion.identity, Vector3.one));

			UnityEngine.Mesh m = new UnityEngine.Mesh ();
			MeshFilter mf= go.GetComponent<MeshFilter>();
			sm.meshFilter=mf;
			UnityEngine.Mesh srcMesh = mf.sharedMesh;
			MeshExtrusion.Edge[] edges = MeshExtrusion.ExtractEdges(srcMesh);
			edges=SortEdges(edges);
			List<OpalTriangleFace> tf=new List<OpalTriangleFace>();
			MeshExtrusion.ExtrudeVerticalMesh (srcMesh, m, sections.ToArray (), edges, false, tf,  globalFaceId);
			CreateOpalEdges(go, srcMesh, m,  edges, height, tf);
			//Update globalFaceId	
			globalFaceId += (uint) edges.Length +2;
			Debug.Log("globalFaceId="+globalFaceId);
			if (applyTransform) {
				if (targetTransform!=null) {
					OpalMeshUtil.TransformStaticMesh(targetTransform, m);
				}
			}
			mf.sharedMesh = m;
			AssetDatabase.CreateAsset (m, "Assets/Resources/"+name+".asset");
			return go;
		}
		protected  MeshExtrusion.Edge[] SortEdges(MeshExtrusion.Edge[] edges) {
			List<MeshExtrusion.Edge> sorted= new List<MeshExtrusion.Edge>();
			MeshExtrusion.Edge last = new MeshExtrusion.Edge();
			bool reverse=false;
			for (int i=0; i<edges.Length; i++) {
				if (edges[i].vertexIndex[0]==edges.Length-1 &&  edges[i].vertexIndex[1]==0){ 
					//This is the last one
					last=edges[i];
					reverse=true; 
				} else if((edges[i].vertexIndex[1]==edges.Length-1 &&  edges[i].vertexIndex[0]==0) ) {
					last=edges[i]; 

				} else {
					sorted.Add(edges[i]);
				}

			}
			if (reverse) {
				sorted.Sort(new ReverseEdgeSorter());
			} else {
				sorted.Sort(new EdgeSorter());
			}
			sorted.Add(last);
			for (int i=0; i<sorted.Count; i++) {				
				sorted[i].faceIndex[0]=i;
				sorted[i].faceIndex[1]=((i+1) % sorted.Count);
			}

			return sorted.ToArray();
		}
		protected virtual List<OpalEdge> CreateOpalEdges(GameObject parent, UnityEngine.Mesh srcMesh, UnityEngine.Mesh extrudedMesh, MeshExtrusion.Edge[] edges, float height, List<OpalTriangleFace> tf) {
			//EqualFace eqF= new EqualFace();
			//eqF.epsilon=1e-4f;
			//EqualFaceDifferenceVector eqF=new EqualFaceDifferenceVector();
                        //Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
			//uint cid=0;
			//Extract faces
			//List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(extrudedMesh,ref globalFaceId, faceMap);
			List<OpalEdge> opalEdges = new List<OpalEdge>();
			OpalMeshProperties mp = parent.GetComponent<Opal.OpalMeshProperties>();
			mp.SetFaceIds(tf); 
			Vector3[] vertices = srcMesh.vertices;
			//uint eid=0;
			//for (int i=1; i<2; i++)
			for (int i=0; i<edges.Length; i++)
			{	
				GameObject go = new GameObject("OpalEdge "+opalEdges.Count);
				//First vertical edges at outline vertices
				MeshExtrusion.Edge e=edges[i];
				//Debug.Log("e="+e.faceIndex[0]+","+e.faceIndex[1]);
				OpalEdge oe=go.AddComponent<OpalEdge>();
				oe.opalMeshProperties=mp;
				//edge
				Vector3 v=Vector3.up*height;
				Vector3 o=vertices[e.vertexIndex[0]];
				//face a
				Vector3 a=vertices[e.vertexIndex[1]]-vertices[e.vertexIndex[0]];
				uint aid=globalFaceId+(uint) e.faceIndex[0];
				int j=i-1;
				if (i==0) {
					j=edges.Length -1;
				}
				Vector3 na= Vector3.Cross(a,v).normalized;
				//face b
				MeshExtrusion.Edge prev=edges[j];
				Vector3 b = vertices[prev.vertexIndex[0]]-vertices[prev.vertexIndex[1]];
				Vector3 nb= Vector3.Cross(v,b).normalized;
				uint bid=globalFaceId + (uint) prev.faceIndex[0];
				//Check conventions
				float an=Vector3.SignedAngle(a,b,v);
				//Internal angle has to be positive from face A to face B. Reversing the edge orientation makes it
				bool revertedEdge=false;
				if (an<0) {
					o=o+v;
					v=-v;
				 	an=Vector3.SignedAngle(a,b,v);
					revertedEdge=true;
					//Debug.Log("i="+i+"revert edge orientation");
				}
				//Internal angle has to be less than 180 degrees. Since SignedAngle does not return angles > 180, the previous condition ensure this. Check anyway
				if (an>180.0f) {
					throw new System.InvalidOperationException ("CreateOpalEdges: internal angle (from face A to B) is greater than 180 degrees  " + an);
				}
				float anb=Vector3.SignedAngle(a,b,Vector3.Cross(na, a));
				//Normals has to point out according to the internal angle
				if (anb <0) {
					//Reverse normal
					na=-na;
				 	anb=Vector3.SignedAngle(a,b,Vector3.Cross(na, a));
					//Debug.Log("i="+i+"revert A normal");
					if (anb <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: A normal does not point out even after reversing  " + anb);
					}
				}
				float bna=Vector3.SignedAngle(b,a,Vector3.Cross(nb, b));
				//Normals has to point out according to the internal angle
				if (bna <0) {
					//Reverse normal
					nb=-nb;
					bna=Vector3.SignedAngle(b,a,Vector3.Cross(nb, b));
					//	Debug.Log("i="+i+"revert B normal");
					if (bna <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: B normal does not point out even after reversing  " + bna);
					}
				}
				//Debug.Log("o="+o+"an="+an);
				//Debug.DrawRay(o,v,Color.blue, 100);
				//Debug.DrawRay(o,a,Color.green, 100);
				//Debug.DrawRay(o,b,Color.cyan, 100);
				//Debug.DrawRay(o,na,Color.red, 100);
				//Debug.DrawRay(o,nb,Color.red, 100);
				//Assing face_id
				//uint aid;
				//if (faceMap.TryGetValue(na,out aid)) {
				//} else {
				//	//Normal may have been reversed for edge creation. Try again
				//	if (faceMap.TryGetValue(-na,out aid)) {
				//	} else {
				//		Debug.DrawRay(o,na*10,Color.blue, 50);
				//		DebugFaceNormals(faceMap,na);
				//		throw new System.InvalidOperationException ("CreateOpalEdges: face id not found for face A with normal " + na );
				//	}					

				//}
				//	
				//uint bid;
				//if (faceMap.TryGetValue(nb,out bid)) {
				//} else {
				//	//Normal may have been reversed for edge creation. Try again
				//	if (faceMap.TryGetValue(-nb,out bid)) {
				//	} else {
				//		Debug.DrawRay(o,nb*10,Color.grey, 50);
				//		DebugFaceNormals(faceMap, nb);
				//		throw new System.InvalidOperationException ("CreateOpalEdges: face id not found for face B with normal " + nb.ToString("E6")  );
				//	}

				//}
				oe.SetInfoValues(o,v,aid,bid,a,b,na,nb,globalEdgeId);	
				opalEdges.Add(oe);
				globalEdgeId++;
				if (applyTransform) {
					if (targetTransform!=null) {
						OpalMeshUtil.TransformOpalEdge(targetTransform, oe);
					}
				}

				//Now compute corresponding horizontal edge on top
				
				go = new GameObject("OpalEdge "+opalEdges.Count);	
				OpalEdge he=go.AddComponent<OpalEdge>();
				he.opalMeshProperties=mp;
				//edge
				o.y=height;
				Vector3 vh=b;
				Vector3 ah=-v;
				if (revertedEdge) {
					ah=v;
				}
				uint aidh=bid;
				uint bidh= globalFaceId +(uint) edges.Length;
				Vector3 bh=Vector3.Cross(ah,vh).normalized;
				na=nb;
				nb=Vector3.Cross(bh,vh).normalized;
				//Debug.DrawRay(o,vh,Color.blue, 100);
				//Debug.DrawRay(o,ah,Color.green, 100);
				//Debug.DrawRay(o,bh,Color.cyan, 100);
				//Debug.DrawRay(o,na,Color.red, 100);
				//Debug.DrawRay(o,nb,Color.red, 100);
				//Assing face_id
				//Check conventions
				an=Vector3.SignedAngle(ah,bh,vh);
				//Internal angle has to be positive from face A to face B. Reversing the edge orientation makes it
				if (an<0) {
					o=o+vh;
					vh=-vh;
				 	an=Vector3.SignedAngle(ah,bh,vh);
					//Debug.Log("i="+i+"hor revert edge orientation");
				}
				//Internal angle has to be less than 180 degrees. Since SignedAngle does not return angles > 180, the previous condition ensure this. Check anyway
				if (an>180.0f) {
					throw new System.InvalidOperationException ("CreateOpalEdges: horizontal internal angle (from face A to B) is greater than 180 degrees  " + an);
				}
				anb=Vector3.SignedAngle(ah,bh,Vector3.Cross(na, ah));
				//Normals has to point out according to the internal angle
				if (anb <0) {
					//Reverse normal
					na=-na;
				 	anb=Vector3.SignedAngle(ah,bh,Vector3.Cross(na, ah));
					//Debug.Log("i="+i+" hor revert A normal");
					if (anb <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: horizontal A normal does not point out even after reversing  " + anb);
					}
				}
				bna=Vector3.SignedAngle(bh,ah,Vector3.Cross(nb, bh));
				//Normals has to point out according to the internal angle
				if (bna <0) {
					//Reverse normal
					nb=-nb;
					bna=Vector3.SignedAngle(bh,ah,Vector3.Cross(nb, bh));
					//Debug.Log("i="+i+" hor revert B normal");
					if (bna <0) {
						throw new System.InvalidOperationException ("CreateOpalEdges: horizontal B normal does not point out even after reversing  " + bna);
					}
				}
				//Debug.Log("an="+an);
				//uint aidh;
				//if (faceMap.TryGetValue(na,out aidh)) {
				//} else {
				//	//Normal may have been reversed for edge creation. Try again
				//	if (faceMap.TryGetValue(-na,out aid)) {
				//	} else {
				//		Debug.DrawRay(o,na*10,Color.blue, 50);
				//		DebugFaceNormals(faceMap,na);
				//		throw new System.InvalidOperationException ("CreateOpalEdges: face id not found for face A with normal " + na );
				//	}					
				//
				//}
				//	
				//uint bidh;
				//if (faceMap.TryGetValue(nb,out bidh)) {
				//} else {
				//	//Normal may have been reversed for edge creation. Try again
				//	if (faceMap.TryGetValue(-nb,out bid)) {
				//	} else {
				//		Debug.DrawRay(o,nb*10,Color.blue, 50);
				//		DebugFaceNormals(faceMap,nb);
				//		throw new System.InvalidOperationException ("CreateOpalEdges: face id not found for face B with normal " + nb );
				//	}
				//
				//}
				he.SetInfoValues(o,vh,aidh,bidh,ah,bh,na,nb,globalEdgeId);	
				opalEdges.Add(he);
				globalEdgeId++;
				if (applyTransform) {
					if (targetTransform!=null) {
						OpalMeshUtil.TransformOpalEdge(targetTransform, he);
					}
				}
			}	
			return opalEdges;
		}
	}
}


