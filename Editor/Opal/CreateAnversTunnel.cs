﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

namespace Opal
{

	public class CreateAnversTunnel : ScriptableWizard
	{
		public Material mat;
		public float resolution = 100f;
		public float totalLength = 0f;

		float endAngle = 4.581f;
		//In degrees
		Vector3 widthDirection = Vector3.right;
		Vector3 lengthDirection = Vector3.zero;
		List<Vector3> points;
		List<bool> curvedTriangle;
		//public List<Vector3> vertList;
		public int curvedSteps = 100;
		public float radiusC = 2900f;
		public float height = 6f;
		public float width = 10.2f;
		Vector3 lastSegment = Vector3.zero;
		public bool straight=false;

		[MenuItem ("Opal/Create Custom Mesh/Create Anvers on editor")]
		static void CreateWizard ()
		{
			//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
			//If you don't want to use the secondary button simply leave it out:
			//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");
			ScriptableWizard.DisplayWizard<CreateAnversTunnel> ("Create Anvers Tunnel editor", "Create");
		}

		void OnWizardCreate ()
		{
			uint meshId=0;
			lengthDirection = new Vector3 (0.0f, -1.0f * Mathf.Sin (Mathf.Deg2Rad * 2.2906f), Mathf.Cos (Mathf.Deg2Rad * 2.2906f));
			lengthDirection = lengthDirection.normalized;
			//Top
			GameObject go = new GameObject ("top");
			MeshRenderer mr = go.AddComponent<MeshRenderer> ();
			mr.sharedMaterial = mat;
			MeshFilter mf = go.AddComponent<MeshFilter> ();
			//mf.mesh = CreateMesh ();
			//mf.mesh = ExtrudeMesh(go);
			CreatePointsAnvers (Vector3.zero, radiusC);
			OpalMeshProperties mp = go.AddComponent<OpalMeshProperties>();
			mp.emProperties.a=5;
			mp.emProperties.c=5;
			mp.curved=true;
			mp.sendWithFaces = false;
			StaticMesh sm=go.AddComponent<StaticMesh>();
			sm.meshId=meshId;
			meshId++;
			sm.opalMeshProperties=mp;
			sm.meshFilter =mf;


			mf.mesh = CreateMinimalMesh (go, widthDirection * width, true, radiusC);

			go = new GameObject ("left");
			mr = go.AddComponent<MeshRenderer> ();
			mr.sharedMaterial = mat;
			mf = go.AddComponent<MeshFilter> ();
			lastSegment = Vector3.zero;

			//Reuse the points
			mf.mesh = CreateMinimalMesh (go, Vector3.down * height, false, 0f);
			mp = go.AddComponent<OpalMeshProperties>();
			mp.emProperties.a=5;
			mp.emProperties.c=5;
			mp.curved=false;
			mp.sendWithFaces = false;
			sm=go.AddComponent<StaticMesh>();
			sm.meshId=meshId;
			meshId++;
			sm.opalMeshProperties=mp;
			sm.meshFilter =mf;

			go = new GameObject ("right");
			mr = go.AddComponent<MeshRenderer> ();
			mr.sharedMaterial = mat;
			mf = go.AddComponent<MeshFilter> ();


			CreatePointsAnvers (new Vector3 (width, 0.0f, 0.0f), radiusC);
			mf.mesh = CreateMinimalMesh (go, Vector3.down * height, false, 0f);
			mp = go.AddComponent<OpalMeshProperties>();
			mp.emProperties.a=5;
			mp.emProperties.c=5;
			mp.curved=false;
			mp.sendWithFaces = false;
			sm=go.AddComponent<StaticMesh>();
			sm.meshId=meshId;
			meshId++;
			sm.opalMeshProperties=mp;
			sm.meshFilter =mf;

			//Bottom
			go = new GameObject ("bottom");
			mr = go.AddComponent<MeshRenderer> ();
			mr.sharedMaterial = mat;
			mf = go.AddComponent<MeshFilter> ();

			CreatePointsAnvers (new Vector3 (0.0f, -height, 0.0f), radiusC + height);
			mf.mesh = CreateMinimalMesh (go, widthDirection * width, true, -1.0f * (radiusC + height));
			mp = go.AddComponent<OpalMeshProperties>();
			mp.emProperties.a=5;
			mp.emProperties.c=5;
			mp.curved=true;
			mp.sendWithFaces = false;
			sm=go.AddComponent<StaticMesh>();
			sm.meshId=meshId;
			meshId++;
			sm.opalMeshProperties=mp;
			sm.meshFilter =mf;

		}

		public void CreatePointsAnvers (Vector3 start, float radius)
		{

			points = new List<Vector3> ();
			curvedTriangle  = new List<bool>();
			lastSegment = start;
			points.Add (start);
			totalLength = 0f;
			int section = 0;
			if (straight) {
				while (section < 3) {
					Vector3 n;
					if (section == 0) {
						Vector3 down = lengthDirection;
						n = lastSegment + down * 568;
						totalLength += 568;
						points.Add (n);
						lastSegment = n;
						//Advance section
						section = 1;
						curvedTriangle.Add (false);
					} else if (section == 1) {
						Vector3 up = lengthDirection;
						up.y = -lengthDirection.y;
						n = lastSegment + up * 568;
						totalLength += 568;
						points.Add (n);
						lastSegment = n;
						curvedTriangle.Add (false);
						//Advance section
						section = 3;
					}
				}
			} else {
				while (section < 3) {
					Vector3 n;
					if (section == 0) {
						Vector3 down = lengthDirection;
						n = lastSegment + down * 426;
						totalLength += 426;
						points.Add (n);
						lastSegment = n;
						//Advance section
						section = 1;
						curvedTriangle.Add (false);
					}
					if (section == 1) {
						//Curved	
						Vector3 o = lastSegment + Vector3.Cross (lengthDirection, widthDirection) * radius;
						Debug.DrawRay (lastSegment, o - lastSegment, Color.blue);
						Debug.Log ("o=" + o);
						GameObject no = new GameObject ("Center of curvature");
						no.transform.position = o;
						no.transform.LookAt (lastSegment);
						float step = Mathf.Deg2Rad * endAngle / curvedSteps;
						n = o;
						for (int j = 1; j < curvedSteps + 1; j++) {
							float t = step * j;
							Vector3 p = new Vector3 (0.0f, -radius * Mathf.Sin (t), radius * Mathf.Cos (t));
							n = no.transform.TransformPoint (p);
							//Debug.Log ("s=" + (n - lastSegment).magnitude);
							totalLength += (n - lastSegment).magnitude;
							points.Add (n);
							curvedTriangle.Add (true);
							lastSegment = n;


						}
						section = 2;

					}
					if (section == 2) {
						Vector3 up = lengthDirection;
						up.y = -lengthDirection.y;
						n = lastSegment + up * 426;
						totalLength += 426;
						points.Add (n);
						lastSegment = n;
						curvedTriangle.Add (false);
						//Advance section
						section = 3;
					}


				}
			}
			Debug.Log ("totalLength=" + totalLength);
		}

		//To avoid the repetition of vertices and triangles
		public Mesh CreateMinimalMesh (GameObject go, Vector3 direction, bool curved, float radius)
		{
			Mesh mesh = new Mesh ();
			//Add curvature information
			MeshCurvatureData md = null;
			List<Vector4> pd1 = null;
			List<Vector4> pd2 = null;
			if (curved) {
				md = go.AddComponent<MeshCurvatureData> ();
				pd1 = new List<Vector4> ();
				pd2 = new List<Vector4> ();
			}
			List<Vector3> centers = new List<Vector3> ();
			//Curvatures
			Vector4 u1;
			Vector4 u2;
			List<Vector3> vertList = new List<Vector3> ();
			//vertList = new List<Vector3> ();
			List<int> triList = new List<int> ();
			List<Vector2> uvList = new List<Vector2> ();
			Vector3 segmentStart = points [0];
			Vector3 p0 = segmentStart;
			Vector3 p1 = segmentStart + direction;
			vertList.Add (p0); //0
			vertList.Add (p1); //1

			int triIdx = 0;
			Debug.Log ("points=" + points.Count);
			Debug.Log ("curved=" + curvedTriangle.Count);
			for (int i = 1; i < points.Count; i++) {


				Vector3 segmentEnd = points [i];

				Vector2[] uvs = new Vector2[] {
					new Vector2 (0, 0), 
					new Vector2 (0, 1), 
					new Vector2 (1, 1),
					new Vector2 (1, 0)
				};

				Vector3 p2 = segmentEnd;
				Vector3 p3 = segmentEnd + direction;

				vertList.Add (p2); //2
				vertList.Add (p3); //3
				//First triangle
				triList.Add (triIdx);
				triList.Add (triIdx + 3);
				triList.Add (triIdx + 1);

				if (curved ) {
					
					//Curvature in width direction is infinity
					u1 = widthDirection.normalized;
					u1.w = Mathf.Infinity;
					if (curvedTriangle [i-1]) {
						//Compute principal direction from normals
						u2 = ComputePrincipalDirection (vertList [triIdx], vertList [triIdx + 3], vertList [triIdx + 1], radius);


					} else {
						u2 = lengthDirection.normalized;
						u2.w = Mathf.Infinity;
					
					}
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid (vertList [triIdx], vertList [triIdx + 3], vertList [triIdx + 1]));
				}
				//Second triangle
				triList.Add (triIdx);
				triList.Add (triIdx + 2);
				triList.Add (triIdx + 3);

				if (curved) {
					//Curvature in width direction is infinity
					u1 = widthDirection.normalized;
					u1.w = Mathf.Infinity;
					if (curvedTriangle [i-1]) {
						//Compute principal direction from normals
						u2 = ComputePrincipalDirection (vertList [triIdx], vertList [triIdx + 2], vertList [triIdx + 3], radius);

					} else {
						u2 = lengthDirection.normalized;
						u2.w = Mathf.Infinity;
					}
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid (vertList [triIdx], vertList [triIdx + 2], vertList [triIdx + 3]));
				}



				segmentStart = segmentEnd;
				triIdx += 2;


			}
			Debug.Log ("End position=" + vertList [vertList.Count - 2]);
			if (curved) {
				md.setPd1 (pd1);
				md.setPd2 (pd2);
				md.centers = centers;
			}
			Debug.Log (vertList.Count);
			mesh.vertices = vertList.ToArray ();
			mesh.triangles = triList.ToArray ();
			mesh.uv = uvList.ToArray ();
			mesh.RecalculateNormals ();
			mesh.RecalculateBounds ();
			return mesh;
		}

		public Vector3 GetCentroid (Vector3 p0, Vector3 p1, Vector3 p2)
		{
			return ((p0 + p1 + p2) / 3);
		}

		public Vector4 ComputePrincipalDirection (Vector3 p0, Vector3 p1, Vector3 p2, float curvature)
		{
			//Compute principal direction from normal
			Vector3 e0 = p1 - p0; //p1 -p0
			Vector3 e1 = p0 - p2; //p0-p2
			Vector3 n = Vector3.Cross (e1, e0).normalized;

			Vector3 pdd = Vector3.Cross (n, widthDirection.normalized).normalized;
			//Debug.Log ("dot=" + Vector3.Dot (pdd, n));
			Vector4 u = pdd;

			u.w = curvature; 
			return u;
		}

	}

}
