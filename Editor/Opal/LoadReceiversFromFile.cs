﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LoadReceiversFromFile : MonoBehaviour {


	[MenuItem ("Opal/Load receivers from file ...")]
	public static void LoadSimpleFromFile ()
	{
		string pathToVertices = EditorUtility.OpenFilePanel ("Select receivers", "", "");
		List<Vector3> vertices = new List<Vector3> ();
		string line;
		using (System.IO.StreamReader file = new System.IO.StreamReader (pathToVertices, System.Text.Encoding.ASCII)) {
			char[] separator = new char[]{ '\t' };
			System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo ("en-US");
			while ((line = file.ReadLine ()) != null) {
				string[] tokens = line.Split (separator);
				Vector3 p = new Vector3 (float.Parse (tokens [3], ci), float.Parse (tokens [4], ci), float.Parse (tokens [5], ci));
				Debug.Log (p);
				vertices.Add (p);

			}
		}
		GameObject root = new GameObject ("receivers");
		foreach (Vector3 v in vertices) {
			GameObject s = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			s.transform.position = v;
			s.transform.parent = root.transform;
		}
	}
}
