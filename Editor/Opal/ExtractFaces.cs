/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
namespace Opal
{

	public class ExtractFaces 
	{
		[MenuItem ("Opal/ExtractFaces")]
			static void ExtractFacesFromMeshes  () {
				Opal.StaticMesh[] staticMeshes = GameObject.FindObjectsOfType<Opal.StaticMesh> (); 
				uint cid=0;
				for (int i = 0; i < staticMeshes.Length; i++) 
				{
					staticMeshes[i].FillMembers();	
					OpalMeshProperties mp=staticMeshes[i].GetOpalMeshProperties();
					Mesh m = staticMeshes[i].meshFilter.sharedMesh;	
					EqualFace eqF= new EqualFace();	
					Dictionary<Vector3,uint> faceMap = new Dictionary<Vector3,uint>(eqF);
					List<OpalTriangleFace> tf=OpalMeshUtil.ComputeFaceIds(m,ref cid, faceMap);
					mp.SetFaceIds(tf);
				}	
			}	
		[MenuItem ("Opal/AddCubeEdges")]
			static void AddEdgesToCubes  () {
				uint lastIndex=0;
				uint eid=0;
				GameObject o=GameObject.Find("Cube-NW");
				OpalMeshUtil.AddCubeEdges(new Vector3(-50.0f,0.0f,60.0f),40.0f,lastIndex, o, ref eid);
				lastIndex +=6;
				o=GameObject.Find("Cube-SW");
				OpalMeshUtil.AddCubeEdges(new Vector3(-50.0f,0.0f,0.0f),40.0f,lastIndex, o, ref eid);
				lastIndex +=6;
				o=GameObject.Find("Cube-SE");
				OpalMeshUtil.AddCubeEdges(new Vector3(10.0f,0.0f,0.0f),40.0f,lastIndex, o, ref eid);
				lastIndex +=6;
				o=GameObject.Find("Cube-NE");
				OpalMeshUtil.AddCubeEdges(new Vector3(10.0f,0.0f,60.0f),40.0f,lastIndex, o, ref eid);
			}
		[MenuItem ("Opal/RemoveEdges")]
			static void RemoveEdges  () {
			//Get all edges in the opal layer
			OpalEdge[] edges = GameObject.FindObjectsOfType<OpalEdge> (); 
			int m = 0;
			for (int i = 0; i < edges.Length; i++) {

				Object.DestroyImmediate(edges[i]);
			}
		
		}
	}
}

