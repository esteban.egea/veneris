﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

namespace Opal
{

	public class ShowImagesInEditor : ScriptableWizard
	{
		//public string pFile = "\\\\192.168.1.33\\eegea\\optix6.5\\SDK\\bin\\im.txt";
		public string pFile="D:\\Users\\eegea\\MyDocs\\investigacion\\vanet\\opal\\anvers\\im.txt";
		public Color c = Color.green;
		public GameObject receiver;
		GameObject root;
		public GameObject rayPrefab;
		[MenuItem("Opal/Load images on editor")]
		static void CreateWizard()
		{
			//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
			//If you don't want to use the secondary button simply leave it out:
			//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");
			ScriptableWizard.DisplayWizard<ShowImagesInEditor>("Load images on editor", "Load");
		}

		void OnWizardCreate()
		{
			ShowAll ();
		}

		void OnWizardUpdate()
		{
			helpString = "Please set the color of the light!";
		}
		public void ShowImage (string line, int i)
		{
			List<Vector3> positions = new List<Vector3> ();
			char[] separator = new char[]{ '\t' };
			string[] tokens = line.Split (separator);
			System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo ("en-US");
			Vector3 p = new Vector3 (float.Parse (tokens [0], ci), float.Parse (tokens [1], ci), 0f);
			//Debug.Log ("p=" + p);

			//GameObject go=new GameObject("ray");
			GameObject go = Instantiate (rayPrefab);
			LineRenderer lr = go.GetComponent<LineRenderer> ();
			lr.startColor = c;
			lr.endColor = c;
			lr.startWidth = 0.02f;

			positions.Add (receiver.transform.position);
			positions.Add (p);


			if (positions.Count > 0) { 

				lr.positionCount = positions.Count;
				lr.SetPositions (positions.ToArray ());
				lr.enabled = true;
				go.name = "image " + i;


			}
			RayInfo ri=go.AddComponent<RayInfo> ();
			ri.unfoldedPathLength = float.Parse (tokens [2]);
			go.transform.parent = root.transform;



		}

		public void ShowAll ()
		{
			string line;
			int i = 0;
			bool found = false;
			root = new GameObject ("images");
			using (System.IO.StreamReader file = new System.IO.StreamReader (pFile, System.Text.Encoding.ASCII)) {

				while ((line = file.ReadLine ()) != null) {
					Debug.Log ("line=" + line);

					ShowImage (line, i);
					i++;


				}


			}
			Debug.Log (i + " line renderers created");

		}
	}
}