using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Veneris;
namespace Opal
{

	public class CreateCubeEdges : ScriptableWizard
	{
		public GameObject target;


		[MenuItem("Opal/Create Custom Mesh/Create cube edges...")]
		static void CreateWizard()
		{
			//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
			//If you don't want to use the secondary button simply leave it out:
			//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");

			ScriptableWizard.DisplayWizard<CreateCubeEdges>("Create cube edges", "Create");
		
		}

		void OnWizardCreate()
		{
			if (target!=null) {
				Create();
			}
			
		}

		void OnWizardUpdate()
		{
			helpString = "Select a cube to add edges";
		}
		public void Create ()
		{
			Vector3 scale=target.transform.lossyScale;
			Vector3 pos=target.transform.position;
			Vector3 origin=pos-scale*0.5f;
			Debug.Log("scale="+scale);
			SumoEnvironmentBuilder b=FindObjectOfType<SumoEnvironmentBuilder>();
			uint globalFaceId = b.globalFaceId +1;
		 	uint globalEdgeId = b.globalEdgeId +1;
			
			AddCubeEdges(origin, scale.x, scale.y, scale.z,globalFaceId,target,ref globalEdgeId);	
			b.globalEdgeId = globalEdgeId;
			b.globalFaceId = globalFaceId+5;
			
		}
		//Pass south-west point of cube and side length
		public  void  AddCubeEdges(Vector3 sw, float xlength, float ylength, float zlength, uint index, GameObject parent, ref uint eid) {
			//Only 8 edges, the edges on the floor are not considered
			Vector3 x=xlength*Vector3.right;
			Vector3 up=ylength*Vector3.up;
			Vector3 z=zlength*Vector3.forward;
			
			Vector3 xu=Vector3.right;
			Vector3 yu=Vector3.up;
			Vector3 zu=Vector3.forward;
			
			//Points
			Vector3 nw=sw+z;
			Vector3 ne=nw+x;
			Vector3 se=ne-z;
			int i=0;
			GameObject cube = new GameObject("OpalEdge "+i);
			//Add verticals
			OpalEdge e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(sw,up,index+4,index+2,z,x,-xu,-zu,eid);
			eid++;
			cube.transform.parent=parent.transform;
			i++;
			cube = new GameObject("OpalEdge "+i);
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(nw,up,index+4,index,-z,x,-xu,zu,eid);
			eid++;
			cube.transform.parent=parent.transform;
			i++;
			cube = new GameObject("OpalEdge "+i);
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(ne,up,index,index+5,-x,-z, zu,xu, eid);
			eid++;
			cube.transform.parent=parent.transform;
			i++;
			cube = new GameObject("OpalEdge "+i);
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(se,up,index+5,index+2,z,-x,xu,-zu,eid);
			eid++;
			//Add horizontals at elevation
			cube.transform.parent=parent.transform;
			i++;
			cube = new GameObject("OpalEdge "+i);
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(sw+up,z,index+4,index+1,-up,x,-xu,yu, eid);
			eid++;
			cube.transform.parent=parent.transform;
			i++;
			cube = new GameObject("OpalEdge "+i);
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(nw+up,x,index,index+1,-up,-z,zu,yu, eid);
			eid++;
			cube.transform.parent=parent.transform;
			i++;
			cube = new GameObject("OpalEdge "+i);
			e = cube.AddComponent<OpalEdge>();
			eid++;
			e.SetInfoValues(ne+up,-z,index+5,index+1,-up,-x,xu,yu,eid);
			eid++;
			cube.transform.parent=parent.transform;
			i++;
			cube = new GameObject("OpalEdge "+i);
			e = cube.AddComponent<OpalEdge>();
			e.SetInfoValues(se+up,-x,index+2,index+1,-up,z,-zu,yu, eid);
			eid++;
		}
	}
}

