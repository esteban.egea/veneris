using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

namespace Opal
{

	public class SaveJsonScenario : ScriptableWizard {
		public string folder;
		public string name;
		[MenuItem ("Opal/Save scenario as JSON...")]
			static void CreateWizard ()
			{
				//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
				//If you don't want to use the secondary button simply leave it out:
				//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");
				ScriptableWizard.DisplayWizard<SaveJsonScenario> ("Save JSON scenario", "Save");
			}

		void OnWizardCreate ()
		{
			if (String.IsNullOrEmpty(folder) ||  String.IsNullOrEmpty(name) ) 
			{
				SaveStaticMeshesToFolder.MenuSaveJsonScenario();
			} else {
				SaveStaticMeshesToFolder.SaveJsonScenario(folder, name);
			}
		}
	}
}
