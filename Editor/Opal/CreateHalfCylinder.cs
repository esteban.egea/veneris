﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Opal
{

	public class CreateHalfCylinder : ScriptableWizard
	{
		public Material mat;
		public int resolution = 100;
		public float thickness = 0.25f;
		public float radius = 1f;
		public float length = 1f;
		public float endAngle = 4.581f; //In degrees
		public Vector3 lengthDirection= Vector3.forward;
		public TextMesh tmPrefab;
		public GameObject lineRenderer;
		public enum cylinderType  {
			Full,
			Half,
			Custom
		};
		public  cylinderType cylType;
		float normalization;

		[MenuItem("Opal/Create Custom Mesh/Create cylinder on editor")]
		static void CreateWizard()
		{
			//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
			//If you don't want to use the secondary button simply leave it out:
			//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");
			ScriptableWizard.DisplayWizard<CreateHalfCylinder>("Create half cylinder on editor", "Create");
		}
		void OnWizardCreate()
		{
			
			if (cylType == cylinderType.Full) {
				normalization = 2 * Mathf.PI / resolution;
			} else if ((cylType == cylinderType.Half)) {
				normalization = Mathf.PI / resolution;
			} else {
				normalization = endAngle * Mathf.Deg2Rad / resolution;
			}
			GameObject go = new GameObject ("HalfCylinder"+resolution);
			MeshRenderer mr = go.AddComponent<MeshRenderer> ();
			mr.sharedMaterial = mat;
			MeshFilter mf = go.AddComponent<MeshFilter> ();
			//mf.mesh = CreateMesh ();
			//mf.mesh = ExtrudeMesh(go);
			mf.mesh= CreateMinimalMesh(go);
		}

		//To avoid the repetition of vertices and triangles
		public Mesh CreateMinimalMesh (GameObject go)
		{
			Mesh mesh = new Mesh ();
			//Add curvature information
			MeshCurvatureData md=go.AddComponent<MeshCurvatureData>();
			List<Vector4> pd1 = new List<Vector4> ();
			List<Vector4> pd2 = new List<Vector4> ();
			List<Vector3> centers = new List<Vector3> ();
			List<Vector3> vertList = new List<Vector3> ();
			List<int> triList = new List<int> ();
			List<Vector2> uvList = new List<Vector2> ();
			//Vector3 offset = Vector3.right * radius;
			//Debug.DrawRay (Vector3.zero, offset, Color.red);
			//Debug.Log ("offset=" + offset.ToString ("F6"));
			int triIdx = 0;
			float t = 1f;
			float futureT = 1f;
			int[] idx = new int[]{ 0, 1, 2, 3, 4, 5, 6, 7 };
			//Curvatures
			Vector4 u1;
			Vector4 u2;
			//Create translation matrix on the extrusion direction 
			Matrix4x4 trm=Matrix4x4.TRS (lengthDirection, Quaternion.identity, Vector3.one);
			for (int s = 0; s < resolution; s++) {
				if (s == 0) {
					t = ((float)s) * normalization;
					futureT = ((float)s + 1) * normalization;
				} else {
					t = futureT;
					futureT = ((float)s + 1) *normalization;
				}

				Vector3 segmentStart = PointOnCylinder (t);
				Vector3 segmentEnd = PointOnCylinder (futureT);
				//Debug.Log ("segmenteStart=" + segmentStart.ToString ("F6"));
				//Debug.DrawRay (Vector3.zero, segmentStart, Color.blue);
				//Debug.Log ("segmenteEnd=" + segmentEnd.ToString ("F6"));
				//Debug.DrawRay (Vector3.zero, segmentEnd, Color.black);
				Vector3 p0 = segmentStart.normalized * radius;
				Vector3 p1 = segmentStart.normalized * (radius + thickness);
				Vector3 p2 = segmentEnd.normalized * radius;
				Vector3 p3 = segmentEnd.normalized * (radius+thickness);

				//Extruded vertices
				Vector3 p00 = trm.MultiplyPoint (p0);
				Vector3 p11 = trm.MultiplyPoint (p1);
				Vector3 p22 = trm.MultiplyPoint (p2);
				Vector3 p33 = trm.MultiplyPoint (p3);



				//This is done to avoid creating internal extructures, only the external outline of the curve is created
				if (s == 0) {
					vertList.Add (p0); //0
					vertList.Add (p1); //1
					vertList.Add (p2); //2
					vertList.Add (p3); //3
					vertList.Add (p00);
					vertList.Add (p11);
					vertList.Add (p22);
					vertList.Add (p33);
					Vector2[] uvs = new Vector2[] {
						new Vector2 (0, 0), 
						new Vector2 (0, 1), 
						new Vector2 (1, 1),
						new Vector2 (1, 0),
						new Vector2 (0, 0), 
						new Vector2 (0, 1), 
						new Vector2 (1, 1),
						new Vector2 (1, 0)

					};
					uvList.AddRange (uvs);
					//Front face
					//First triangle
					triList.Add (idx [0]);
					triList.Add (idx [3]);
					triList.Add (idx [1]);

					//This face is flat for a cylinder
					u1=Vector3.right;
					u1.w = Mathf.Infinity;
					u2=Vector3.up;
					u2.w = Mathf.Infinity;
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [3]], vertList [idx [1]]));

					//Second triangle
					triList.Add (idx [0]);
					triList.Add (idx [2]);
					triList.Add (idx [3]);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [2]], vertList [idx [3]]));
					//Back face
					//First extruded triangle
					triList.Add (idx [5]);
					triList.Add (idx [7]);
					triList.Add (idx [4]);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [5]], vertList [idx [7]], vertList [idx [4]]));
					//Second  extruded triangle
					triList.Add (idx [4]);
					triList.Add (idx [7]);
					triList.Add (idx [6]);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [4]], vertList [idx [7]], vertList [idx [6]]));
					//Rigth face
					triList.Add (idx [1]);
					triList.Add (idx [7]);
					triList.Add (idx [5]);
					//Curvature in length direction is infinity
					u1 = lengthDirection.normalized;
					u1.w = Mathf.Infinity;

					//Compute principal direction from normal
					u2=ComputePrincipalDirection(vertList [idx [1]],vertList [idx [7]],vertList [idx [5]],1);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [1]], vertList [idx [7]], vertList [idx [5]]));
					//Second  extruded triangle
					triList.Add (idx [1]);
					triList.Add (idx [3]);
					triList.Add (idx [7]);
				

					//Compute principal direction from normal
					u2=ComputePrincipalDirection(vertList [idx [1]],vertList [idx [3]],vertList [idx [7]],1);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [1]], vertList [idx [3]], vertList [idx [7]]));

					//Left face
					triList.Add (idx [0]);
					triList.Add (idx [6]);
					triList.Add (idx [2]);

					//Compute principal direction from normal
					u2=ComputePrincipalDirection(vertList [idx [0]],vertList [idx [6]],vertList [idx [2]],-1);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [6]], vertList [idx [2]]));
					//Second  extruded triangle
					triList.Add (idx [0]);
					triList.Add (idx [4]);
					triList.Add (idx [6]);

					u2=ComputePrincipalDirection(vertList [idx [0]],vertList [idx [4]],vertList [idx [6]],-1);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [4]], vertList [idx [6]]));
					if (!(cylType == cylinderType.Full)) {
						//First cap
						//First triangle
						triList.Add (idx [0]);
						triList.Add (idx [5]);
						triList.Add (idx [4]);
						u1=Vector3.forward;
						u1.w = Mathf.Infinity;
						u2=Vector3.right;
						u2.w = Mathf.Infinity;
						pd1.Add (u1);
						pd2.Add (u2);
						centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [5]], vertList [idx [4]]));
						triList.Add (idx [0]);
						triList.Add (idx [1]);
						triList.Add (idx [5]);
						pd1.Add (u1);
						pd2.Add (u2);
						centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [1]], vertList [idx [5]]));
					}
					triIdx = 8;
				} else {
					idx [0] = idx [2];
					idx [1] = idx [3];
					idx [4] = idx [6];
					idx [5] = idx [7];
					vertList.Add (p2);
					idx [2] = triIdx;
					++triIdx;
					vertList.Add (p3);
					idx [3] = triIdx;
					++triIdx;
					vertList.Add (p22);
					idx [6] = triIdx;
					++triIdx;
					vertList.Add (p33);
					idx [7] = triIdx;
					++triIdx;
					Vector2[] uvs = new Vector2[] {

						new Vector2 (1, 1),
						new Vector2 (1, 0),
						new Vector2 (1, 1),
						new Vector2 (1, 0)

					};
					uvList.AddRange (uvs);

					//Front face
					//First triangle
					triList.Add (idx [0]);
					triList.Add (idx [3]);
					triList.Add (idx [1]);
					u1=Vector3.right;
					u1.w = Mathf.Infinity;
					u2=Vector3.up;
					u2.w = Mathf.Infinity;
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [3]], vertList [idx [1]]));
					//Second triangle
					triList.Add (idx [0]);
					triList.Add (idx [2]);
					triList.Add (idx [3]);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [2]], vertList [idx [3]]));
					//Back face
					//First extruded triangle
					triList.Add (idx [5]);
					triList.Add (idx [7]);
					triList.Add (idx [4]);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [5]], vertList [idx [7]], vertList [idx [4]]));
					//Second  extruded triangle
					triList.Add (idx [4]);
					triList.Add (idx [7]);
					triList.Add (idx [6]);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [4]], vertList [idx [7]], vertList [idx [6]]));
					//Rigth face
					triList.Add (idx [1]);
					triList.Add (idx [7]);
					triList.Add (idx [5]);
					//Curvature in length direction is infinity
					u1 = lengthDirection.normalized;
					u1.w = Mathf.Infinity;

					//Compute principal direction from normal
					u2=ComputePrincipalDirection(vertList [idx [1]],vertList [idx [7]],vertList [idx [5]],1);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [1]], vertList [idx [7]], vertList [idx [5]]));
					//Second  extruded triangle
					triList.Add (idx [1]);
					triList.Add (idx [3]);
					triList.Add (idx [7]);
					//Compute principal direction from normal
					u2=ComputePrincipalDirection(vertList [idx [1]],vertList [idx [3]],vertList [idx [7]],1);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [1]], vertList [idx [3]], vertList [idx [7]]));
					//Left face
					triList.Add (idx [0]);
					triList.Add (idx [6]);
					triList.Add (idx [2]);

					//Compute principal direction from normal
					u2=ComputePrincipalDirection(vertList [idx [0]],vertList [idx [6]],vertList [idx [2]],-1);
					pd1.Add (u1);
					pd2.Add (u2);	
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [6]], vertList [idx [2]]));

					//Second  extruded triangle
					triList.Add (idx [0]);
					triList.Add (idx [4]);
					triList.Add (idx [6]);
					u2=ComputePrincipalDirection(vertList [idx [0]],vertList [idx [4]],vertList [idx [6]],-1);
					pd1.Add (u1);
					pd2.Add (u2);
					centers.Add (GetCentroid(vertList [idx [0]], vertList [idx [4]], vertList [idx [6]]));
				}

			

		


			}
			if (!(cylType == cylinderType.Full)) {
				//End cap
				//First triangle
				triList.Add (idx [2]);
				triList.Add (idx [7]);
				triList.Add (idx [3]);
				u1=Vector3.forward;
				u1.w = Mathf.Infinity;
				u2=Vector3.right;
				u2.w = Mathf.Infinity;
				pd1.Add (u1);
				pd2.Add (u2);
				centers.Add (GetCentroid(vertList [idx [2]], vertList [idx [7]], vertList [idx [3]]));
				triList.Add (idx [2]);
				triList.Add (idx [6]);
				triList.Add (idx [7]);
				pd1.Add (u1);
				pd2.Add (u2);
				centers.Add (GetCentroid(vertList [idx [2]], vertList [idx [6]], vertList [idx [7]]));
			}

			//Check
			Debug.Log("tri="+triList.Count+"pd1="+pd1.Count+"pd2="+pd2.Count);

			md.setPd1(pd1);
			md.setPd2(pd2);
			md.centers = centers;
			Debug.Log (vertList.Count);
			mesh.vertices = vertList.ToArray ();
			mesh.triangles = triList.ToArray ();
			mesh.uv = uvList.ToArray ();
			mesh.RecalculateNormals ();
			mesh.RecalculateBounds ();

		
			return mesh;
		}

		public Vector4 ComputePrincipalDirection(Vector3 p0, Vector3 p1, Vector3 p2, float curvature) {
			//Compute principal direction from normal
			Vector3 e0 = p1-p0; //p1 -p0
			Vector3 e1 = p0-p2; //p0-p2
			Vector3 n= Vector3.Cross(e1,e0).normalized;

			Vector3 pdd = Vector3.Cross (n, lengthDirection.normalized).normalized;
			//Debug.Log ("dot=" + Vector3.Dot (pdd, n));
			Vector4 u= pdd;
			//Debug.Log ("normal=" + n.normalized.ToString("F6") + "u="+u.normalized.ToString("F6")+"R="+curvature +"center="+GetCentroid(p0,p1,p2));
		
			u.w = curvature; //Radius is 1
			return u;
		}
		public Vector3 GetCentroid(Vector3 p0, Vector3 p1, Vector3 p2) {
			return ((p0 + p1 + p2) / 3);
		}

		public Mesh ExtrudeMesh (GameObject par)
		{
			//Create only the 2D curve on the XY plane and then extrude on Z
			Mesh mesh;

			mesh = new Mesh ();
			List<Vector3> vertList = new List<Vector3> ();
			List<int> triList = new List<int> ();
			List<Vector2> uvList = new List<Vector2> ();
			//Vector3 offset = Vector3.right * radius;
			//Debug.DrawRay (Vector3.zero, offset, Color.red);
			//Debug.Log ("offset=" + offset.ToString ("F6"));
			int triIdx = 0;
			float t = 1f;
			float futureT = 1f;
			for (int s = 0; s < resolution; s++) {
				if (s == 0) {
					t = ((float)s) * Mathf.PI / resolution;
					futureT = ((float)s + 1) * Mathf.PI / resolution;
				} else {
					t = futureT;
					futureT = ((float)s+1 ) * Mathf.PI / resolution;
				}

				Vector3 segmentStart = PointOnCylinder (t);
				Vector3 segmentEnd = PointOnCylinder (futureT);
				//Debug.Log ("segmenteStart=" + segmentStart.ToString ("F6"));
				//Debug.DrawRay (Vector3.zero, segmentStart, Color.blue);
				//Debug.Log ("segmenteEnd=" + segmentEnd.ToString ("F6"));
				//Debug.DrawRay (Vector3.zero, segmentEnd, Color.black);
				Vector3 p0 = segmentStart.normalized * radius;
				Vector3 p1 = segmentStart.normalized * (radius+thickness);
				Vector3 p2 = segmentEnd.normalized *(radius+ thickness);
				Vector3 p3 = segmentEnd.normalized * radius;

				//This is done to avoid creating internal extructures, only the external outline of the curve is created
				if (s == 0) {
					vertList.Add (p0);
					vertList.Add (p1);
					vertList.Add (p2);
					vertList.Add (p3);
					Vector2[] uvs = new Vector2[] {
						new Vector2 (0, 0), 
						new Vector2 (0, 1), 
						new Vector2 (1, 1),
						new Vector2 (1, 0)

					};
					uvList.AddRange (uvs);
					//First triangle
					triList.Add (triIdx + 0);
					triList.Add (triIdx + 2);
					triList.Add (triIdx + 1);
					//Second triangle
					triList.Add (triIdx + 0);
					triList.Add (triIdx + 3);
					triList.Add (triIdx + 2);
					triIdx += 4;
				} else {
					vertList.Add (p2);
					vertList.Add (p3);
					Vector2[] uvs = new Vector2[] {

						new Vector2 (1, 1),
						new Vector2 (1, 0)

					};
					uvList.AddRange (uvs);
					//First triangle
					triList.Add (triIdx -1);
					triList.Add (triIdx + 0);
					triList.Add (triIdx - 2);
					//Second triangle
					triList.Add (triIdx -1);
					triList.Add (triIdx + 1);
					triList.Add (triIdx );
					triIdx += 2;
				}




			}

			mesh.vertices = vertList.ToArray ();
			mesh.triangles = triList.ToArray ();
			mesh.uv = uvList.ToArray ();
			mesh.RecalculateNormals ();
			mesh.RecalculateBounds ();
			//return mesh;

			//Now extrude on z
			List<Matrix4x4> sections = new List<Matrix4x4> ();
			sections.Add (Matrix4x4.TRS (new Vector3 (0f, 0f, length), Quaternion.identity, Vector3.one));
			sections.Add (Matrix4x4.TRS (Vector3.zero, Quaternion.identity, Vector3.one));
			Mesh m = new Mesh ();
			List<MeshExtrusion.Edge> edges = new List<MeshExtrusion.Edge> ();
			MeshExtrusion.ExtrudeMesh (mesh, m, sections.ToArray (), true, edges);
			GameObject go = GameObject.Instantiate (lineRenderer);
			LineRenderer lr = go.GetComponent<LineRenderer> ();
			List<Vector3> points = new List<Vector3> ();
			int i = 0;
			foreach (MeshExtrusion.Edge e in edges) {
				Debug.Log ("e=" + e.vertexIndex [0] + "," + e.vertexIndex [1]);
				if (i == 0) {
					points.Add (mesh.vertices [e.vertexIndex [0]]);
					points.Add (mesh.vertices [e.vertexIndex [1]]);
					//Debug.DrawLine (mesh.vertices [e.vertexIndex [0]], mesh.vertices [e.vertexIndex [1]],Color.red);
				} else {
					points.Add (mesh.vertices [e.vertexIndex [1]]);
				}
				++i;
			}
			lr.positionCount = points.Count;
			lr.SetPositions (points.ToArray ());
			go.transform.parent = par.transform;
			return m;
		}
		//cacluates point coordinates on a quadratic curve
		public static Vector3 PointOnPath (float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
		{
			float u, uu, uuu, tt, ttt;
			Vector3 p;

			u = 1 - t;
			uu = u * u;
			uuu = uu * u;

			tt = t * t;
			ttt = tt * t;

			p = uuu * p0;
			p += 3 * uu * t * p1;
			p += 3 * u * tt * p2;
			p += ttt * p3;

			return p;
		}
		//cacluates point coordinates on a cylinder
		public  Vector3 PointOnCylinder (float t)
		{



			return new Vector3 (radius * Mathf.Cos (t), radius * Mathf.Sin (t), 0.0f);
		}
		public Mesh CreateMesh ()
		{
			Mesh mesh;

			mesh = new Mesh ();

			float scaling = radius;
			float width = thickness / 2f;
			List<Vector3> vertList = new List<Vector3> ();
			List<int> triList = new List<int> ();
			List<Vector2> uvList = new List<Vector2> ();
			Vector3 upNormal = new Vector3 (0, 0, -1);

			triList.AddRange (new int[] {                     
				2, 1, 0,    //start face
				0, 3, 2
			});


			for (int s = 0; s < resolution; s++) {
				/*float t = ((float)s) / resolution;
				float futureT = ((float)s + 1) / resolution;

				Vector3 segmentStart = PointOnPath (t, start, handle1, handle2, end);
				Vector3 segmentEnd = PointOnPath (futureT, start, handle1, handle2, end);
*/
				float t = ((float)s) * Mathf.PI / resolution;
				float futureT = ((float)s + 1) * Mathf.PI / resolution;
				Vector3 segmentStart = PointOnCylinder (t);
				Vector3 segmentEnd = PointOnCylinder (futureT);

				Debug.Log ("segmenteStart=" + segmentStart.ToString ("F6"));
				Debug.DrawRay (Vector3.zero, segmentStart, Color.blue);
				Debug.Log ("segmenteEnd=" + segmentEnd.ToString ("F6"));
				Debug.DrawRay (Vector3.zero, segmentEnd, Color.black);

				Vector3 segmentDirection = segmentEnd - segmentStart;
				if (s == 0 || s == resolution - 1)
					segmentDirection = new Vector3 (0, 1, 0);
				segmentDirection.Normalize ();
				Vector3 segmentRight = Vector3.Cross (upNormal, segmentDirection);
				Debug.DrawRay (Vector3.zero, segmentRight, Color.yellow);
				Debug.Log ("segmenteRight=" + segmentRight.ToString ("F6"));
				segmentRight *= width;
				Vector3 offset = segmentRight.normalized * (width / 2) * scaling;
				Debug.DrawRay (Vector3.zero, offset, Color.red);
				Debug.Log ("offset=" + offset.ToString ("F6"));
				Vector3 br = segmentRight + upNormal * width + offset;
				Vector3 tr = segmentRight + upNormal * -width + offset;
				Vector3 bl = -segmentRight + upNormal * width + offset;
				Vector3 tl = -segmentRight + upNormal * -width + offset;

				int curTriIdx = vertList.Count;

				Vector3[] segmentVerts = new Vector3[] {
					segmentStart + br,
					segmentStart + bl,
					segmentStart + tl,
					segmentStart + tr,
				};
				Debug.DrawRay (Vector3.zero, segmentStart + br, Color.green);
				Debug.DrawRay (Vector3.zero, segmentStart + bl, Color.red);
				Debug.DrawRay (Vector3.zero, segmentStart + tr, Color.green);
				Debug.DrawRay (Vector3.zero, segmentStart + tl, Color.red);
				for (int i = 0; i < segmentVerts.Length; i++) {
					Debug.Log ("v=" + segmentVerts [i].ToString ("F6"));
				}
				vertList.AddRange (segmentVerts);

				Vector2[] uvs = new Vector2[] {
					new Vector2 (0, 0), 
					new Vector2 (0, 1), 
					new Vector2 (1, 1),
					new Vector2 (1, 1)
				};
				uvList.AddRange (uvs);

				int[] segmentTriangles = new int[] {
					curTriIdx + 6, curTriIdx + 5, curTriIdx + 1, //left face
					curTriIdx + 1, curTriIdx + 2, curTriIdx + 6,
					curTriIdx + 7, curTriIdx + 3, curTriIdx + 0, //right face
					curTriIdx + 0, curTriIdx + 4, curTriIdx + 7,
					curTriIdx + 1, curTriIdx + 5, curTriIdx + 4, //top face
					curTriIdx + 4, curTriIdx + 0, curTriIdx + 1,
					curTriIdx + 3, curTriIdx + 7, curTriIdx + 6, //bottom face
					curTriIdx + 6, curTriIdx + 2, curTriIdx + 3
				};
				triList.AddRange (segmentTriangles);

				// final segment fenceposting: finish segment and add end face
				if (s == resolution - 1) {
					curTriIdx = vertList.Count;

					vertList.AddRange (new Vector3[] {
						segmentEnd + br,
						segmentEnd + bl,
						segmentEnd + tl,
						segmentEnd + tr
					});

					uvList.AddRange (new Vector2[] { 
						new Vector2 (0, 0), 
						new Vector2 (0, 1), 
						new Vector2 (1, 1),
						new Vector2 (1, 1)
					}
					);
					triList.AddRange (new int[] {
						curTriIdx + 0, curTriIdx + 1, curTriIdx + 2, //end face
						curTriIdx + 2, curTriIdx + 3, curTriIdx + 0
					});
				}
			}
			/*int j = 0;
			foreach (Vector3 pos in vertList) {
				TextMesh tm = GameObject.Instantiate (tmPrefab, pos, Quaternion.identity) as TextMesh;
				tm.name = (j).ToString ();
				tm.text = (j).ToString ();
				tm.transform.parent = transform;
				j++;
			}
			*/



			mesh.vertices = vertList.ToArray ();
			mesh.triangles = triList.ToArray ();
			mesh.uv = uvList.ToArray ();
			mesh.RecalculateNormals ();
			mesh.RecalculateBounds ();


			return mesh;
		}
	}

}
