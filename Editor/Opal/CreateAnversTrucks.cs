using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

namespace Opal
{

	public class CreateAnversTrucks : ScriptableWizard
	{
		public uint trucks=10;
		public float truckDistance=40f;
		public float initialDistance=50f;
	//To get the correct z
		protected float R=2900.0f;
		protected float l=112.0f; //2*R*sin a/2=2*R*0.04f;
		protected float h=2.32092875f; //h=R-0.5*sqrt(4*R*R-4*l*l)
		[MenuItem ("Opal/Create Custom Mesh/Create Anvers Trucks...")]
			static void CreateWizard ()
			{
				//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
				//If you don't want to use the secondary button simply leave it out:
				//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");
				ScriptableWizard.DisplayWizard<CreateAnversTrucks> ("Create Anvers Trucks editor", "Create");
			}

		void OnWizardCreate ()
		{
			GameObject go = new GameObject("target");
			float z=initialDistance;
			go.transform.position = new Vector3(7f, -5.5f, z);
			uint gfid=0;
			uint geid=0;
			uint smid=0;
			for (int i=0; i< trucks; i++) {
				CreateCubeFromPlant c= new CreateCubeFromPlant();
				if (z <=426) {
					go.transform.position = new Vector3(7f, -5.5f-(0.04f*z), z);
					go.transform.rotation=Quaternion.FromToRotation(Vector3.forward,new Vector3(0.0f,-4.0f,100f));
				} else if (z>426 && z<=658) {
					float zp=z-538f;
					float yp=Mathf.Sqrt((R*R)-(zp*zp))-R+h;
					float fl=-(23.04f+yp);
					go.transform.position = new Vector3(7f, fl+0.5f, z);
					go.transform.rotation=Quaternion.LookRotation(Vector3.forward, Vector3.up);
				} else {
					go.transform.position = new Vector3(7f, -22.5f+(0.04f*(z-658.0f)), z);
					go.transform.rotation=Quaternion.FromToRotation(Vector3.forward,new Vector3(0.0f,4.0f,100f));
				} 
				c.name="atruck"+i;
				c.applyTransform=true;
				c.targetTransform=go.transform;
				c.globalFaceId=gfid;
				c.globalEdgeId=geid;
				c.staticMeshId=smid;
				GameObject cube=c.Create();
				cube.name="truck"+i;	
				//cube.transform.position=go.transform.position;
				z = z+truckDistance;
				gfid +=6;
				geid +=8;
				smid++;
			}
		}
	}
}

