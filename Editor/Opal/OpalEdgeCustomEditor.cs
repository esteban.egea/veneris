/******************************************************************************/
// 
// Copyright (c) 2021 Esteban Egea-Lopez http://girtel.upct.es/~eegea
// 
/*******************************************************************************/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Opal {
	[CustomEditor(typeof(OpalEdge))]
		public class OpalEdgeCustomEditor : Editor {
			OpalEdge _target;	
			GUIStyle style = new GUIStyle();
			void OnEnable(){

				style.fontStyle = FontStyle.Bold;
				style.normal.textColor = Color.white;
				_target = (OpalEdge)target;


			}
			public override void OnInspectorGUI(){
				if (_target.enabled) {
				//Outline edge
				Debug.DrawRay(_target.info.o,_target.info.v,Color.green, 10);
				Debug.DrawRay(_target.info.o,_target.info.a,Color.red, 10);
				Debug.DrawRay(_target.info.o,_target.info.b,Color.blue, 10);
				}
				
				DrawDefaultInspector();
			}

		}
}
