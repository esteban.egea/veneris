﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Globalization;
using System.Threading;

namespace Opal
{
	
	public class SaveParticularMesh : ScriptableWizard
	{

		//public string pFile = "\\\\192.168.1.33\\eegea\\optix6.5\\SDK\\bin\\h.txt";
		public GameObject target;


		[MenuItem("Opal/Save mesh...")]
		static void CreateWizard()
		{
			//ScriptableWizard.DisplayWizard<ShowRaysInEditor>("Load rays on editor", "Load","Apply");
			//If you don't want to use the secondary button simply leave it out:
			//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");

			ScriptableWizard.DisplayWizard<SaveParticularMesh>("Save Mesh", "Save");
		
		}

		void OnWizardCreate()
		{
			Save();
			
		}

		void OnWizardUpdate()
		{
			helpString = "Select mesh to save";
		}
		public void Save ()
		{
			CultureInfo ci = new CultureInfo("en-US");
			Thread.CurrentThread.CurrentCulture = ci;
			Thread.CurrentThread.CurrentUICulture = ci;
			string folder = Directory.GetCurrentDirectory ();
			StaticMesh sm=target.GetComponent<StaticMesh>();
			if (sm!=null) {
				SaveStaticMeshesToFolder.SaveStaticMesh(target.transform,sm,false,folder);
			}
		}
	}
}
