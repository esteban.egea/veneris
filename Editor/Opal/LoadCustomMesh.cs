﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class LoadCustomMesh  {

	[MenuItem ("Opal/Load Simple Mesh from file ...")]
	public static void LoadSimpleFromFile ()
	{
		string pathToVertices = EditorUtility.OpenFilePanel ("Select vertices", "", "");
		string pathToIndex = EditorUtility.OpenFilePanel ("Select indices", "", "");
		string line;
		List<Vector3> vertices = new List<Vector3> ();
		using (System.IO.StreamReader file = new System.IO.StreamReader (pathToVertices, System.Text.Encoding.ASCII)) {
			char[] separator = new char[]{ '\t' };
			System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo ("en-US");
			while ((line = file.ReadLine ()) != null) {
				string[] tokens = line.Split (separator);
				Vector3 p = new Vector3 (float.Parse (tokens [0], ci), float.Parse (tokens [1], ci), float.Parse (tokens [2], ci));
				Debug.Log (p);
				vertices.Add (p);

			}
		}
		List<int> indices = new List<int> ();
		using (System.IO.StreamReader file = new System.IO.StreamReader (pathToIndex, System.Text.Encoding.ASCII)) {
			
			System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo ("en-US");
			while ((line = file.ReadLine ()) != null) {
				int i = int.Parse (line);
				Debug.Log (i);
				indices.Add (i);
			}
		}
		GameObject go = new GameObject ("Simple mesh");
		MeshFilter mf=go.AddComponent<MeshFilter> ();
		Mesh mesh = new Mesh ();
		mesh.SetVertices (vertices);
		mesh.triangles=indices.ToArray();
		mf.mesh = mesh;
	

	}
}
